﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace BoundHound.Framework
{
    public struct EnvironmentVariables
    {
        public const string BoundHoundSourcePath = "BOUNDHOUND_SOURCE_PATH";
    }

    public struct MimeTypeConstant
    {
        public const string Xlsx = "application/vnd.openxmlformats-officedocument.spreadsheetml.sheet";
        public const string Pdf = "application/pdf";
        public const string Csv = "text/csv";
        public const string Html = "text/html";
        public const string OctetStream = "application/octet-stream";
        public const string Json = "application/json";
    }

    public struct OtherConstants
    {
        // NoAccountId is used when we have data in the database where we used 0 to mean that the accountId was not specified
        public const int NoSchoolId = 0;

        public const int DefaultTenantId = 0;
        public const int DefaultUserId = 1;
        public const string DefaultUserName = "Default user";
        public const string BoundHoundAnonymousUserName = "Anonymous";
    }

    /// <summary>
    ///Define constants for all items stored in the context object.
    /// </summary>
    public struct ContextKeys
    {
        public const string TenantId = "tenantId";
        public const string UserId = "userId";
    }

    public struct UserConstants
    {
        public const int AnonymousUserId = 1;
        public const int BoundhoundCustomerServiceId = 2;
        public const int BatchProcessUserId = 3;

        public const string BoundhoundBatchProcessUserName = "Batch Process";
        public const string BoundhoundAnonymousUserName = "Anonymous";
    }
    public struct TokenAuthentication
    {
        public const string TokenHeaderName = "BoundHoundAuthToken";
        public const string TokenHeaderServerName = "BoundHoundServerToken";
        public const string TokenIssuer = "BoundHound.com";
        public const string TokenAudience = "BoundHound.com";
    }

    public struct DefaultData
    {
        public const string StudentCategory = "Day scholar";
        public const string Section = "Sec A";
        public const int SectionStrength = 25;

    }

    public struct LicenseKeys
    {
        public const string EvoPDFKey = "Iqy8rb6+rby5v626o72tvryjvL+jtLS0tK29";
    }

    public struct ReportsConstants
    {
        public const string IncludeAllText = "ALL";
        public const string DummyIDsText = "ALL";

    }

}
