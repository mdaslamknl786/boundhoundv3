﻿using StructureMap;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Web.Http.Dependencies;

namespace BoundHound.Framework.Services
{
    public class StructureMapDependencyResolver : StructureMapScope, IDependencyResolver
    {
        private readonly IContainer m_container;

        public StructureMapDependencyResolver(IContainer container)
            : base(container)
        {
            m_container = container;
        }

        public IDependencyScope BeginScope()
        {
            var childContainer = m_container.GetNestedContainer();
            return new StructureMapScope(childContainer);
        }
    }
}
