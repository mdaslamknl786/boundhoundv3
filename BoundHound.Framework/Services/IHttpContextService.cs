﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Collections.Specialized;
using System.Linq;
using System.Security.Claims;
using System.Security.Principal;
using System.Text;
using System.Threading.Tasks;
using System.Web;

namespace BoundHound.Framework.Services
{
    public interface IHttpContextService
    {
        bool IsAuthenticated { get; }
        IDictionary Items { get; }
        string CurrentUserCookie { get; }
        string RequestUserAgent { get; }
        string RequestPath { get; }
        string RequestHost { get; }
        NameValueCollection RequestHeaders { get; }
        NameValueCollection QueryString { get; }
        NameValueCollection ServerVariables { get; }
        HttpResponseBase Response { get; }
        HttpRequestBase Request { get; }
        HttpServerUtilityBase Server { get; }
        IPrincipal CurrentUser { get; set; }
        void CompleteRequest();
        void ServerExecute(string path);
        void ServerTransfer(string path);
        List<Claim> CurrentUserClaims { get; }
        NameValueCollection RequestParams { get; }
        string PhysicalApplicationPath { get; }
        string UserHostAddress { get; }
    }
}
