﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Collections.Specialized;
using System.Linq;
using System.Security.Claims;
using System.Security.Principal;
using System.Web;
using Microsoft.Owin;

namespace BoundHound.Framework.Services
{
    public class HttpContextService : IHttpContextService
    {
        public bool IsAuthenticated => HttpContext.Current.Request.IsAuthenticated;

        public IDictionary Items => HttpContext.Current.Items;

        public string CurrentUserCookie => CurrentUser.Identity.Name;

        public string RequestUserAgent => HttpContext.Current.Request.UserAgent;

        public string RequestPath => HttpContext.Current.Request.Path;

        public string RequestHost => HttpContext.Current.Request.Url.Host;

        public NameValueCollection QueryString => HttpContext.Current.Request.QueryString;

        public NameValueCollection RequestHeaders => HttpContext.Current.Request.Headers;

        public NameValueCollection ServerVariables => HttpContext.Current.Request.ServerVariables;

        public IPrincipal CurrentUser
        {
            get
            {
                return HttpContext.Current.User;
            }
            set
            {
                HttpContext.Current.User = value;
            }
        }

        public HttpResponseBase Response => new HttpResponseWrapper(HttpContext.Current.Response);

        public HttpRequestBase Request => new HttpRequestWrapper(HttpContext.Current.Request);

        public HttpServerUtilityBase Server => new HttpServerUtilityWrapper(HttpContext.Current.Server);

        public void CompleteRequest() => HttpContext.Current.ApplicationInstance.CompleteRequest();

        public void ServerExecute(string path) => HttpContext.Current.Server.Execute(path);

        public void ServerTransfer(string path) => HttpContext.Current.Server.Transfer(path);

        public List<Claim> CurrentUserClaims
        {
            get
            {
                var httpContext = HttpContext.Current;

                if (httpContext == null)
                {
                    return new List<Claim>();
                }

                IOwinContext owinContext;
                try
                {
                    owinContext = httpContext.Request.GetOwinContext();
                }
                catch (InvalidOperationException)
                {
                    // In the context of Reports, where we use Nancy, we do not establish
                    // an OwinContext, and therefore we return an empty list of claims. This
                    // means that 
                    return new List<Claim>();
                }
                return owinContext?.Authentication.User.Claims.ToList() ?? new List<Claim>();
            }
            set { throw new NotImplementedException(); }
        }

        public NameValueCollection RequestParams
        {
            get
            {
                return HttpContext.Current.Request.Params;
            }
            set { throw new NotImplementedException(); }
        }

        public string PhysicalApplicationPath => HttpContext.Current.Request.PhysicalApplicationPath;

        public string UserHostAddress => HttpContext.Current.Request.UserHostAddress;
    }
}
