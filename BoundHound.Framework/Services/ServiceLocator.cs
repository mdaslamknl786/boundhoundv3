﻿using System;
using System.Collections.Generic;
using System.Threading;
using StructureMap;
using StructureMap.Query;

namespace BoundHound.Framework.Services
{
    public static class ServiceLocator
    {
        private static Lazy<IContainer> m_lazy;

        private static void Init()
        {
            m_lazy =
                new Lazy<IContainer>(() => new Container(), LazyThreadSafetyMode.ExecutionAndPublication);
        }

        static ServiceLocator()
        {
            Init();
        }

        public static IContainer Container => m_lazy.Value;

        public static IModel Model => Container.Model;

        public static T GetInstance<T>() => Container.GetInstance<T>();
        public static object GetInstance(Type type) => Container.GetInstance(type);
        public static T TryGetInstance<T>() => Container.TryGetInstance<T>();
        public static T GetNamedInstance<T>(string instanceName) => Container.GetInstance<T>(instanceName);
        public static IEnumerable<T> GetAllInstances<T>() => Container.GetAllInstances<T>();

        public static void Initialize(Action<ConfigurationExpression> configure)
        {
            Init();
            //Container.Configure(configure);
        }


        //public static void Configure(Action<ConfigurationExpression> configure) => Container.Configure(configure);

        public static void Inject<T>(T instance) where T : class => Container.Inject(instance);
        public static void Inject(Type pluginType, object instance) => Container.Inject(pluginType, instance);
        public static ExplicitArgsExpression With<T>(T instance) => Container.With(instance);
    }
}
