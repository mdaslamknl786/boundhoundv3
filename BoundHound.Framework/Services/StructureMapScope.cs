﻿using StructureMap;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Web.Http.Dependencies;

namespace BoundHound.Framework.Services
{
    public class StructureMapScope : IDependencyScope
    {
        private readonly IContainer m_container;

        public StructureMapScope(IContainer container)
        {
            if (container == null)
            {
                throw new ArgumentNullException("container");
            }
            m_container = container;
        }

        public object GetService(Type serviceType)
        {
            if (serviceType == null)
            {
                return null;
            }

            if (serviceType.IsAbstract || serviceType.IsInterface)
            {
                return m_container.TryGetInstance(serviceType);
            }

            return m_container.GetInstance(serviceType);
        }

        public IEnumerable<object> GetServices(Type serviceType)
        {
            return m_container.GetAllInstances(serviceType).Cast<object>();
        }

        public void Dispose()
        {
            m_container.Dispose();
        }
    }
}
