﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace BoundHound.Framework
{
    [Serializable]
    public class BoundHoundContext
    {
        public BoundHoundContext(int tenantId, int userId, string actingUserName, string actingUserEmail)
        {
            TenantId = tenantId;
            UserId = userId;
            ActingUserName = actingUserName;
            ActingUserEmail = actingUserEmail;
        }

        public int TenantId { get; }

        public int UserId { get; }

        public int ActingUserId { get; }

        public string ActingUserName { get; }

        public string ActingUserEmail { get; }

    }
}
