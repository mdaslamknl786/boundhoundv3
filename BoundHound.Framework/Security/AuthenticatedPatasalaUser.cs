﻿using BoundHound.Framework.Services;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace BoundHound.Framework.Security
{
    public class AuthenticatedPatasalaUser
    {
        private readonly IHttpContextService m_httpContextService;
        public const int NoValidId = -1;

        public AuthenticatedPatasalaUser(IHttpContextService httpContextService)
        {
            m_httpContextService = httpContextService;
        }

        // Patasala user fields
        public enum Field
        {
            UserId,
            UserTypeId,
            UserName,
            UserEmail,
            Role,
            ClientKey,
            BranchId,
            SchoolId,
            PersonId
        }

        public int UserId()
        {
            var value = GetValueFromUserField(Field.UserId);
            return value != null ? Convert.ToInt32(value) : NoValidId;
        }

        public int UserTypeId()
        {
            var value = GetUserFieldValue(Field.UserTypeId);
            return value != null ? Convert.ToInt32(value) : NoValidId;
        }
        public string UserName()
        {
            var value = GetValueFromUserField(Field.UserName);
            return value?.ToString() ?? string.Empty;
        }
        public string UserEmail()
        {
            var value = GetValueFromUserField(Field.UserEmail);
            return value?.ToString() ?? string.Empty;
        }
        public int RoleId()
        {
            var value = GetValueFromUserField(Field.Role);
            return value != null ? Convert.ToInt32(value) : NoValidId;
        }

        public int PersonId()
        {
            var value = GetValueFromUserField(Field.PersonId);
            return value != null ? Convert.ToInt32(value) : NoValidId;
        }

        public string ClientKey()
        {
            var value = GetUserFieldValue(Field.ClientKey);
            return value?.ToString() ?? string.Empty;
        }
        public int BranchId()
        {
            var value = GetValueFromUserField(Field.BranchId);
            return value != null ? Convert.ToInt32(value) : NoValidId;
        }
        public int SchoolId()
        {
            var value = GetValueFromUserField(Field.SchoolId);
            return value != null ? Convert.ToInt32(value) : NoValidId;
        }

        private object GetValueFromUserField(Field field)
        {
            var value = GetUserFieldValue(field);
            return value;
        }

        private object GetUserFieldValue(Field field)
        {
            var claims = ((System.Security.Claims.ClaimsIdentity)m_httpContextService.CurrentUser.Identity).Claims;
            var claim = claims?.FirstOrDefault(y => y.Type == field.ToString());
            return claim?.Value;
        }

        /// <summary>
        /// Method prepare user information cookie by given parameters
        /// </summary>
        /// <param name="schoolId"></param>
        /// <param name="branchId"></param>
        /// <param name="userId">User id</param>
        /// <param name="userTypeId">User type id</param>
        /// <param name="userName">User name</param>
        /// <param name="roleId">Role id</param>
        /// <param name="newFeature">new feature</param>
        /// <param name="backgroundImageId"></param>
        /// <param name="apiVersion">Api version - used in the context of the Public API</param>
        /// <param name="clientKey">Client key - used in the context of the Public API</param>
        /// <returns></returns>
        public string PrepareUserInfoForCookie(int schoolId,
            int branchId,
            int userId,
            int userTypeId,
            string userName,
            int roleId,
            int personId,
            string newFeature,
            string backgroundImageId,
            string apiVersion = "",
            string clientKey = "")
        {
            return
                $"{userId}:{userTypeId}:{userName}:{roleId}:{apiVersion}:{clientKey}:{branchId}:{schoolId}:{personId}";
        }

        /// <summary>
        /// Method prepare user information cookie by given parameters
        /// </summary>
        /// <param name="userId">User id</param>
        /// <param name="userEmail">User type id</param>
        /// <param name="userName">User name</param>
        /// <param name="clientKey">Client key - used in the context of the Public API</param>
        /// <returns></returns>
        public string PrepareAuthenticationCookieString(uint userId,
            string userEmail,
            string userName,
            string clientKey = "")
        {
            // concat all parameters
            return string.Format("{0}:{1}:{2}:{3}", userId, userEmail, userName, clientKey);
        }
    }
}
