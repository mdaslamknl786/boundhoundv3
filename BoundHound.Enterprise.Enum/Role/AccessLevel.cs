﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace BoundHound.Enterprise.Enum.Role
{
    public enum AccessLevel
    {
        None = 1,
        Read = 2,
        AddEdit = 3,
        Delete = 4
    }
}
