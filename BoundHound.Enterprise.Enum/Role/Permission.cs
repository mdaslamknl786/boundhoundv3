﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace BoundHound.Enterprise.Enum.Role
{
    public enum Permission
    {
        Locations = 100,
        Branches = 101,
        Classes = 102,
        Sections = 103,
        Students = 104,
        Users = 105,
        Reports = 106,
        SchoolSettings = 107,
        ImportStudents = 108,
        MainDashboard = 109,
        AcademicYears = 110,
        PublicSite = 111,
        SchoolDetails = 112,

        AdmissionSettings = 200,
        EmailTemplates = 201,
        CustomFields = 202,
        Applicants = 203,
        BulkSms = 204,
        BulkEmail = 205,
        AdmissionsReports = 206,
        AdmissionsDashboard = 207,

        FeeTypes = 300,
        CollectionAccounts = 301,
        BankAccounts = 302,
        TuitionFeePlans = 303,
        TransportFeePlans = 304,
        Concessions = 305,
        FeeCollections = 306,
        StudentLedger = 307,
        FinanceReports = 308,
        FinanceDashboard = 309,
        Receipts = 310,
        FinanceSettings = 311,
        BankDeposits = 312,

        ClassTerms = 400,
        Evaluations = 401,
        Exams = 402,
        ExamSchedule = 403,
        GradingScales = 404,
        Subjects = 405,
        Activities = 406,
        SubjectSkills = 406,
        ActivitySkills = 407,
        MarksEntry = 408
    }
}
