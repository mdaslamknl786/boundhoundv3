﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace BoundHound.Model
{
    public class AuthUserDTO
    {
            public string UserName{ get; set; }
            public string UserEmail{ get; set; }
            public int TenatId { get; set; }
            public int UserId { get; set; }
            public int SubscriptionInDays { get; set; }
            public string access_token { get; set; }
            public string ExpiredDate { get; set; }
            public string GenerateDate { get; set; }
            public string Modules { get; set; }
    }
}
