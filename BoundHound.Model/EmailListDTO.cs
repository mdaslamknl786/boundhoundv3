﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace BoundHound.Model
{
    public class EmailListDTO 
    {
        public int ListId { get; set; }
        public string ListName { get; set; }
        public string ListDesc { get; set; }
        public List<EmailDTO> Emails { get; set; }
    }
}
