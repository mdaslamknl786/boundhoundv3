﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace BoundHound.Model
{
    public class ListDTO
    {
        public int ListId { get; set; }
        public string ListName { get; set; }
        public string ListDesc { get; set; }
        public int TenantId { get; set; }
        public int UserId { get; set; }
        public DateTime LastUpdate { get; set; }
        public bool IsActive { get; set; }
        public string TenantName { get; set; }
        public List<EmailDTO> Emails { get; set; }
    }
}
