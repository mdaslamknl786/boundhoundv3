﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace BoundHound.Model
{
    public class TenantDTO
    {
        public int TenantID { get; set; }
        public string TenantName { get; set; }
        public string LinkedInEmail { get; set; }
        public string LinkedPassword { get; set; }
        public SubscriptionDTO Subscription { get; set; }
    }
}
