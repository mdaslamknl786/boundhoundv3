﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace BoundHound.Model
{
    public class UserDTO
    {
        public int Id { get; set; }
        public int UserTypeId { get; set; }
        public string UserName { get; set; }
        public string UserEmail { get; set; }
        public string UserPassword { get; set; }
        public int TenantId { get; set; }
        public int RoleId { get; set; }
        public bool IsFirstTime { get; set; }
        public string Company { get; set; }
    }
}
