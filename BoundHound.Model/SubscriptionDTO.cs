﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace BoundHound.Model
{
    public class SubscriptionDTO : EntityBase
    {
        public int SubscriptionID { get; set; }
        public string SubscriptionName { get; set; }
        public int SubscriptionDays { get; set; }
        
    }
}
