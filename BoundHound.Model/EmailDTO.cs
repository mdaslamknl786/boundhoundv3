﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace BoundHound.Model
{
    public class EmailDTO
    {
        public int emaillistId { get; set; }
        public string firstname { get; set; }
        public string lastname { get; set; }
        public string email { get; set; }
        public string phone { get; set; }
        public string mobile { get; set; }
        public string title { get; set; }
        public string company { get; set; }
        public string website { get; set; }
        public string location { get; set; }
        public string Url { get; set; }
        public string photourl { get; set; }
        public string profileid { get; set; }
        public int listId { get; set; }
        public string Degree { get; set; }


    }
}
