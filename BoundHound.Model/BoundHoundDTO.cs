﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace BoundHound.Model
{
    public class BoundHoundDTO
    {
        public int Id { get; }
        public string Name { get; }
        public string ServerName { get; }
        public int AccountType { get; }
        public int IsActive { get; }
        public DateTime? InActivatedDate { get; }
        public int DatabaseId { get; }
        public int? LogoFileId { get; set; }
        //public ContactDTO Contact { get; set; }
    }
}
