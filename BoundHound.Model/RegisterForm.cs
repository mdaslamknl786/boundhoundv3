﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace BoundHound.Model
{
    public class RegisterForm
    {
        public int RegisterId { get; set; }
        public string Email { get; set; }
        public string Password { get; set; }
        public string UserName { get; set; }
        public int TenantID { get; set; }
        public bool IsFirstTime { get; set; }
        public string Company { get; set; }
    }
}
