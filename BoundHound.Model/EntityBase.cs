﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace BoundHound.Model
{
    public class EntityBase
    {
        public DateTime CreatedTime { get; set; }
        public int CreatedStaffId { get; set; }
        public DateTime UpdatedTime { get; set; }
        public int UpdatedStaffId { get; set; }
    }
}
