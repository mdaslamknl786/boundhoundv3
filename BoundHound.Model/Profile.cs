﻿using System;
using System.Collections.Generic;
using System.Text;

namespace BoundHound.Model
{
    public class Profile
    {
        public int Id { get; set; }

        public string MaidenName { get; set; }
        public string FormattedName { get; set; }
        public string PhoneticFirstName { get; set; }

        public int ProfileID { get; set; }
        public string FirstName { get; set; }
        public string LastName { get; set; }
        public string Headline { get; set; }
        public string URL { get; set; }

    }
}
