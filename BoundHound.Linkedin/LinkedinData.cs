﻿using BoundHound.Core;
using BoundHound.Model;
using HtmlAgilityPack;
using OpenQA.Selenium;
using OpenQA.Selenium.Chrome;
using OpenQA.Selenium.Interactions;
using System;
using System.Collections.Generic;
using System.IO;
using System.Text.RegularExpressions;
using System.Threading;
using System.Web;

namespace BoundHound.Linkedin
{
    public class LinkedinData
    {
        ChromeDriver driver;
     
        public IEnumerable<EmailDTO> GetEmailList(int noOfConnections, UserInfo MyUserInfo)
        {
            string ProfileID;
            string URl;
            List<EmailDTO> emailInfo = new List<EmailDTO>();
            string patth = HttpContext.Current.ApplicationInstance.Server.MapPath("~/Bin");
            ////string chromedriverpath = Directory.GetCurrentDirectory();
            //string chromedriverpath = @"D:\MdAslam\MyProjects\Egrabber_SOW\Utils\2.35.chromedriver_win32";
            HtmlNodeCollection nodeCollection = null;
            ChromeOptions options = new ChromeOptions();
            //options.AddArguments("--disable-extensions");
            options.AddArgument("test-type");
            options.AddArgument("--ignore-certificate-errors");
            options.AddArgument("no-sandbox");
            options.AddArgument("--window-position=-32000,-32000");
            options.AddArgument("--start-maximized");
            //options.AddArgument("--headless");//hide browser
            //options.AddArgument("--silent");


            ChromeDriverService service = ChromeDriverService.CreateDefaultService(patth);
            using (driver = new ChromeDriver(service, options))
            {
                // Go to the home page
                driver.Navigate().GoToUrl("https://www.linkedin.com/uas/login");
                //driver.Manage().Window.Minimize();

                // Get User Name field, Password field and Login Button
                var userNameField = driver.FindElementByName("session_key");
                var userPasswordField = driver.FindElementById("session_password-login");
                //var loginButton = driver.FindElementByXPath("//input[@value='Login']");

                var loginButton = driver.FindElementById("btn-primary");

                //var loginButton = driver.FindElementByXPath("//div[@id='passwordNext']");

                // Type user name and password
                userNameField.SendKeys(MyUserInfo.Email);
                userPasswordField.SendKeys(MyUserInfo.Password);

                // and click the login button
                loginButton.Click();

                Thread.Sleep(2000);
                driver.Navigate().GoToUrl("https://www.linkedin.com/mynetwork/invite-connect/connections/");

                Thread.Sleep(1000);


                var result1 = driver.PageSource;
                File.WriteAllText(patth + "\\result.txt", result1);

                //DataScrapping

                string source = System.IO.File.ReadAllText(patth + "\\result.txt");


                HtmlAgilityPack.HtmlDocument htmlDocument = new HtmlAgilityPack.HtmlDocument();
                htmlDocument.LoadHtml(source);

                //HtmlNodeCollection nodeCollection = htmlDocument.DocumentNode
                //   .SelectNodes("//span[@class='mn-person-info__name Sans-17px-black-85%-semibold-dense']");

                //Thread.Sleep(1000);

                nodeCollection = htmlDocument.DocumentNode.SelectNodes("//div[@class='mn-person-info__details']");
                if (nodeCollection == null)
                {
                    nodeCollection = htmlDocument.DocumentNode.SelectNodes("//div[@class='mn-connection-card__details']");
                }
                foreach (HtmlNode item in nodeCollection)
                {
                    try
                    {

                        string aa = item.OuterHtml;
                        string[] aa1 = aa.Split();
                        string[] aa2 = aa1[4].Split(new string[] { "/", "-" }, StringSplitOptions.None);
                        if (aa2.Length > 4)
                        {
                            ProfileID = aa2[4];
                            URl = @"https://www.linkedin.com/" + aa2[1] + "/" + aa2[2] + "-" + aa2[3] + "-" + aa2[4] + "";
                        }
                        else
                        {
                            URl = @"https://www.linkedin.com/" + aa2[1] + "/" + aa2[2] + "";
                            ProfileID = aa2[3];
                        }
                        string Title = aa1[67] + " " + aa1[68] + " " + aa1[69] + " " + aa1[70] + " " + aa1[71] + " " + aa1[72] + " " + aa1[73] + " " + aa1[73];
                        string[] MainTitile = Title.Split();
                        Title = "";
                        for (int i = 0; i <= MainTitile.Length; i++)
                        {
                            try
                            {
                                if (MainTitile[i] == "at")
                                { break; }
                                else
                                {
                                    Title += MainTitile[i] + " ";
                                }
                            }
                            catch { }
                        }

                        Title = Title.Trim();
                        //Thread.Sleep(1000);
                        driver.Navigate().GoToUrl(URl);
                        Thread.Sleep(5000);

                        try
                        {
                            IList<IWebElement> links = driver.FindElements(By.TagName("Button"));
                            int count = 0;
                            foreach (var link in links)
                            {
                                try
                                {
                                    if (link.Text == "Show more\r\nSee more contact and personal info")
                                    {
                                        link.Click();
                                        IWebElement body = driver.FindElement(By.TagName("body"));
                                        IAction scrollDown = new Actions(driver)
                                            .MoveToElement(body, body.Size.Width - 100, 150) // position mouse over scrollbar
                                            .ClickAndHold()
                                            .MoveByOffset(0, 1000) // scroll down
                                            .Release()
                                            .Build();
                                        scrollDown.Perform();
                                    }
                                }
                                catch (Exception ex)
                                {
                                    var Message = ex.ToString();
                                }
                                count++;
                            }


                            //  var expand = driver.FindElement(By.CssSelector("contact-see-more-less link-without-visited-state"));

                            var result2 = driver.PageSource;

                            File.WriteAllText(patth + "\\Data.txt", result2);
                            string source1 = File.ReadAllText(patth + "\\Data.txt");

                            HtmlAgilityPack.HtmlDocument htmlDocument1 = new HtmlAgilityPack.HtmlDocument();
                            htmlDocument1.LoadHtml(source1);

                            HtmlNodeCollection nodeCollection1 = htmlDocument1.DocumentNode
                               .SelectNodes("//div[@class='pv-profile-section__section-info section-info']");

                            HtmlNodeCollection nodeCollection2 = htmlDocument1.DocumentNode
                                .SelectNodes("//section[@class='pv-profile-section pv-top-card-section artdeco-container-card ember-view']");

                            HtmlNodeCollection nodeCollection3 = htmlDocument1.DocumentNode
                                .SelectNodes("//h3[@class='pv-top-card-section__company Sans-17px-black-70% mb1 inline-block']");

                            HtmlNodeCollection nodeCollection4 = htmlDocument1.DocumentNode
                               .SelectNodes("//h2[@class='pv-top-card-section__headline Sans-19px-black-85%']");

                            HtmlNodeCollection nodeCollection5 = htmlDocument1.DocumentNode
                                        .SelectNodes("//a[@data-control-name='background_details_company']");

                            foreach (HtmlNode item1 in nodeCollection1)
                            {
                                string photourl = null; string companyname = null; string companyID = null;
                                try
                                {
                                    foreach (HtmlNode item2 in nodeCollection2)
                                    {
                                        string path = item2.OuterHtml;
                                        string[] path1 = path.Split(new string[] { "&quot" }, StringSplitOptions.None);
                                        path1 = path1[1].Split(new string[] { "amp", ";" }, StringSplitOptions.None);

                                        foreach (HtmlNode item4 in nodeCollection4)
                                        {
                                            string[] TT = item4.OuterHtml.Split();
                                            foreach (HtmlNode item3 in nodeCollection3)
                                            {
                                                string[] cm = item3.OuterHtml.Split();
                                                for (int i = 12; i < cm.Length; i++)
                                                {
                                                    if (cm[i] != "")
                                                    {
                                                        companyname += cm[i] + " ";
                                                    }
                                                    else
                                                    {
                                                        companyname = companyname.Trim();
                                                        if (companyname.EndsWith("."))
                                                        {
                                                            companyname = companyname.Substring(0, companyname.Length - 1);
                                                        }
                                                        break;
                                                    }
                                                }
                                                try
                                                {
                                                    foreach (HtmlNode item5 in nodeCollection5)
                                                    {
                                                        string[] TT1 = item5.OuterHtml.Split(new string[] { "\"" }, StringSplitOptions.None);
                                                        companyID = TT1[3];
                                                        if (Regex.IsMatch(companyID, "/company/") == true)
                                                        {
                                                            companyID = TT1[3];
                                                            companyID = "https://www.linkedin.com" + companyID;
                                                            break;
                                                        }
                                                        else
                                                        {
                                                            companyID = null;
                                                            break;
                                                        }
                                                    }
                                                }
                                                catch (Exception ex)
                                                {
                                                    var mm = ex.ToString();
                                                }

                                            }
                                        }
                                        photourl = path1[1] + path1[3] + path1[5];
                                    }
                                }
                                catch { }

                                string email = item1.XPath;
                                var mail = driver.FindElementByXPath(email).Text;
                                string ss = mail;
                                string[] ss1 = mail.Split();
                                string emailid = null; string Phone = null; string location = null; string mobile = null;
                                string website = null;

                                for (int a = 0; a < ss1.Length; a++)
                                {
                                    if (ss1[a] == "Email" || ss1[a] == "Phone" || ss1[a] == "Address")
                                    {
                                        if (ss1[a] == "Email")
                                        {
                                            a++; a++;
                                            emailid = ss1[a];
                                        }
                                        if (ss1[a] == "Address")
                                        {
                                            a++; a++;
                                            location = ss1[a] + " , " + ss1[a + 1];
                                        }
                                        if (ss1[a] == "Phone")
                                        {
                                            a++; a++;
                                            Phone = ss1[a];
                                            mobile = ss1[a];
                                        }
                                    }
                                }

                                if (companyID != null)
                                {
                                    driver.Navigate().GoToUrl(companyID);
                                    Thread.Sleep(2000);
                                    IList<IWebElement> links1 = driver.FindElements(By.TagName("Button"));
                                    foreach (var link1 in links1)
                                    {
                                        try
                                        {
                                            if (link1.Text == "See more\r\nSee more details about " + companyname + "")
                                            {
                                                link1.Click();
                                                break;
                                            }
                                        }
                                        catch (Exception ex) {
                                            var Message = ex.ToString();
                                        }
                                    }

                                    try
                                    {
                                        var result3 = driver.PageSource;
                                        File.WriteAllText(patth + "\\Data.txt", result3);
                                        string source2 = File.ReadAllText(patth + "\\Data.txt");

                                        HtmlAgilityPack.HtmlDocument htmlDocument2 = new HtmlAgilityPack.HtmlDocument();
                                        htmlDocument2.LoadHtml(source2);

                                        HtmlNodeCollection nodeCollection6 = htmlDocument2.DocumentNode
                                           .SelectNodes("//div[@class='org-about-company-module__company-page-url truncate Sans-15px-black-70% mb3']");
                                        foreach (HtmlNode item6 in nodeCollection6)
                                        {
                                            string[] web = item6.OuterHtml.Split(new string[] { "\"" }, StringSplitOptions.None);
                                            website = web[7];
                                        }
                                    }
                                    catch (Exception ex) {
                                        var Message = ex.ToString();
                                    }

                                }

                                emailInfo.Add(new EmailDTO { firstname = aa1[29], lastname = aa1[30], email = emailid, phone = Phone, mobile = mobile, title = Title, company = companyname, website = website, location = location, Url = URl.ToString(), photourl = photourl, profileid = ProfileID });

                                //MySqlCommand command = new MySqlCommand("insert into emaillist(firstname,lastname,email,phone,mobile,title,company,website,location,Url," +
                                //                                          "photourl)values('" + aa1[29] + "','" + aa1[30] + "','" + emailid + "','" + Phone + "','" + mobile + "','" + Title + "','" + companyname + "','" + website + "','" + location + "','" + URl + "','" + photourl + "')", conn);
                                // MySqlDataReader MyReader2;
                                //conn.Open();
                                //command.ExecuteReader();     
                                //conn.Close();
                            }



                        }
                        catch (Exception ex)
                        {
                            var Message = ex.ToString();
                        }
                        if (noOfConnections > 0)
                        {
                            if (emailInfo.Count == noOfConnections)
                            {
                                break;
                            }
                        }
                    }
                    catch (Exception exp)
                    {
                        var Message = exp.ToString();
                        continue;
                    }
                }

                driver.Close();
            }
            return emailInfo;
        }
        public IEnumerable<EmailDTO> GetConnections_Old(int noOfConnections, UserInfo MyUserInfo)
        {
            string ProfileID;
            string URl;
            List<EmailDTO> emailInfo = new List<EmailDTO>();
            string patth = HttpContext.Current.ApplicationInstance.Server.MapPath("~/Bin");
            HtmlNodeCollection nodeCollection = null;
            ChromeOptions options = new ChromeOptions();
            options.AddArguments("--disable-extensions");
            options.AddArgument("test-type");
            options.AddArgument("--ignore-certificate-errors");
            options.AddArgument("no-sandbox");
            options.AddArgument("--window-position=-32000,-32000");
            options.AddArgument("--start-maximized");
            //options.AddArgument("--silent");

            ChromeDriverService service = ChromeDriverService.CreateDefaultService(patth);
            using (driver = new ChromeDriver(service, options))
            {
                driver.Navigate().GoToUrl("https://www.linkedin.com/uas/login");
                var userNameField = driver.FindElementByName("session_key");
                var userPasswordField = driver.FindElementById("session_password-login");

                var loginButton = driver.FindElementById("btn-primary");

                userNameField.SendKeys(MyUserInfo.Email);
                userPasswordField.SendKeys(MyUserInfo.Password);

                // and click the login button
                loginButton.Click();

                Thread.Sleep(2000);
                driver.Navigate().GoToUrl("https://www.linkedin.com/mynetwork/invite-connect/connections/");
                Thread.Sleep(1000);
                var result1 = driver.PageSource;
                File.WriteAllText(patth + "\\result.txt", result1);
                string source = System.IO.File.ReadAllText(patth + "\\result.txt");

                HtmlAgilityPack.HtmlDocument htmlDocument = new HtmlAgilityPack.HtmlDocument();
                htmlDocument.LoadHtml(source);

                nodeCollection = htmlDocument.DocumentNode.SelectNodes("//div[@class='mn-person-info__details']");
                if (nodeCollection == null)
                {
                    nodeCollection = htmlDocument.DocumentNode.SelectNodes("//div[@class='mn-connection-card__details']");
                    
                }
                foreach (HtmlNode item in nodeCollection)
                {
                    try
                    {
                       
                        string aa = item.OuterHtml;

                        
                        string[] aa1 = aa.Split();
                        string[] aa2 = aa1[4].Split(new string[] { "/", "-" }, StringSplitOptions.None);
                        if (aa2.Length > 4)
                        {
                            ProfileID = aa2[2] + "-" + aa2[3] + "-" + aa2[4];
                            URl = @"https://www.linkedin.com/" + aa2[1] + "/" + aa2[2] + "-" + aa2[3] + "-" + aa2[4] + "";
                        }
                        else
                        {
                            URl = @"https://www.linkedin.com/" + aa2[1] + "/" + aa2[2] + "";
                            ProfileID = aa2[2];
                        }
                        string Title = aa1[67] + " " + aa1[68] + " " + aa1[69] + " " + aa1[70] + " " + aa1[71] + " " + aa1[72] + " " + aa1[73] + " " + aa1[73];
                        string[] MainTitile = Title.Split();
                        Title = "";
                        for (int i = 0; i <= MainTitile.Length; i++)
                        {
                            try
                            {
                                if (MainTitile[i] == "at")
                                { break; }
                                else
                                {
                                    Title += MainTitile[i] + " ";
                                }
                            }
                            catch { }
                        }
                        Title = MyUtils.RemoveHTMLTags(Title.Trim());
                        Title = Title.Trim();
                        emailInfo.Add(new EmailDTO { firstname = aa1[29], lastname = aa1[30], title = Title, Url = URl.ToString(), profileid = ProfileID });
                        //break;
                    }
                    catch(Exception exp) {
                        var Message = exp.ToString();
                    }
                    if (noOfConnections > 0)
                    {
                        if (emailInfo.Count == noOfConnections)
                        {
                            break;
                        }
                    }
                }
                driver.Close();
            }
            return emailInfo;
        }

        public IEnumerable<EmailDTO> GetConnections(int noOfConnections, UserInfo MyUserInfo)
        {
            List<EmailDTO> emailInfo = new List<EmailDTO>();
            string patth = HttpContext.Current.ApplicationInstance.Server.MapPath("~/Bin");
            HtmlNodeCollection nodeCollection = null;
            ChromeOptions options = new ChromeOptions();
            options.AddArguments("--disable-extensions");
            options.AddArgument("test-type");
            options.AddArgument("--ignore-certificate-errors");
            options.AddArgument("no-sandbox");
            options.AddArgument("--window-position=-32000,-32000");
            options.AddArgument("--start-maximized");
            //options.AddArgument("--silent");
            ChromeDriverService service = ChromeDriverService.CreateDefaultService(patth);
            using (driver = new ChromeDriver(service, options))
            {
                driver.Navigate().GoToUrl("https://www.linkedin.com/uas/login");
                var userNameField = driver.FindElementByName("session_key");
                var userPasswordField = driver.FindElementById("session_password-login");
                var loginButton = driver.FindElementById("btn-primary");
                userNameField.SendKeys(MyUserInfo.Email);
                userPasswordField.SendKeys(MyUserInfo.Password);
                // and click the login button
                loginButton.Click();
                Thread.Sleep(2000);
                driver.Navigate().GoToUrl("https://www.linkedin.com/mynetwork/invite-connect/connections/");
                Thread.Sleep(1000);
                try
                {
                    var txtarea = driver.FindElementByXPath("//h2[@class='Sans-26px-black-85% ph5 pt5']");
                    string[] aa = txtarea.Text.Split();
                    int total, count = 0, checkcount = 0;
                    if (noOfConnections != 0)
                    {
                        if (noOfConnections > 40)
                        {
                            count = noOfConnections / 7;
                            checkcount = 0;
                        }
                    }
                    else
                    {
                        total = Convert.ToInt32(aa[0]);
                        count = total / 7;
                        checkcount = 0;
                    }
                    if (noOfConnections > 10 || noOfConnections == 0)
                    {
                        for (int K = 0; K < count + 2; K++)
                        {
                            Actions act = new Actions(driver);
                            act.SendKeys(OpenQA.Selenium.Keys.PageDown).Build().Perform();
                            Thread.Sleep(1000);
                            IList<IWebElement> allcount = driver.FindElements(By.ClassName("list-style-none"));
                            if (checkcount == allcount.Count)
                            {
                                Thread.Sleep(2000);
                                Actions act1 = new Actions(driver);
                                act1.SendKeys(OpenQA.Selenium.Keys.Up).Build().Perform();
                                Thread.Sleep(3000);
                                count--;
                                K--;
                                continue;
                            }
                            checkcount = allcount.Count;
                            Thread.Sleep(3000);
                        }
                    }
                }
                catch { }
                Thread.Sleep(2000);
                var result1 = driver.PageSource;
                File.WriteAllText(patth + "\\result.txt", result1);
                string source = System.IO.File.ReadAllText(patth + "\\result.txt");
                HtmlAgilityPack.HtmlDocument htmlDocument = new HtmlAgilityPack.HtmlDocument();
                htmlDocument.LoadHtml(source);
                nodeCollection = htmlDocument.DocumentNode.SelectNodes("//li[@class='list-style-none']");
                for (int j = 0; j < nodeCollection.Count; j++)
                {
                    string Title = null; string ProfileID = null; string photourl = null;
                    string URl = null, firstname = null, lastname = null;
                    HtmlNode item = nodeCollection[j];
                    string[] aa = item.OuterHtml.Split();
                    try
                    {
                        for (int i = 0; i < aa.Length; i++)
                        {
                            if (aa[i] == "class=\"mn-connection-card")
                            {
                                i += 4;
                                string[] url = aa[i].Split(new string[] { "in/", "/" }, StringSplitOptions.None);
                                ProfileID = url[2];
                                URl = @"https://www.linkedin.com/in/" + ProfileID + "";
                            }
                            if (aa[i] == "style=\"background-image:")
                            {
                                i++;
                                string[] path1 = aa[i].Split(new string[] { "&quot" }, StringSplitOptions.None);
                                path1 = path1[1].Split(new string[] { "amp", ";" }, StringSplitOptions.None);
                                photourl = path1[1] + path1[3] + path1[5];
                            }
                            if (aa[i] == "class=\"mn-connection-card__name")
                            {
                                i += 9;
                                for (int C1 = i; C1 < aa.Length; C1++)
                                {
                                    if (aa[C1] == "")
                                    { break; }
                                    else
                                    {
                                        if (firstname == null)
                                        {
                                            firstname = aa[C1];
                                        }
                                        else
                                        {
                                            lastname += aa[C1] + " ";
                                        }
                                    }
                                }
                                lastname = lastname.Trim();
                            }
                            if (aa[i] == "class=\"mn-connection-card__occupation")
                            {
                                i += 9;
                                for (int C1 = i; C1 < aa.Length; C1++)
                                {
                                    if (aa[C1] == "at" || aa[C1] == "")
                                    { break; }
                                    else
                                    {
                                        Title += aa[C1] + " ";
                                    }
                                }
                                Title = Title.Trim();
                                break;
                            }
                        }
                        emailInfo.Add(new EmailDTO { firstname = firstname, lastname = lastname, title = Title, Url = URl.ToString(), profileid = ProfileID, photourl = photourl });
                    }
                    catch { }
                    if (noOfConnections > 0)
                    {
                        if (emailInfo.Count == noOfConnections)
                        {
                            break;
                        }
                    }
                }
                driver.Close();
            }
            return emailInfo;
        }

        public IEnumerable<EmailDTO> GetConnectionEmailByProfileId(string ProfileURL, UserInfo MyUserInfo)
        {
            List<EmailDTO> emailInfo = new List<EmailDTO>();
            string patth = HttpContext.Current.ApplicationInstance.Server.MapPath("~/Bin");

            ChromeOptions options = new ChromeOptions();
            options.AddArguments("--disable-extensions");
            options.AddArgument("test-type");
            options.AddArgument("--ignore-certificate-errors");
            options.AddArgument("no-sandbox");
            options.AddArgument("--window-position=-32000,-32000");
            options.AddArgument("--start-maximized");
            //options.AddArgument("--silent");

            ChromeDriverService service = ChromeDriverService.CreateDefaultService(patth);
            using (driver = new ChromeDriver(service, options))
            {
                // Go to the home page
                driver.Navigate().GoToUrl("https://www.linkedin.com/uas/login");
                var userNameField = driver.FindElementByName("session_key");
                var userPasswordField = driver.FindElementById("session_password-login");

                var loginButton = driver.FindElementById("btn-primary");

                userNameField.SendKeys(MyUserInfo.Email);
                userPasswordField.SendKeys(MyUserInfo.Password);

                loginButton.Click();

                Thread.Sleep(1000);
                driver.Navigate().GoToUrl(ProfileURL);
                Thread.Sleep(3000);

                try
                {
                    IList<IWebElement> links = driver.FindElements(By.TagName("Button"));
                    int count = 0;
                    foreach (var link in links)
                    {
                        try
                        {
                            if (link.Text == "Show more\r\nSee more contact and personal info")
                            {
                                link.Click();
                                IWebElement body = driver.FindElement(By.TagName("body"));
                                IAction scrollDown = new Actions(driver)
                                    .MoveToElement(body, body.Size.Width - 100, 150) // position mouse over scrollbar
                                    .ClickAndHold()
                                    .MoveByOffset(0, 1000) // scroll down
                                    .Release()
                                    .Build();
                                scrollDown.Perform();
                            }
                        }
                        catch (Exception ex)
                        {
                            var Message = ex.ToString();
                        }
                        count++;
                    }


                    //  var expand = driver.FindElement(By.CssSelector("contact-see-more-less link-without-visited-state"));

                    var result2 = driver.PageSource;

                    File.WriteAllText(patth + "\\Data.txt", result2);
                    string source1 = File.ReadAllText(patth + "\\Data.txt");

                    HtmlAgilityPack.HtmlDocument htmlDocument1 = new HtmlAgilityPack.HtmlDocument();
                    htmlDocument1.LoadHtml(source1);

                    HtmlNodeCollection nodeCollection1 = htmlDocument1.DocumentNode
                       .SelectNodes("//div[@class='pv-profile-section__section-info section-info']");

                    HtmlNodeCollection nodeCollection2 = htmlDocument1.DocumentNode
                        .SelectNodes("//section[@class='pv-profile-section pv-top-card-section artdeco-container-card ember-view']");

                    HtmlNodeCollection nodeCollection3 = htmlDocument1.DocumentNode
                        .SelectNodes("//h3[@class='pv-top-card-section__company Sans-17px-black-70% mb1 inline-block']");

                    HtmlNodeCollection nodeCollection4 = htmlDocument1.DocumentNode
                       .SelectNodes("//h2[@class='pv-top-card-section__headline Sans-19px-black-85%']");

                    HtmlNodeCollection nodeCollection5 = htmlDocument1.DocumentNode
                                .SelectNodes("//a[@data-control-name='background_details_company']");

                    foreach (HtmlNode item1 in nodeCollection1)
                    {
                        string photourl = null; string companyname = null; string companyID = null;
                        try
                        {
                            foreach (HtmlNode item2 in nodeCollection2)
                            {
                                string path = item2.OuterHtml;
                                string[] path1 = path.Split(new string[] { "&quot", ";" }, StringSplitOptions.None);

                                foreach (HtmlNode item4 in nodeCollection4)
                                {
                                    string[] TT = item4.OuterHtml.Split();
                                    foreach (HtmlNode item3 in nodeCollection3)
                                    {
                                        string[] cm = item3.OuterHtml.Split();
                                        for (int i = 12; i < cm.Length; i++)
                                        {
                                            if (cm[i] != "")
                                            {
                                                companyname += cm[i] + " ";
                                            }
                                            else
                                            {
                                                companyname = companyname.Trim();
                                                if (companyname.EndsWith("."))
                                                {
                                                    companyname = companyname.Substring(0, companyname.Length - 1);
                                                }
                                                break;
                                            }
                                        }
                                        try
                                        {
                                            foreach (HtmlNode item5 in nodeCollection5)
                                            {
                                                string[] TT1 = item5.OuterHtml.Split(new string[] { "\"" }, StringSplitOptions.None);
                                                companyID = TT1[3];
                                                if (Regex.IsMatch(companyID, "/company/") == true)
                                                {
                                                    companyID = TT1[3];
                                                    companyID = "https://www.linkedin.com" + companyID;
                                                    break;
                                                }
                                                else
                                                {
                                                    companyID = null;
                                                    break;
                                                }
                                            }
                                        }
                                        catch (Exception ex)
                                        {
                                            var Message = ex.ToString();
                                        }

                                    }
                                }
                                photourl = path1[3];
                            }
                        }
                        catch { }

                        string email = item1.XPath;
                        var mail = driver.FindElementByXPath(email).Text;
                        string ss = mail;
                        string[] ss1 = mail.Split();
                        string emailid = null; string Phone = null; string location = null; string mobile = null;
                        string website = null;

                        for (int a = 0; a < ss1.Length; a++)
                        {
                            if (ss1[a] == "Email" || ss1[a] == "Phone" || ss1[a] == "Address")
                            {
                                if (ss1[a] == "Email")
                                {
                                    a++; a++;
                                    emailid = ss1[a];
                                }
                                if (ss1[a] == "Address")
                                {
                                    a++; a++;
                                    location = ss1[a] + " , " + ss1[a + 1];
                                }
                                if (ss1[a] == "Phone")
                                {
                                    a++; a++;
                                    Phone = ss1[a];
                                    mobile = ss1[a];
                                }
                            }
                        }

                        if (companyID != null)
                        {
                            driver.Navigate().GoToUrl(companyID);
                            Thread.Sleep(2000);
                            IList<IWebElement> links1 = driver.FindElements(By.TagName("Button"));
                            foreach (var link1 in links1)
                            {
                                try
                                {
                                    if (link1.Text == "See more\r\nSee more details about " + companyname + "")
                                    {
                                        link1.Click();
                                        break;
                                    }
                                }
                                catch (Exception ex) {
                                    var Message = ex.ToString();
                                    continue;
                                }
                            }

                            try
                            {
                                var result3 = driver.PageSource;
                                File.WriteAllText(patth + "\\Data.txt", result3);
                                string source2 = File.ReadAllText(patth + "\\Data.txt");

                                HtmlAgilityPack.HtmlDocument htmlDocument2 = new HtmlAgilityPack.HtmlDocument();
                                htmlDocument2.LoadHtml(source2);

                                HtmlNodeCollection nodeCollection6 = htmlDocument2.DocumentNode
                                   .SelectNodes("//div[@class='org-about-company-module__company-page-url truncate Sans-15px-black-70% mb3']");
                                foreach (HtmlNode item6 in nodeCollection6)
                                {
                                    string[] web = item6.OuterHtml.Split(new string[] { "\"" }, StringSplitOptions.None);
                                    website = web[7];
                                }
                            }
                            catch (Exception ex) {
                                var Message = ex.ToString();
                            }
                        }
                        emailInfo.Add(new EmailDTO { firstname = null, lastname = null, email = emailid, phone = Phone, mobile = mobile, title = null, company = companyname, website = website, location = location, Url = null, photourl = photourl, profileid = null });

                    }
                }
                catch (Exception ex)
                {
                    var Message = ex.ToString();
                }
                driver.Close();
            }
            return emailInfo;
        }
        public IEnumerable<EmailDTO> GetConnectionEmailByProfileList(IEnumerable<EmailDTO> EmailDTOList, UserInfo MyUserInfo)
        {

            //profile = new List<string>();
            //profile.Clear();
            //profile.Add("anuj-joshi-7552482b/");
            //profile.Add("sruthi-reddy-35549660");
            //profile.Add("aravindthirumalai/");

            List <EmailDTO> emailInfo = new List<EmailDTO>();
            string patth = HttpContext.Current.ApplicationInstance.Server.MapPath("~/Bin");

            ChromeOptions options = new ChromeOptions();
            options.AddArguments("--disable-extensions");
            options.AddArgument("test-type");
            options.AddArgument("--ignore-certificate-errors");
            options.AddArgument("no-sandbox");
            options.AddArgument("--window-position=-32000,-32000");
            options.AddArgument("--start-maximized");
            //options.AddArgument("--silent");
            ChromeDriverService service = ChromeDriverService.CreateDefaultService(patth);
            using (driver = new ChromeDriver(service, options))
            {
                // Go to the home page
                driver.Navigate().GoToUrl("https://www.linkedin.com/uas/login");
                var userNameField = driver.FindElementByName("session_key");
                var userPasswordField = driver.FindElementById("session_password-login");

                var loginButton = driver.FindElementById("btn-primary");
                userNameField.SendKeys(MyUserInfo.Email);
                userPasswordField.SendKeys(MyUserInfo.Password);

                loginButton.Click();

                foreach (var item in EmailDTOList)
                {
                    Thread.Sleep(1000);
                    //driver.Navigate().GoToUrl("https://www.linkedin.com/in/" + item + "");
                    driver.Navigate().GoToUrl(item.Url);
                    Thread.Sleep(3000);

                    try
                    {
                        IList<IWebElement> links = driver.FindElements(By.TagName("Button"));
                        int count = 0;
                        foreach (var link in links)
                        {
                            try
                            {
                                if (link.Text == "Show more\r\nSee more contact and personal info")
                                {
                                    link.Click();
                                    IWebElement body = driver.FindElement(By.TagName("body"));
                                    IAction scrollDown = new Actions(driver)
                                        .MoveToElement(body, body.Size.Width - 100, 150) // position mouse over scrollbar
                                        .ClickAndHold()
                                        .MoveByOffset(0, 1000) // scroll down
                                        .Release()
                                        .Build();
                                    scrollDown.Perform();
                                }
                            }
                            catch (Exception ex)
                            {
                                var Message = ex.ToString();
                            }
                            count++;
                        }


                        //  var expand = driver.FindElement(By.CssSelector("contact-see-more-less link-without-visited-state"));

                        var result2 = driver.PageSource;

                        File.WriteAllText(patth + "\\Data.txt", result2);
                        string source1 = File.ReadAllText(patth + "\\Data.txt");

                        HtmlAgilityPack.HtmlDocument htmlDocument1 = new HtmlAgilityPack.HtmlDocument();
                        htmlDocument1.LoadHtml(source1);

                        HtmlNodeCollection nodeCollection1 = htmlDocument1.DocumentNode
                           .SelectNodes("//div[@class='pv-profile-section__section-info section-info']");

                        HtmlNodeCollection nodeCollection2 = htmlDocument1.DocumentNode
                            .SelectNodes("//section[@class='pv-profile-section pv-top-card-section artdeco-container-card ember-view']");

                        HtmlNodeCollection nodeCollection3 = htmlDocument1.DocumentNode
                            .SelectNodes("//h3[@class='pv-top-card-section__company Sans-17px-black-70% mb1 inline-block']");

                        HtmlNodeCollection nodeCollection4 = htmlDocument1.DocumentNode
                           .SelectNodes("//h2[@class='pv-top-card-section__headline Sans-19px-black-85%']");

                        HtmlNodeCollection nodeCollection5 = htmlDocument1.DocumentNode
                                    .SelectNodes("//a[@data-control-name='background_details_company']");

                        foreach (HtmlNode item1 in nodeCollection1)
                        {
                            string photourl = null; string companyname = null; string companyID = null;
                            try
                            {
                                foreach (HtmlNode item2 in nodeCollection2)
                                {
                                    string path = item2.OuterHtml;
                                    string[] path1 = path.Split(new string[] { "&quot", ";" }, StringSplitOptions.None);

                                    foreach (HtmlNode item4 in nodeCollection4)
                                    {
                                        string[] TT = item4.OuterHtml.Split();
                                        foreach (HtmlNode item3 in nodeCollection3)
                                        {
                                            string[] cm = item3.OuterHtml.Split();
                                            for (int i = 12; i < cm.Length; i++)
                                            {
                                                if (cm[i] != "")
                                                {
                                                    companyname += cm[i] + " ";
                                                }
                                                else
                                                {
                                                    companyname = companyname.Trim();
                                                    if (companyname.EndsWith("."))
                                                    {
                                                        companyname = companyname.Substring(0, companyname.Length - 1);
                                                    }
                                                    break;
                                                }
                                            }
                                            try
                                            {
                                                foreach (HtmlNode item5 in nodeCollection5)
                                                {
                                                    string[] TT1 = item5.OuterHtml.Split(new string[] { "\"" }, StringSplitOptions.None);
                                                    companyID = TT1[3];
                                                    if (Regex.IsMatch(companyID, "/company/") == true)
                                                    {
                                                        companyID = TT1[3];
                                                        companyID = "https://www.linkedin.com" + companyID;
                                                        break;
                                                    }
                                                    else
                                                    {
                                                        companyID = null;
                                                        break;
                                                    }
                                                }
                                            }
                                            catch (Exception ex)
                                            {
                                                var Message = ex.ToString();
                                            }

                                        }
                                    }
                                    photourl = path1[3];
                                }
                            }
                            catch { }

                            string email = item1.XPath;
                            var mail = driver.FindElementByXPath(email).Text;
                            string ss = mail;
                            string[] ss1 = mail.Split();
                            string emailid = null; string Phone = null; string location = null; string mobile = null;
                            string website = null;

                            for (int a = 0; a < ss1.Length; a++)
                            {
                                if (ss1[a] == "Email" || ss1[a] == "Phone" || ss1[a] == "Address")
                                {
                                    if (ss1[a] == "Email")
                                    {
                                        a++; a++;
                                        emailid = ss1[a];
                                    }
                                    if (ss1[a] == "Address")
                                    {
                                        a++; a++;
                                        location = ss1[a] + " , " + ss1[a + 1];
                                    }
                                    if (ss1[a] == "Phone")
                                    {
                                        a++; a++;
                                        Phone = ss1[a];
                                        mobile = ss1[a];
                                    }
                                }
                            }

                            if (companyID != null)
                            {
                                driver.Navigate().GoToUrl(companyID);
                                Thread.Sleep(2000);
                                IList<IWebElement> links1 = driver.FindElements(By.TagName("Button"));
                                foreach (var link1 in links1)
                                {
                                    try
                                    {
                                        if (link1.Text == "See more\r\nSee more details about " + companyname + "")
                                        {
                                            link1.Click();
                                            break;
                                        }
                                    }
                                    catch (Exception ex) { }
                                }

                                try
                                {
                                    var result3 = driver.PageSource;
                                    File.WriteAllText(patth + "\\Data.txt", result3);
                                    string source2 = File.ReadAllText(patth + "\\Data.txt");

                                    HtmlAgilityPack.HtmlDocument htmlDocument2 = new HtmlAgilityPack.HtmlDocument();
                                    htmlDocument2.LoadHtml(source2);

                                    HtmlNodeCollection nodeCollection6 = htmlDocument2.DocumentNode
                                       .SelectNodes("//div[@class='org-about-company-module__company-page-url truncate Sans-15px-black-70% mb3']");
                                    foreach (HtmlNode item6 in nodeCollection6)
                                    {
                                        string[] web = item6.OuterHtml.Split(new string[] { "\"" }, StringSplitOptions.None);
                                        website = web[7];
                                    }
                                }
                                catch (Exception ex) {
                                    var Message = ex.ToString();
                                }
                            }
                            emailInfo.Add(new EmailDTO { firstname = null, lastname = null, email = emailid, phone = Phone, mobile = mobile, title = null, company = companyname, website = website, location = location, Url = null, photourl = photourl, profileid = null });

                        }
                    }
                    catch (Exception ex)
                    {
                    }
                }
                driver.Close();
            }
            return emailInfo;
        }
        public bool SendMessage(string ProfileURL,string Message,UserInfo MyUserInfo)
        {
            try
            {
                //Get Profile URL


                List<string> list = new List<string>();
                list.Add(ProfileURL);


                ChromeOptions options = new ChromeOptions();
                options.AddArguments("--disable-extensions");
                options.AddArgument("test-type");
                options.AddArgument("--ignore-certificate-errors");
                options.AddArgument("no-sandbox");
                options.AddArgument("--window-position=-32000,-32000");
                options.AddArgument("--start-maximized");
                //options.AddArgument("--headless");//hide browser


                using (driver = new ChromeDriver())
                {

                    driver.Navigate().GoToUrl("https://www.linkedin.com/uas/login");
                    //driver.Manage().Window.Minimize();

                    // Get User Name field, Password field and Login Button
                    var userNameField = driver.FindElementByName("session_key");
                    var userPasswordField = driver.FindElementById("session_password-login");
                    //var loginButton = driver.FindElementByXPath("//input[@value='Login']");

                    var loginButton = driver.FindElementById("btn-primary");

                    //var loginButton = driver.FindElementByXPath("//div[@id='passwordNext']");

                    // Type user name and password
                    userNameField.SendKeys(MyUserInfo.Email);
                    userPasswordField.SendKeys(MyUserInfo.Password);

                    // and click the login button
                    loginButton.Click();

                    //Thread.Sleep(2000);
                    //https://www.linkedin.com/in/mohita-rustagi-1a90b941/
                    //driver.Navigate().GoToUrl("https://www.linkedin.com/in/mohd-aslam-71b32826/");


                    for (int i = 0; i < list.Count; i++)
                    {
                        //string url = "https://www.linkedin.com/in/" + list[i];
                        string url = list[i];


                        string[] url1 = list[i].Split(new string[] { "-", "/" }, StringSplitOptions.None);
                        driver.Navigate().GoToUrl(url);
                        Thread.Sleep(2000);
                        string name = null;
                        try
                        {
                            IList<IWebElement> links2 = driver.FindElements(By.TagName("h1"));
                            name = links2[0].Text;
                        }
                        catch { }

                        try
                        {
                            IList<IWebElement> links = driver.FindElements(By.TagName("Button"));
                            string Text = "Message\r\nSend a message to " + name;
                            Text = Text.TrimEnd();
                            foreach (var link in links)
                            {
                                if (link.Text == Text)
                                {

                                    link.Click();
                                    Thread.Sleep(1000);
                                    IList<IWebElement> links1 = driver.FindElements(By.TagName("Button"));
                                    foreach (var link1 in links1)
                                    {
                                        if (link1.Text == "Send")
                                        {
                                            //string body = "Hi " + name;
                                            string body =Message;
                                            //string body = createEmailBody("Hi " + name, "Please Check your Account information", "Hello....",name);
                                            var txtarea = driver.FindElementByName("message");
                                            txtarea.SendKeys(body);
                                            Thread.Sleep(1000);
                                            link1.Click();
                                            //link1.SendKeys("Hello");
                                            Thread.Sleep(1000);
                                            break;
                                        }
                                    }
                                    break;
                                }
                            }

                        }

                        catch (Exception ex) {
                            throw ex;
                        }
                    }
                }
                return true;
            }
            catch (Exception ex) {
                throw ex;
            }
            

        }
        public string UppercaseFirst(string s)
        {
            char[] a = s.ToCharArray();
            a[0] = char.ToUpper(a[0]);
            return new string(a);
        }


        public IEnumerable<EmailDTO> SendInvitations_Old(string SearchStr, string ConnectionType, UserInfo MyUserInfo)
        {
            string patth = HttpContext.Current.ApplicationInstance.Server.MapPath("~/Bin");
            List<EmailDTO> emailInfo = new List<EmailDTO>();
            //List<Data> emailInfo = new List<Data>();
            //button3.Enabled = false;
            ChromeOptions options = new ChromeOptions();
            options.AddArguments("--disable-extensions");
            options.AddArgument("test-type");
            options.AddArgument("--ignore-certificate-errors");
            options.AddArgument("no-sandbox");
            options.AddArgument("--window-position=-32000,-32000");
            options.AddArgument("--start-maximized");
            //options.AddArgument("--headless");//hide browser

            using (driver = new ChromeDriver())
            {
                    // Go to the home page
                    driver.Navigate().GoToUrl("https://www.linkedin.com/uas/login");
                    //driver.Manage().Window.Minimize();

                    // Get User Name field, Password field and Login Button
                    var userNameField = driver.FindElementByName("session_key");
                    var userPasswordField = driver.FindElementById("session_password-login");
                    //var loginButton = driver.FindElementByXPath("//input[@value='Login']");

                    var loginButton = driver.FindElementById("btn-primary");
                    // Type user name and password
                    userNameField.SendKeys(MyUserInfo.Email);
                    userPasswordField.SendKeys(MyUserInfo.Password);

                    // and click the login button
                    loginButton.Click();

                    Thread.Sleep(2000);
                    driver.Navigate().GoToUrl("https://www.linkedin.com/search/results/people/?origin=DISCOVER_FROM_SEARCH_HOME&page=1");
                    Thread.Sleep(2000);
                    string location = "Hyderabad";
                    string connection = "2nd";
                    string CurrentCompanies = "Sparsh Communication";

                try
                {

                    //IList<IWebElement> btncount = driver.FindElements(By.ClassName("button-primary-medium facet-collection-list__apply-button"));
                    //foreach (var locationbutton in btncount)
                    //{
                    IList<IWebElement> links = driver.FindElements(By.TagName("Button"));
                    //foreach (var link in links)
                    //{ 
                    if (links[10].Text == "Expand Locations facet\r\nLocations")
                    {
                        links[10].Click();

                        var txtarea1 = driver.FindElementByXPath("//input[@placeholder='Add a filter']");//.SendKeys(location + " Area, India");
                        txtarea1.SendKeys(location + " Area, India");
                        //txtarea1.SendKeys(OpenQA.Selenium.Keys.Down);
                        Thread.Sleep(1000);
                        txtarea1.SendKeys(OpenQA.Selenium.Keys.Enter);
                        links[12].Click();
                        //var locationbutton = driver.FindElementByXPath("//button[@class='button-primary-medium facet-collection-list__apply-button']");
                        //btncount[].Click();
                        Thread.Sleep(2000);
                        //break;
                    }

                    //}
                    IList<IWebElement> links1 = driver.FindElements(By.TagName("Button"));
                    //foreach (var link in links1)
                    //{
                    if (links1[13].Text == "Expand Connections facet\r\nConnections")
                    {
                        try
                        {
                            links1[13].Click();
                            if (connection ==ConnectionType)
                            {
                                var txtarea2 = driver.FindElementByXPath("//input[@value='F']");
                                txtarea2.SendKeys(OpenQA.Selenium.Keys.Space);
                                //txtarea2.SendKeys(OpenQA.Selenium.Keys.Enter);

                            }
                            if (connection == "2nd")
                            {
                                var txtarea2 = driver.FindElementByXPath("//input[@value='S']");
                                txtarea2.SendKeys(OpenQA.Selenium.Keys.Space);
                                //txtarea2.SendKeys(OpenQA.Selenium.Keys.Enter);
                            }
                            if (connection == ConnectionType)
                            {
                                var txtarea2 = driver.FindElementByXPath("//input[@value='O']");
                                txtarea2.SendKeys(OpenQA.Selenium.Keys.Space);
                                //txtarea2.SendKeys(OpenQA.Selenium.Keys.Enter);
                            }
                            links1[15].Click();
                        }
                        catch { }

                        //var locationbutton1 = driver.FindElementByXPath("//button[@class='button-primary-medium facet-collection-list__apply-button']");
                        //btncount[2].Click();
                    }

                    IWebElement body = driver.FindElement(By.TagName("body"));
                    IAction scrollDown = new Actions(driver)
                        .MoveToElement(body, body.Size.Width - 100, 150) // position mouse over scrollbar
                        .ClickAndHold()
                        .MoveByOffset(0, 1000) // scroll down
                        .Release()
                        .Build();
                    scrollDown.Perform();

                    Thread.Sleep(1000);
                    var result1 = driver.PageSource;
                    File.WriteAllText(patth + "\\result.txt", result1);
                    string source = System.IO.File.ReadAllText(patth + "\\result.txt");
                    HtmlAgilityPack.HtmlDocument htmlDocument = new HtmlAgilityPack.HtmlDocument();
                    htmlDocument.LoadHtml(source);
                    HtmlNodeCollection nodeCollection = htmlDocument.DocumentNode
                                                        .SelectNodes("//p[@class='subline-level-1 Sans-15px-black-85% search-result__truncate']");
                    HtmlNodeCollection nodeImage = htmlDocument.DocumentNode
                                                        .SelectNodes("//div[@class='search-result__image-wrapper']");
                    HtmlNodeCollection nodebutton = htmlDocument.DocumentNode
                                                        .SelectNodes("//div[@class='search-result__actions']");

                    HtmlNodeCollection nodeUrl = htmlDocument.DocumentNode
                                                        .SelectNodes("//div[@class='search-result__info pt3 pb4 ph0']");

                    try
                    {
                        for (int i = 0; i < nodeImage.Count; i++)
                        //foreach (HtmlNode item in nodeImage)
                        {
                            HtmlNode item = nodeImage[i];
                            string FullName = null; string URl = null; string Photo = null; string btnconnect = null; string profile = null; ;
                            try
                            {
                                string[] aa = item.OuterHtml.Split(new string[] { "src", "alt", "class" }, StringSplitOptions.None);
                                //string[] aa1 = aa.Split(new string[] { "alt=","\\" }, StringSplitOptions.None);
                                string[] aa2 = aa[5].Split(new string[] { "/", "\"" }, StringSplitOptions.None);
                                FullName = aa2[1].TrimEnd();
                                string[] name1 = FullName.Split();
                                aa2 = aa[6].Split(new string[] { "\"", "amp", ";" }, StringSplitOptions.None);
                                if (aa2.Length == 7)
                                    Photo = aa2[1] + aa2[3] + aa2[5];
                                else
                                    Photo = aa2[1] + aa2[2];
                                for (int a = i; a < nodebutton.Count; a++)
                                //foreach (HtmlNode item1 in nodebutton)
                                {
                                    HtmlNode item1 = nodebutton[a];
                                    try
                                    {
                                        string[] aaa2 = item1.OuterHtml.Split(new string[] { "button" }, StringSplitOptions.None);
                                        string[] a2 = aaa2[2].Split(new string[] { "/", "\r\n" }, StringSplitOptions.None);
                                        //weather connection is connected or not 
                                        //if inMail we can't connect
                                        //Invite sent we can connect invitation sent but not connected waiting for connection approval
                                        // Connect we can able to connect and sent invitation message
                                        //btnconnect will get this all above information

                                        btnconnect = a2[1].Trim();
                                        for (int c = a; c < nodeCollection.Count; c++)
                                        //foreach (HtmlNode item2 in nodeCollection)
                                        {
                                            HtmlNode item2 = nodeCollection[c];
                                            try
                                            {
                                                string[] aa3 = item2.OuterHtml.Split(new string[] { "/", "\r\n" }, StringSplitOptions.None);
                                                profile = aa3[1].Trim();
                                                string[] bb = profile.Split();
                                                profile = "";
                                                for (int k = 0; k <= bb.Length; k++)
                                                {
                                                    try
                                                    {
                                                        if (bb[k] == "at")
                                                        { break; }
                                                        else
                                                        {
                                                            profile += bb[k] + " ";
                                                        }
                                                    }
                                                    catch { }
                                                }
                                                profile = profile.Trim();
                                                for (int j = c; j < nodeCollection.Count; j++)
                                                //foreach (HtmlNode item4 in nodeUrl)
                                                {
                                                    HtmlNode item4 = nodeUrl[j];
                                                    try
                                                    {
                                                        string[] aa4 = item4.OuterHtml.Split(new string[] { "/", "\r\n", "href" }, StringSplitOptions.None);
                                                        URl = @"https://www.linkedin.com/in/" + aa4[4];
                                                        break;
                                                    }
                                                    catch { }
                                                    // break;
                                                }


                                            }
                                            catch { }
                                            break;
                                        }
                                    }
                                    catch { }
                                    break;
                                }
                                emailInfo.Add(new EmailDTO { emaillistId = 0, firstname = name1[0], lastname = name1[1], email = "", phone = "", mobile = "", title = profile, company = "", website = "", location = "", Url = URl.ToString(), photourl = Photo, profileid = "", Degree = "" });

                            }
                            catch (Exception ex) { }
                        }
                    }
                    catch (Exception ex) { }

                    //Sending invite code

                    //IList<IWebElement> name1 = driver.FindElements(By.TagName("h3"));
                    //for (int i = 4; i < name1.Count; i++)
                    //{
                    //    try
                    //    {
                    //        string[] Fullname = name1[i].Text.Split(new string[] { "\r\n" }, StringSplitOptions.None);
                    //        string Btnname = "Connect with " + Fullname[0];
                    //        var check = driver.FindElementByXPath("//button[@aria-label='" + Btnname + "']");
                    //        check.Click();

                    //        var send = driver.FindElementByXPath("//button[@class='button-primary-large ml1']");
                    //        send.Click();

                    //    }
                    //    catch (Exception ex) { }
                    //}

                    //var name = driver.FindElementByXPath("//h3[@class='actor-name-with-distance single-line-truncate ember-view']");


                }
                catch (Exception ex)
                {
                    throw ex;
                }
                return emailInfo;
                    //Thread.Sleep(1000);

            }
        }

        public IEnumerable<EmailDTO> SendInvitations(string SearchStr, string ConnectionType, UserInfo MyUserInfo)
        {
            string patth = HttpContext.Current.ApplicationInstance.Server.MapPath("~/Bin");
            List<EmailDTO> emailInfo = new List<EmailDTO>();
            //List<Data> emailInfo = new List<Data>();
            //button3.Enabled = false;
            ChromeOptions options = new ChromeOptions();
            options.AddArguments("--disable-extensions");
            options.AddArgument("test-type");
            options.AddArgument("--ignore-certificate-errors");
            options.AddArgument("no-sandbox");
            options.AddArgument("--window-position=-32000,-32000");
            options.AddArgument("--start-maximized");
            //options.AddArgument("--headless");//hide browser
            using (driver = new ChromeDriver())
            {
                // Go to the home page
                driver.Navigate().GoToUrl("https://www.linkedin.com/uas/login");
                //driver.Manage().Window.Minimize();
                // Get User Name field, Password field and Login Button
                var userNameField = driver.FindElementByName("session_key");
                var userPasswordField = driver.FindElementById("session_password-login");
                //var loginButton = driver.FindElementByXPath("//input[@value='Login']");
                var loginButton = driver.FindElementById("btn-primary");
                // Type user name and password
                userNameField.SendKeys(MyUserInfo.Email);
                userPasswordField.SendKeys(MyUserInfo.Password);
                // and click the login button
                loginButton.Click();
                Thread.Sleep(2000);
                driver.Navigate().GoToUrl("https://www.linkedin.com/search/results/people/?origin=DISCOVER_FROM_SEARCH_HOME&page=1");
                Thread.Sleep(2000);
                //string CurrentCompanies = "Sparsh Communication";
                try
                {
                    int linkcount = 0;
                    IList<IWebElement> links = driver.FindElements(By.TagName("Button"));
                    //for(int link=0;link<links.Count;link++)
                    foreach (var link in links)
                    {

                        if (link.Text == "Expand Locations facet\r\nLocations")
                        {
                            link.Click();
                            var txtarea1 = driver.FindElementByXPath("//input[@placeholder='Add a filter']");
                            txtarea1.SendKeys(SearchStr + " Area, India");
                            Thread.Sleep(1000);
                            txtarea1.SendKeys(OpenQA.Selenium.Keys.Enter);
                            linkcount += 2;
                            links[linkcount].Click();
                            Thread.Sleep(2000);
                            break;
                        }
                        linkcount++;
                    }
                    linkcount = 0;
                    IList<IWebElement> links1 = driver.FindElements(By.TagName("Button"));
                    foreach (var link in links1)
                    {
                        if (link.Text == "Expand Connections facet\r\nConnections")
                        {
                            try
                            {
                                link.Click();
                                if (ConnectionType == "1st")
                                {
                                    var txtarea2 = driver.FindElementByXPath("//input[@value='F']");
                                    txtarea2.SendKeys(OpenQA.Selenium.Keys.Space);
                                }
                                if (ConnectionType == "2nd")
                                {
                                    var txtarea2 = driver.FindElementByXPath("//input[@value='S']");
                                    txtarea2.SendKeys(OpenQA.Selenium.Keys.Space);
                                }
                                if (ConnectionType == "3rd")
                                {
                                    var txtarea2 = driver.FindElementByXPath("//input[@value='O']");
                                    txtarea2.SendKeys(OpenQA.Selenium.Keys.Space);
                                }
                                linkcount += 2;
                                links1[15].Click();
                                break;
                            }
                            catch { }
                        }
                        linkcount++;
                    }
                    for (int loop = 0; loop < 1; loop++)
                    {
                        Thread.Sleep(1000);
                        Actions act = new Actions(driver);
                        act.SendKeys(OpenQA.Selenium.Keys.PageDown).Build().Perform();
                    }
                    Thread.Sleep(1000);
                    var result1 = driver.PageSource;
                    File.WriteAllText(patth + "\\result.txt", result1);
                    string source = System.IO.File.ReadAllText(patth + "\\result.txt");
                    HtmlAgilityPack.HtmlDocument htmlDocument = new HtmlAgilityPack.HtmlDocument();
                    htmlDocument.LoadHtml(source);
                    HtmlNodeCollection nodeCollection = htmlDocument.DocumentNode
                                                        .SelectNodes("//p[@class='subline-level-1 Sans-15px-black-85% search-result__truncate']");
                    HtmlNodeCollection nodeImage = htmlDocument.DocumentNode
                                                        .SelectNodes("//div[@class='search-result__image-wrapper']");
                    HtmlNodeCollection nodebutton = htmlDocument.DocumentNode
                                                        .SelectNodes("//div[@class='search-result__actions']");
                    HtmlNodeCollection nodeUrl = htmlDocument.DocumentNode
                                                        .SelectNodes("//div[@class='search-result__info pt3 pb4 ph0']");
                    try
                    {
                        for (int a = 0; a < nodebutton.Count; a++)
                        {
                            string firstname = null; string lastname = null; string URl = null; string Photo = null; string profile = null;
                            HtmlNode item1 = nodebutton[a];
                            try
                            {
                                string[] aaa2 = item1.OuterHtml.Split();
                                for (int D = 0; D < aaa2.Length; D++)
                                {
                                    if (aaa2[D] == "Connect")
                                    {
                                        for (int i = a; i < nodeImage.Count; i++)
                                        {
                                            HtmlNode item = nodeImage[i];
                                            try
                                            {
                                                string[] aa = item.OuterHtml.Split(new string[] { "src", "alt", "class" }, StringSplitOptions.None);
                                                string[] aa2 = aa[5].Split(new string[] { "/", "\"" }, StringSplitOptions.None);
                                                firstname = aa2[1].TrimEnd();
                                                string[] name1 = firstname.Split(); firstname = null;
                                                for (int C1 = 0; C1 < name1.Length; C1++)
                                                {
                                                    if (name1[C1] == "")
                                                    { break; }
                                                    else
                                                    {
                                                        if (firstname == null)
                                                        {
                                                            firstname = name1[C1];
                                                        }
                                                        else
                                                        {
                                                            lastname += name1[C1] + " ";
                                                        }
                                                    }
                                                }
                                                lastname = lastname.Trim();
                                                try
                                                {
                                                    aa2 = aa[6].Split(new string[] { "\"", "amp", ";" }, StringSplitOptions.None);
                                                    if (aa2.Length == 7)
                                                        Photo = aa2[1] + aa2[3] + aa2[5];
                                                    else
                                                        Photo = aa2[1] + aa2[2];
                                                }
                                                catch { }
                                                for (int c = i; c < nodeCollection.Count; c++)
                                                {
                                                    HtmlNode item2 = nodeCollection[c];
                                                    try
                                                    {
                                                        string[] aa3 = item2.OuterHtml.Split(new string[] { "/", "\r\n" }, StringSplitOptions.None);
                                                        profile = aa3[1].Trim();
                                                        string[] bb = profile.Split();
                                                        profile = "";
                                                        for (int k = 0; k <= bb.Length; k++)
                                                        {
                                                            try
                                                            {
                                                                if (bb[k] == "at")
                                                                { break; }
                                                                else
                                                                {
                                                                    profile += bb[k] + " ";
                                                                }
                                                            }
                                                            catch { }
                                                        }
                                                        profile = profile.Trim();
                                                        for (int j = c; j < nodeUrl.Count; j++)
                                                        {
                                                            HtmlNode item4 = nodeUrl[j];
                                                            try
                                                            {
                                                                string[] aa4 = item4.OuterHtml.Split(new string[] { "/", "\r\n", "href" }, StringSplitOptions.None);
                                                                for (int M = 0; M < aa4.Length; M++)
                                                                {
                                                                    if (aa4[M] == "in")
                                                                    {
                                                                        M++;
                                                                        URl = @"https://www.linkedin.com/in/" + aa4[M];
                                                                        break;
                                                                    }
                                                                }
                                                            }
                                                            catch { }
                                                            break;
                                                        }
                                                    }
                                                    catch { }
                                                    break;
                                                }
                                            }
                                            catch { }
                                            break;
                                        }
                                        emailInfo.Add(new EmailDTO { emaillistId = 0, firstname = firstname, lastname = lastname, email = "", phone = "", mobile = "", title = profile, company = "", website = "", location = "", Url = URl.ToString(), photourl = Photo, profileid = "", Degree = "" });
                                        break;
                                    }
                                }

                            }
                            catch { }
                        }
                    }
                    catch (Exception ex) { }
                }
                catch (Exception ex)
                {
                    throw ex;
                }
                return emailInfo;
            }
        }

        public IEnumerable<SendInvitationDTO> ClickSendInvitaion(List<String> ProfileURl, string[] connectionid, string Message, UserInfo MyUserInfo)
        {
            List<SendInvitationDTO> SendinvitationInfo = new List<SendInvitationDTO>();
            ChromeOptions options = new ChromeOptions();
            options.AddArguments("--disable-extensions");
            options.AddArgument("test-type");
            options.AddArgument("--ignore-certificate-errors");
            options.AddArgument("no-sandbox");
            options.AddArgument("--window-position=-32000,-32000");
            options.AddArgument("--start-maximized");
            //options.AddArgument("--headless");//hide browser


            using (driver = new ChromeDriver())
            {
                // Go to the home page
                driver.Navigate().GoToUrl("https://www.linkedin.com/uas/login");
                //driver.Manage().Window.Minimize();

                // Get User Name field, Password field and Login Button
                var userNameField = driver.FindElementByName("session_key");
                var userPasswordField = driver.FindElementById("session_password-login");
                //var loginButton = driver.FindElementByXPath("//input[@value='Login']");

                var loginButton = driver.FindElementById("btn-primary");
                // Type user name and password
                userNameField.SendKeys(MyUserInfo.Email);
                userPasswordField.SendKeys(MyUserInfo.Password);

                // and click the login button
                loginButton.Click();

                Thread.Sleep(2000);

                for (int i = 0; i < ProfileURl.Count; i++)
                {
                    driver.Navigate().GoToUrl(ProfileURl[i]);
                    Thread.Sleep(2000);
                    string name = null;
                    try
                    {
                        IList<IWebElement> links2 = driver.FindElements(By.TagName("h1"));
                        name = links2[0].Text;
                    }
                    catch { }

                    try
                    {
                        IList<IWebElement> links = driver.FindElements(By.TagName("Button"));
                        foreach (var link in links)
                        {
                            if (link.Text == "Pending\r\nAn invitation has been sent to " + name + "")
                            {
                                SendinvitationInfo.Add(new SendInvitationDTO { connectionid = Convert.ToInt32(connectionid[i]), Message = "", lastUpdate = DateTime.Now, userId = 1, isActive = 1 });
                                break;
                            }
                            if (link.Text == "Connect\r\nConnect with " + name + "")
                            {
                                link.Click();
                                Thread.Sleep(1000);
                                IList<IWebElement> links1 = driver.FindElements(By.TagName("Button"));
                                foreach (var link1 in links1)
                                {
                                    if (link1.Text == "Add a note")
                                    {
                                        link1.Click();
                                        Thread.Sleep(1000);
                                        string body = "Hi " + name + "\r\n" + ". This is Test Mail.Please Don't reply..";
                                        var txtarea = driver.FindElementByName("message");
                                        txtarea.SendKeys(body);
                                        Thread.Sleep(1000);
                                        IList<IWebElement> links2 = driver.FindElements(By.TagName("Button"));
                                        foreach (var link2 in links2)
                                        {
                                            if (link2.Text == "Send invitation")
                                            {
                                                link2.Click();
                                                Thread.Sleep(1000);
                                                SendinvitationInfo.Add(new SendInvitationDTO { connectionid = Convert.ToInt32(connectionid[i]), Message = body, lastUpdate = DateTime.Now, userId = 1, isActive = 1 });
                                                break;
                                            }
                                        }
                                        break;
                                    }

                                }
                            }
                        }
                    }
                    catch (Exception ex) { }
                }
                return SendinvitationInfo;
            }
        }

        public IEnumerable<SendInvitationDTO> FindStatusofInvitationById(List<String> ProfileURl, string[] connectionid, string Message, UserInfo MyUserInfo)
        {
            List<SendInvitationDTO> SendinvitationInfo = new List<SendInvitationDTO>();
            ChromeOptions options = new ChromeOptions();
            options.AddArguments("--disable-extensions");
            options.AddArgument("test-type");
            options.AddArgument("--ignore-certificate-errors");
            options.AddArgument("no-sandbox");
            options.AddArgument("--window-position=-32000,-32000");
            options.AddArgument("--start-maximized");
            //options.AddArgument("--headless");//hide browser


            using (driver = new ChromeDriver())
            {
                // Go to the home page
                driver.Navigate().GoToUrl("https://www.linkedin.com/uas/login");
                //driver.Manage().Window.Minimize();

                // Get User Name field, Password field and Login Button
                var userNameField = driver.FindElementByName("session_key");
                var userPasswordField = driver.FindElementById("session_password-login");
                //var loginButton = driver.FindElementByXPath("//input[@value='Login']");

                var loginButton = driver.FindElementById("btn-primary");
                // Type user name and password
                userNameField.SendKeys(MyUserInfo.Email);
                userPasswordField.SendKeys(MyUserInfo.Password);

                // and click the login button
                loginButton.Click();

                Thread.Sleep(2000);

                for (int i = 0; i < ProfileURl.Count; i++)
                {
                    driver.Navigate().GoToUrl(ProfileURl[i]);
                    Thread.Sleep(2000);
                    string name = null;
                    try
                    {
                        IList<IWebElement> links2 = driver.FindElements(By.TagName("h1"));
                        name = links2[0].Text;
                    }
                    catch { }

                    try
                    {
                        IList<IWebElement> links = driver.FindElements(By.TagName("Button"));
                        foreach (var link in links)
                        {
                            if (link.Text == "Pending\r\nAn invitation has been sent to " + name + "")
                            {
                                SendinvitationInfo.Add(new SendInvitationDTO { connectionid = Convert.ToInt32(connectionid[i]), Message = "", lastUpdate = DateTime.Now, userId = 1, isActive = 1 });
                                break;
                            }
                        }
                    }
                    catch (Exception ex) { }
                }
                return SendinvitationInfo;
            }
        }

    }
}
