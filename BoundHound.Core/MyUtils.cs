﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace BoundHound.Core
{
    public static class MyUtils
    {
        public static string RemoveHTMLTags(string InputValue)
        {
            string OutputValue = "";

            // regex which match tags
            System.Text.RegularExpressions.Regex rx = new System.Text.RegularExpressions.Regex("<[^>]*>");

            // replace all matches with empty strin
            OutputValue = rx.Replace(InputValue, "");

            return OutputValue;
        }
    }
}
