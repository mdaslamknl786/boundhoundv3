
import {JsonObject, JsonProperty} from "json2typescript";

@JsonObject
export class EmailListExport {
 
    // This maps the value of the JSON key "countryName" to the class property "name".
    // If the JSON value is not of type string (or missing), there will be an exception.
    @JsonProperty("emaillistId", Number)
    emaillistId:number;

    @JsonProperty("firstname", String)
    firstname:string;

    @JsonProperty("lastname", String)
    lastname:string;

    @JsonProperty("email", String)
    email:string;

    @JsonProperty("phone", String)
    phone:string;

    @JsonProperty("mobile", String)
    mobile:string;

    @JsonProperty("title", String)
    title:string;

    @JsonProperty("company", String)
    company:string;

    @JsonProperty("website", String)
    website:string;

    @JsonProperty("location", String)
    location:string;

    @JsonProperty("Url", String)
    Url:string;

    @JsonProperty("photourl", String)
    photourl:string;
  
    // This maps the value of the JSON key "cities" to the class property "cities".
    // If the JSON value is not of type array object (or missing), there will be an exception.
    // There will be an exception too if the objects in the array do not match the class "City".
    // @JsonProperty("SampleDataList", [SampleData])
    // SampleDataList: SampleData[] = undefined;
    
}