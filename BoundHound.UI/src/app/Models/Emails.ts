export class Emails
{
    emaillistId:number;
    firstname:string;
    lastname:string;
    email:string;
    phone:string;
    mobile:string;
    title:string;
    company:string;
    website:string;
    location:string;
    Url:string;
    photourl:string;
}


export interface Email
{
    emaillistId:number;
    firstname:string;
    lastname:string;
    email:string;
    phone:string;
    mobile:string;
    title:string;
    company:string;
    website:string;
    location:string;
    Url:string;
    photourl:string;
    profileid:string;
}

export class ExportEmails
{
    firstname:string;
    lastname:string;
    email:string;
    phone:string;
    mobile:string;
    title:string;
    company:string;
    website:string;
    location:string;
    Url:string;
    photourl:string;
    profileid:string;
}