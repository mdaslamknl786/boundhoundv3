
import {JsonObject, JsonProperty} from "json2typescript";
import {SampleData} from "./SampleData";

@JsonObject
export class SampleDataJSON {
 
    // This maps the value of the JSON key "countryName" to the class property "name".
    // If the JSON value is not of type string (or missing), there will be an exception.
    
   @JsonProperty("Min", String)
   Min: number;

   @JsonProperty("Max", String)
   Max: string;

   @JsonProperty("ClientID", String)
   clientID: string;

   @JsonProperty("LowHigh", String)
   LowHigh:number;

   @JsonProperty("Month", String)
   Month: string;

   @JsonProperty("StateID", String)
   StateID:string;

   @JsonProperty("SegmentID", String)
   SegmentID:number;

   @JsonProperty("CollateralID", String)
   CollateralID:number;

    @JsonProperty("ClientName", String)
    name:string;

    // This maps the value of the JSON key "cities" to the class property "cities".
    // If the JSON value is not of type array object (or missing), there will be an exception.
    // There will be an exception too if the objects in the array do not match the class "City".
    // @JsonProperty("SampleDataList", [SampleData])
    // SampleDataList: SampleData[] = undefined;
    
}