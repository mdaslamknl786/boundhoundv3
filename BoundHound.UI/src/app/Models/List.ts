export class Lists
{
    Id:number;
    ListId:number;
    listName:string;
    listDesc:string;
    TenantId:number;
    UserId:number;
    LastUpdate:Date;
    IsActive:boolean;
}
