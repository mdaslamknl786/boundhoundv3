import { PageNotFoundComponent } from './shared/page-not-found/page-not-found.component';
import { Component } from '@angular/core';
import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { AppComponent } from './app.component';
import { UnauthorizedComponent } from './public/unauthorized/unauthorized.component';
import { AuthGuardService } from './Services/auth-guard.service';
import { AuthorGuardService } from './Services/author-guard.service';

const routes: Routes = [
  // {
  //   path: '',
  //   loadChildren: 'app/Modules/signin/signin.module#SigninModule',
  //   pathMatch: 'full'
  // },
  {
    path: 'login',
    loadChildren: 'app/Modules/login/login.module#LoginModule',
    pathMatch: 'full'
  },
  {
    path: '',
    loadChildren: 'app/Modules/dashboard/dashboard.module#DashboardModule',
    pathMatch: 'full'
  },
  {
    path: 'dashboard',
    loadChildren: 'app/Modules/dashboard/dashboard.module#DashboardModule',
    canActivate: [AuthGuardService],
    canActivateChild: [AuthorGuardService],
    pathMatch: 'full'
  },
  {
    path: 'login',
    loadChildren: 'app/Modules/login/login.module#LoginModule',
    pathMatch: 'full'
  },
  {
    path: 'signin',
    loadChildren: 'app/Modules/signin/signin.module#SigninModule'
    // canActivate: [AuthGuardService],
    // canActivateChild: [AuthorGuardService],
  },
  {
    path:'home',
    loadChildren:'app/Modules/home/home.module#HomeModule'
  },
  {
    path:'email',
    loadChildren:'app/Modules/email/email.module#EmailModule',
    data: { preload: true }
  },
  {
    path:'connections',
    loadChildren:'app/Modules/connections/connections.module#ConnectionsModule'
  },
  {
    path:'profile',
    loadChildren:'app/Modules/profile/profile.module#ProfileModule'
  },
  {
    path:'messaging',
    loadChildren:'app/Modules/messaging/messaging.module#MessagingModule'
  },
  {
    path:'leadfinder',
    loadChildren:'app/Modules/leadfinder/leadfinder.module#LeadfinderModule'
  },
  {
    path:'register',
    loadChildren:'app/Modules/register/register.module#RegisterModule'
  },
  {
    path:'myinfo',
    loadChildren:'app/Modules/myinfo/myinfo.module#MyinfoModule'
  },
  {
    path:'popup',
    loadChildren:'app/Modules/popup/popup.module#PopupModule'
  },
  {
    path:'campaigns',
    loadChildren:'app/Modules/campaigns/campaigns.module#CampaignsModule'
  },
  {
    path: 'error',
    component: AppComponent,
    children: [
      {
        path: '401',
        component: UnauthorizedComponent
      },
    ]
  },
  {
    path: '**',
    component: PageNotFoundComponent
  }
];



@NgModule({
  imports: [
    //RouterModule.forRoot(routes,{useHash: true})],
    RouterModule.forRoot(routes)],
  exports: [RouterModule]  
})
export class AppRoutingModule { }
