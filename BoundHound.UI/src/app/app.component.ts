import { Component,Input   } from '@angular/core';

@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.css']
})
export class AppComponent {
  title = 'Bound Hound';
  User:any;

  showMenu : boolean = false; 
  mainData : boolean =false;
  
   
  
  onMenuClick(eventArgs){
    this.showMenu = !this.showMenu
    this.mainData = !this.mainData
  } 

}
