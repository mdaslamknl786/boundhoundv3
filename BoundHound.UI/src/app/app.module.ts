import { NgModule } from '@angular/core';
import { BrowserAnimationsModule } from '@angular/platform-browser/animations';
import { AppRoutingModule } from './app-routing.module';
import { AppComponent } from './app.component';
import { AngularWebStorageModule } from 'angular-web-storage';
import { PageNotFoundComponent } from './shared/page-not-found/page-not-found.component';
import { SharedModule } from './Shared/shared/shared.module';
import { SharedServiceComponent } from './Shared/service/shared-service/shared-service.component';
import { LoginService } from './Services/login.service';
import { BrowserModule } from '@angular/platform-browser';


import { AppCustomMaterialModuleModule } from './Shared/app-custom-material-module/app-custom-material-module.module';

import { DialogService } from './Shared/dialog.service';
import { APP_BASE_HREF } from '@angular/common';

import { LocationStrategy, HashLocationStrategy } from '@angular/common';
import { UnauthorizedComponent } from './public/unauthorized/unauthorized.component';
import { AuthService } from 'ng4-social-login';

import 'hammerjs';
import { HttpClientModule } from '@angular/common/http';

@NgModule({
  declarations: [
    AppComponent,
    PageNotFoundComponent,
    SharedServiceComponent,
    UnauthorizedComponent
    
  ],
  imports: [
    //NgbModule.forRoot(),
    BrowserModule,
    AppRoutingModule,
    AngularWebStorageModule,
    SharedModule,
    BrowserAnimationsModule,
    AppCustomMaterialModuleModule,
    HttpClientModule
  ],
  providers: [
  //   ProductService, {
  //     provide: LocationStrategy, useClass: HashLocationStrategy
  // },
    
    LoginService,DialogService,AuthService,
     { provide: LocationStrategy, useClass: HashLocationStrategy }
    //{provide: APP_BASE_HREF, useValue: 'index.html'}
  ],
  bootstrap: [AppComponent]
})
export class AppModule { }
