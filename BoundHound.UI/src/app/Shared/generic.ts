export const EMAIL_PATTERN = /^[a-zA-Z0-9.!#$%&’*+/=?^_`{|}~-]+@[a-zA-Z0-9-]+(?:\.[a-zA-Z0-9-]+)*$/;
export const MOBILE_NUMBER_PATTERN = /^\d{10}$/;
export const NUMBER_PATERN = /^[0-9]*$/;
export const STRING_PATERN = /^[a-zA-Z ]*$/;
export const ALPHA_NUMERIC = /^(?=.*[a-zA-Z])([a-zA-Z0-9 _-]+)$/;
export const ALPHA_NUMERIC_OR_NUMBER = /^(?=.*[0-9])([a-zA-Z0-9 _-]+)$/;
export const ALPHA_NUMERIC_WITH_SPECIALCHARACTER=/^(?=.*[0-9])([a-zA-Z0-9 &]+)$/    

export const MEDIUM_OF_ADVERTISEMENT = [
    {'Id': 1, 'Value': 'Newspaper'},
    {'Id': 2, 'Value': 'Television'},
    {'Id': 3, 'Value': 'Online advertisement'},
    {'Id': 4, 'Value': 'Student referral'},
    {'Id': 5, 'Value': 'Staff referral'},
    {'Id': 6, 'Value': 'Word of mouth'}
];

export const YES_NO: any = [
    {'Id': 1, 'Value': 'Yes'},
    {'Id': 2, 'Value': 'No'}
];

export const TIME_HALFHOUR_INTERVALS = [
    '8:00 AM', '8:30 AM', '9:00 AM', '9:30 AM',
    '10:00 AM', '10:30 AM', '11:00 AM', '11:30 AM',
    '12:00 noon', '12:30 PM', '1:00 PM', '1:30 PM',
    '2:00 PM', '2:30 PM', '3:00 PM', '3:30 PM',
    '4:00 PM', '4:30 PM', '5:00 PM', '5:30 PM', '6:00 PM'
];

export const SMS_MESSAGE_LENGTH = 160;

export class DateTimeFormates {
    static readonly DATE = 'DD MMM YYYY';
    static readonly TIME = 'hh:mm a';
    static readonly DATE_TIME = `${DateTimeFormates.DATE} ${DateTimeFormates.TIME}`;
  };

export const CHART_COLORS = [
        'rgba(255, 99, 132, 0.6)',
        'rgba(54, 162, 235, 0.6)',
        'rgba(255, 206, 86, 0.6)',
        'rgba(75, 192, 192, 0.6)',
        'rgba(153, 102, 255, 0.6)',
        'rgba(255, 159, 64, 0.6)'
    ];