import { LoginService } from './../../../Services/login.service';
import { Component, OnInit,OnChanges,Input,Output,EventEmitter  } from '@angular/core';
import { LocalStorageService, SessionStorageService, LocalStorage, SessionStorage } from 'angular-web-storage';
import { Router, ActivatedRoute, ParamMap } from '@angular/router';
import { SimpleChange } from '@angular/core/src/change_detection/change_detection_util';
import { SocialUser } from 'ng4-social-login';
import { AuthEventService } from './../../../Services/auth-event.service';
//import { AuthService, SocialUser } from 'ng4-social-login';


@Component({
  selector: 'app-public-header',
  templateUrl: './public-header.component.html',
  styleUrls: ['./public-header.component.css']
})
export class PublicHeaderComponent implements OnInit,OnChanges {
  @Input() isSelected:boolean;
@Output() change =new EventEmitter;

  // Define a variable to use for showing/hiding the Login button
  isUserLoggedIn: boolean;
  LoggedInUser: string;
  UserPhotoUrl: string;
  AccessToken: string;
  UserName:any;
  //logoUrl="./../assets/images/bh-logo-1.png";
  logoUrl = "./../assets/images/BoundHound-Logo.png";
  MyUserInfo:SocialUser;
  wasClicked = false;
  constructor(public local: LocalStorageService, public session: SessionStorageService,
    private router: Router,
    public logservice: LoginService,
    public autheventservice:AuthEventService) { 
       // Subscribe here, this will automatically update 
        // "isUserLoggedIn" whenever a change to the subject is made.
      //   this.sharedService.IsUserLoggedIn.subscribe( value => {
      //     
      //     this.isUserLoggedIn = value;
      // });

      this.logservice.UserDetails.subscribe(value =>{
        //
        this.MyUserInfo=value;
      });

      this.logservice.IsUserLoggedIn.subscribe(value =>{
        //
        this.isUserLoggedIn=value;
      });

      
    }
    public getUserLoggedIn(): boolean 
    { 
      return this.logservice.getUserLoggedIn();
      
    }
  ngOnInit() {
    //debugger;
    this.LoggedInUser = localStorage.getItem("UserName");
    this.UserPhotoUrl = localStorage.getItem("UserPhotoUrl");
    this.AccessToken = localStorage.getItem("UserToken");


    //this.autheventservice.getLoggedInName.subscribe(name => this.UserName=name);
    this.autheventservice.getLoggedInName.subscribe((user) => {
      //debugger;
      this.UserName=name;
    });

    if (this.LoggedInUser == null) {
      this.router.navigate(['/signin']);
    }
    // localStorage.setItem("UserId",user.id);
    // localStorage.setItem("UserName",user.name);
    // localStorage.setItem("UserEmail",user.email);
    // localStorage.setItem("UserPhotoUrl",user.photoUrl);
    // localStorage.setItem("UserToken",user.token);
    // localStorage.setItem("UserProvier",user.provider);
  }

  onClick(){     
    this.isSelected=!this.isSelected;
    this.change.emit(this.isSelected);
   }

   
  ngOnChanges(SimpleChange: any) {

    // this.autheventservice.getLoggedInName.subscribe((user) => {
    //   debugger;
    //   this.UserName=name;
    // });

  }
  showMenu() {
    this.wasClicked = !this.wasClicked;
  }
  signOut()
  {
    localStorage.clear();
    this.showMenu();
  }
}
