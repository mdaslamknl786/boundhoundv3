import { AppCustomMaterialModuleModule } from './../app-custom-material-module/app-custom-material-module.module';
import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';

@NgModule({
  imports: [
    CommonModule,
    AppCustomMaterialModuleModule
  ],
  exports:[
    AppCustomMaterialModuleModule
  ],
  declarations: []
})
export class CoreModule { }
