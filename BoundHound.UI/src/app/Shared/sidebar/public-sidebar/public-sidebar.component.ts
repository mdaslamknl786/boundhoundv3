import { Component, OnInit,Input } from '@angular/core';

@Component({
  selector: 'app-public-sidebar',
  templateUrl: './public-sidebar.component.html',
  styleUrls: ['./public-sidebar.component.css']
})
export class PublicSidebarComponent implements OnInit {
  appDropdownLists : boolean = false;
  appDropdownCampaigns : boolean = false;
  appDropdownAnalytics : boolean = false;
  appDropdownConversations : boolean =false;
  constructor() { }

  @Input() User: any;
  wasClicked = false;
  collapseMenu() {     
    this.wasClicked= !this.wasClicked;   
  }
  
  showDropdownLists(){  
    this.appDropdownLists= !this.appDropdownLists;     
  }
  showDropdownCampaigns(){
    this.appDropdownCampaigns= !this.appDropdownCampaigns;  
  }
  showDropdownAnalytics(){
    this.appDropdownAnalytics= !this.appDropdownAnalytics;  
  }
  showDropdownConversations(){
    this.appDropdownConversations= !this.appDropdownConversations;  
  }
 
  ngOnInit() {
  }

}
