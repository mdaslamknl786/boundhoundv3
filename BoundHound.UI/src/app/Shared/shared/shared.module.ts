import { PublicHeaderComponent } from './../header/public-header/public-header.component';
import { PublicSidebarComponent } from './../sidebar/public-sidebar/public-sidebar.component';

import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';

import { SharedRoutingModule } from './shared-routing.module';
import { PopupheaderComponent } from './../popupheader/popupheader.component';
import { AuthEventService } from './../../Services/auth-event.service';
import { AuthGuardService } from '../../Services/auth-guard.service';
import { AuthApiService } from '../../Services/auth-api.service';
import { AuthorGuardService } from '../../Services/author-guard.service';
import { ApiService } from '../../Services/api.service';
import { SessionStorageService } from '../../Services/sessionstorage.service.';


@NgModule({
  imports: [
    CommonModule,
    SharedRoutingModule
  ],
  providers:[AuthEventService,AuthGuardService,AuthApiService,AuthorGuardService,ApiService,SessionStorageService],
  declarations: [PublicHeaderComponent,PublicSidebarComponent,PopupheaderComponent],
  exports:[PublicHeaderComponent,PublicSidebarComponent,PopupheaderComponent]
})
export class SharedModule { }
