import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';

import { CampaignsRoutingModule } from './campaigns-routing.module';
import { CampaignLayoutComponent } from './components/campaign-layout/campaign-layout.component';
import { CampaignsComponent } from './components/campaigns/campaigns.component';

@NgModule({
  imports: [
    CommonModule,
    CampaignsRoutingModule
  ],
  declarations: [CampaignLayoutComponent, CampaignsComponent]
})
export class CampaignsModule { }
