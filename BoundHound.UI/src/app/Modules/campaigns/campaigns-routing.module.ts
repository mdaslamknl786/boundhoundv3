import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { CampaignLayoutComponent } from './components/campaign-layout/campaign-layout.component';
import { CampaignsComponent } from './components/campaigns/campaigns.component';

const CampaignsRoutes: Routes = [
  {
    path: '', component: CampaignLayoutComponent,

    children: [
      {
        path: '',
        component: CampaignsComponent
        
       }
      // {
      //   path:'list',
      //   component: EmailListComponent
      // },
      // {
      //   path:'create',
      //   component: EmailCreateComponent
      // }

    ]
  }
];

@NgModule({
  imports: [RouterModule.forChild(CampaignsRoutes)],
  exports: [RouterModule]
})
export class CampaignsRoutingModule { }
