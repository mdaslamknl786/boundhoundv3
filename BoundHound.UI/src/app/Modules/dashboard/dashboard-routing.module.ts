import { DashboardListComponent } from './components/dashboard-list/dashboard-list.component';
import { DashboardLayoutComponent } from './components/dashboard-layout/dashboard-layout.component';
import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';



const routes: Routes = [
  {
    path: '', component: DashboardLayoutComponent,
    // resolve: {
    //   academicYears: AcademicYearsResolver,
    // },
    children: [
      {
        path: '',
        component: DashboardListComponent
      },
      {
        path:'list',
        component: DashboardListComponent
      }
    ]
  }
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class DashboardRoutingModule { }
