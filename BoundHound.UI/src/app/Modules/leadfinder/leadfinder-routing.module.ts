import { LeadfinderListComponent } from './components/leadfinder-list/leadfinder-list.component';
import { LeadfinderlayoutComponent } from './components/leadfinderlayout/leadfinderlayout.component';
import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';



const routes: Routes = [
  {
    path: '', component: LeadfinderlayoutComponent,
    // resolve: {
    //   academicYears: AcademicYearsResolver,
    // },
    children: [
      {
        path: '',
        component: LeadfinderListComponent
      },
      {
        path:'list',
        component: LeadfinderListComponent
      }
    ]
  }
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class LeadfinderRoutingModule { }
