import { SharedModule } from './../../Shared/shared/shared.module';
import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';

import { LeadfinderRoutingModule } from './leadfinder-routing.module';
import { LeadfinderlayoutComponent } from './components/leadfinderlayout/leadfinderlayout.component';
import { LeadfinderListComponent } from './components/leadfinder-list/leadfinder-list.component';

@NgModule({
  imports: [
    CommonModule,
    LeadfinderRoutingModule,
    SharedModule
  ],
  declarations: [LeadfinderlayoutComponent, LeadfinderListComponent]
})
export class LeadfinderModule { }
