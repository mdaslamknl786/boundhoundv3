import { RegisterUsers } from './../../Models/RegisterUsers';
import { Injectable } from '@angular/core';
import { Observable } from 'rxjs/Observable';
import { HttpHeaders } from '@angular/common/http';
import { ApiService } from './../../Services/api.service';
import { environment } from './../../../environments/environment';
import { LinkedinService } from './../../Services/linkedin.service';
import { BehaviorSubject } from 'rxjs';


@Injectable()
export class RegisterService {
  private _webApiUrl = environment.apiEndPoint;
  static nextCrisisId = 100;
  //private emailslists$: BehaviorSubject<RegisterUsers[]> = new BehaviorSubject<RegisterUsers[]>();

  constructor(private apiService:ApiService,private linkedservice:LinkedinService){}

  RegisterForm(registerObj:any): Observable<any> {
    //
    let reqHeaders = new HttpHeaders();
    //return  this.apiService.putData(`${this._webApiUrl}UpdateEmail`, vendorObj, reqHeaders);  
    return  this.apiService.postData(`${this._webApiUrl}RegisterForm`, registerObj, reqHeaders);  

  }  

}
