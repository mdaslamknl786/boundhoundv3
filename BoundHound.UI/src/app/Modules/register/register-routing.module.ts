import { RegisterLayoutComponent } from './register-layout/register-layout.component';
import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { RegisterRegisterComponent } from './register-register/register-register.component';
import { CanDeactivateGuard } from './../../Services/can-deactivate-guard.service';
const RegisterRoutes: Routes = [
  {
    path: '', component: RegisterRegisterComponent,

    children: [
      {
        path: 'register',
        component: RegisterRegisterComponent
        
      }
    ]
  }
];

@NgModule({
  imports: [RouterModule.forChild(RegisterRoutes)],
  exports: [RouterModule]
})
export class RegisterRoutingModule { }
