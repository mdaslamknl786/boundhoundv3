import { Headers } from '@angular/http';
import { Component, OnInit } from '@angular/core';
import { MatTableDataSource } from '@angular/material';
import { HttpHeaders } from '@angular/common/http';
import { HttpClientModule } from '@angular/common/http';
import { Observable } from 'rxjs/Observable';
import { Router, ActivatedRoute, ParamMap } from '@angular/router';
import { MatSnackBar } from '@angular/material';
import { ApiService } from './../../../Services/api.service';
import { STRING_PATERN, EMAIL_PATTERN } from './../../../Shared/generic';

import {FormBuilder,FormGroup,Validators,FormControl,FormArray} from '@angular/forms';

import { RegisterUsers } from './../../../Models/RegisterUsers';
//import { AppCustomMaterialModuleModule } from './../../../Shared/app-custom-material-module/app-custom-material-module.module';

import { RegisterService } from './../register.service';
@Component({
  selector: 'app-register-register',
  templateUrl: './register-register.component.html',
  styleUrls: ['./register-register.component.css']
})
export class RegisterRegisterComponent implements OnInit {
  myForm:FormGroup;
  existedPriorities = [];
  formError: any;
  registerInfoForm: FormGroup;
  feeTypeId: number;
  isExistingFeeType: boolean = false;
  selectedFeeType: string = "";
  selectedInventory: string = "DonotProrate";
  previousFeeType: string;
  previousInventory: string;
  isFeeTypeSelected: boolean = false;
  schoolId: number = 1;
  isMiscellaneousFee: boolean = false;
  public feeType;


  UserTypeList = [
    { 'label': 'Individual', 'value': 'Individual' },
    { 'label': 'Company', 'value': 'Company' }
  ];
  
  // statuses = FINANCE_STATUS;
  // prioritiesList = priorities;
  // feeTypeCategories = feeTypeCategories;
  // inventories = inventories;
  

  constructor(public fb: FormBuilder,
    public router: Router,
    public route: ActivatedRoute,
    public snackBar: MatSnackBar,
    public apiservice:ApiService,
    public registerservice:RegisterService) {
  }

// Code Starts Here
ngOnInit() {

  // this.UserTypeList.forEach(priority => {
  //   this.existedPriorities.push(priority);
  // });


  this.myForm=this.fb.group({
    name : ['',Validators.required],
    age:['',Validators.required]
  });

  this.registerInfoForm = this.fb.group({
    Id: '0',
    RegisterId: 1,
    Email: ['', { validators: [Validators.pattern(EMAIL_PATTERN), Validators.required] }],
    Password: ['', { validators: [Validators.pattern(STRING_PATERN), Validators.required] }],
    UserName:['', { validators: [Validators.pattern(STRING_PATERN), Validators.required] }],
    Company:'',
    TenantId:1,
    IsFirstTime:1,
    UserType:'Individual',
  });

  // public int RegisterId { get; set; }
  // public string Email { get; set; }
  // public string Password { get; set; }
  // public string UserName { get; set; }
  // public int TenantID { get; set; }
  // public bool IsFirstTime { get; set; }

  // this.prioritiesList.forEach(priority => {
  //   this.existedPriorities.push(priority);
  // });


  this.route.paramMap.subscribe((param: ParamMap) => {
    this.feeTypeId = Number(param.get('feeTypeId'));

    if (param.get('feeTypeId') != undefined) {
      this.feeTypeId = Number(param.get('feeTypeId'));
      this.isExistingFeeType = true;
    }
  });

  // if (this.isExistingFeeType) {
  //   // API Call Starts Here
  //   this.feeTypeService.getFeeTypeById(Number(this.feeTypeId)).subscribe((data) => {
  //     this.feeType = data;        
  //     this.feeTypeInfoForm.patchValue(this.feeType);
  //     //FeeType selection 
  //     if (this.feeType.IsCompulsoryFee) {
  //       this.selectedFeeType = "IsCompulsoryFee";
  //     }
  //     else if (this.feeType.IsTransportFee) {
  //       this.selectedFeeType = "IsTransportFee";
  //     }
  //     else if (this.feeType.IsFoodFee) {
  //       this.selectedFeeType = "IsFoodFee";
  //     }

  //     else if (this.feeType.IsMiscellaneousFee) {
  //       this.selectedFeeType = "IsMiscellaneousFee";
  //     }
  //     else {
  //       this.selectedFeeType = "IsAdmissionFee";
  //     }

  //     this.previousFeeType = this.selectedFeeType;

  //     //Inventory selection 
  //     if (this.feeType.ProrateForTerm) {
  //       this.selectedInventory = "ProrateForTerm";
  //     }

  //     else if (this.feeType.ProrateForMonth) {
  //       this.selectedInventory = "ProrateForMonth";
  //     }
  //     else {
  //       this.selectedInventory = "DonotProrate";
  //     }

  //     this.previousInventory = this.selectedInventory;
  //     this.GetAllocatedFeeTypePriorties();
  //   });
  //   // API Call Ends Here
  // }
  // else{
  //   this.GetAllocatedFeeTypePriorties();
  // }

  this.registerInfoForm.get('Email').setValidators([Validators.required, Validators.pattern(EMAIL_PATTERN)]);
  this.registerInfoForm.get('Password').setValidators([Validators.required, Validators.pattern(STRING_PATERN)]);
}


// GetAllocatedFeeTypePriorties(): void {
//   let reqHeaders = new HttpHeaders();
//   let feeTypes = <Observable<Array<FeeTypes>>>this.feeTypeService.getFeeTypes();
//   feeTypes.subscribe((feeTypes) => {     
//     feeTypes.forEach(feeType => {

//       const index: number = this.existedPriorities.indexOf(this.existedPriorities.filter(x => x.label == feeType.AllocationPriority)[0]);
//       if (index !== -1) {
//         this.existedPriorities.splice(index, 1);
//       }
//     });

//     if (this.isExistingFeeType) {        
//       let priority = this.prioritiesList.filter(x => x.label === this.feeType.AllocationPriority)[0];
//       this.existedPriorities.push(priority);
//     }
//   });

//}

// onFeeTypeChange(event) {
//   this.isFeeTypeSelected = false;
//   if (event.value === "IsMiscellaneousFee") {
//     this.isMiscellaneousFee = true;
//   }
//   else {
//     this.isMiscellaneousFee = false;
//   }
// }

createRegisForm(registerForm: any): void {
  const self = this;
  //debugger;
  this.registerservice.RegisterForm(registerForm).subscribe(
    (data: any) => {
      //debugger;
      
      self.snackBar.open(data.Message, null, {
        duration: 5000, verticalPosition: 'top',
        horizontalPosition: 'right', panelClass: 'boundhound-snackbar'
      });
      if(data.Status=="SUCCESS")
      {
        this.router.navigate(['/signin']);
      }
    },
    err => {
      self.snackBar.open(err.error.ExceptionMessage, null, {
        duration: 5000, verticalPosition: 'top',
        horizontalPosition: 'right', panelClass: 'boundhound-snackbar'
      });       
    });

  // this.apiservice.createFeeType(registerForm).subscribe(
  //   (data: any) => {
  //     self.snackBar.open('Fee type saved successfully.', null, {
  //       duration: 5000, verticalPosition: 'top',
  //       horizontalPosition: 'right', panelClass: 'patasala-snackbar'
  //     });
  //     this.router.navigate(['/finance/settings/fee-types']);
  //   },
  //   err => {
  //     self.snackBar.open(err.error.ExceptionMessage, null, {
  //       duration: 5000, verticalPosition: 'top',
  //       horizontalPosition: 'right', panelClass: 'patasala-snackbar'
  //     });       
  //   });

}

// updateFeeType(feeType: any): void {
//   const self = this;
//   this.feeTypeService.updateFeeType(feeType, this.feeTypeId).subscribe(
//     (data: any) => {
//       self.snackBar.open('Fee type updated successfully.', null, {
//         duration: 5000, verticalPosition: 'top',
//         horizontalPosition: 'right', panelClass: 'patasala-snackbar'
//       });
//       this.router.navigate(['/finance/settings/fee-types']);
//     },
//     err => {
//       self.snackBar.open(err.error.ExceptionMessage, null, {
//         duration: 5000, verticalPosition: 'top',
//         horizontalPosition: 'right', panelClass: 'patasala-snackbar'
//       });       
//     }
//   );
// }

  onSubmit(registerInfoForm: RegisterUsers) {
    //debugger;
    if (this.registerInfoForm.valid) {
      this.formError = "";
      this.createRegisForm(registerInfoForm);
    }
    else {

      this.formError = 'Please fix the above problems (marked in red) and then click on Save.';
    }

  }

// Code Ends Here






}


class FeeTypes {
  Id: number;
  FeeName: string;
  AllocationPriority: number;
}

export const UserTypeList = [
  {'label': 'Individual', 'value' : 'Individual'},
  {'label': 'Company', 'value': 'Company'}
  ];