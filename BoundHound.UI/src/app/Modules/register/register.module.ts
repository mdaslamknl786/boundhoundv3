import { ApiService } from './../../Services/api.service';
import { LinkedinService } from './../../Services/linkedin.service';
import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';

import { RegisterRoutingModule } from './register-routing.module';
import { SharedModule } from './../../Shared/shared/shared.module';
import { RegisterLayoutComponent } from './register-layout/register-layout.component';
import { RegisterRegisterComponent } from './register-register/register-register.component';
import { CoreModule } from './../../Shared/core/core.module';
import { SessionStorageService } from './../../Shared/sessionstorage.service';

import { SharedService } from './../../Services/shared.service';

import { HttpModule } from '@angular/http';
import { HttpClientModule } from '@angular/common/http';
import { AppCustomMaterialModuleModule } from './../../Shared/app-custom-material-module/app-custom-material-module.module';

import { RegisterService } from './register.service';
//import { FormBuilder, FormControl,ReactiveFormsModule,FormsModule } from '@angular/forms';
@NgModule({
  imports: [
    CommonModule,
    CommonModule,
    HttpModule,
    HttpClientModule,
    CoreModule,
    RegisterRoutingModule,
    // FormsModule,
    // ReactiveFormsModule,
    // FormBuilder,
    // FormControl
  ],
  declarations: [RegisterLayoutComponent, RegisterRegisterComponent],
  providers:[LinkedinService ,SharedService,ApiService, SessionStorageService,RegisterService]
})
export class RegisterModule { }
