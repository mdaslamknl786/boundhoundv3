import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';

import { SigninRoutingModule } from './signin-routing.module';

import { SigninComponent } from './components/signin/signin.component';
import { ForgetpasswordComponent } from './components/forgetpassword/forgetpassword.component';
import { AuthEventService } from './../../Services/auth-event.service';

import {
  AuthService,
  SocialLoginModule,
  AuthServiceConfig,
  GoogleLoginProvider,
  FacebookLoginProvider,
  LinkedinLoginProvider
  } from 'ng4-social-login';
import { SharedModule } from '../../Shared/shared/shared.module';

  const CONFIG = new AuthServiceConfig([
    {
      id: GoogleLoginProvider.PROVIDER_ID,
      //provider: new GoogleLoginProvider('319348025793-06ou4buiipncf6eqttf813m17gevmk89.apps.googleusercontent.com')
      provider: new GoogleLoginProvider('141358472348-mes05ajr7e7l8n1evcekc7t6o7ioh0dn.apps.googleusercontent.com')
      
      //provider: new GoogleLoginProvider('624796833023-clhjgupm0pu6vgga7k5i5bsfp6qp6egh.apps.googleusercontent.com')
      
    },
    {
      id: FacebookLoginProvider.PROVIDER_ID,
      provider: new FacebookLoginProvider('334255156744933')
    },
    {
      id: LinkedinLoginProvider.PROVIDER_ID,
      provider: new LinkedinLoginProvider('812mb1rk4zp2cb')
    }
  ]);
  
  export function provideConfig() {
    return CONFIG;
  }

@NgModule({
  imports: [
    CommonModule,
    SigninRoutingModule,
    SharedModule
  ],
  providers: [
    {
      provide: AuthServiceConfig,
      useFactory: provideConfig
    },
    AuthService,
    AuthEventService
  ],
  declarations: [SigninComponent,ForgetpasswordComponent]
})
export class SigninModule { }
