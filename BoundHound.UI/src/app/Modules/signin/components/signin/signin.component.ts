import { LoginService } from './../../../../Services/login.service';
import { Component, OnInit } from '@angular/core';
import { Router, ActivatedRoute, ParamMap } from '@angular/router';
import {
  FacebookLoginProvider,
  GoogleLoginProvider,
  LinkedinLoginProvider
} from 'ng4-social-login'

import { SocialUser } from 'ng4-social-login';
import { AuthService } from 'ng4-social-login';

import { LocalStorageService, SessionStorageService, LocalStorage, SessionStorage } from 'angular-web-storage';

import { AuthEventService } from './../../../../Services/auth-event.service';

@Component({
  selector: 'app-signin',
  templateUrl: './signin.component.html',
  styleUrls: ['./signin.component.css']
})
export class SigninComponent implements OnInit {
  public user: SocialUser;
  public loggedIn: boolean;

  constructor(private authService: AuthService,
    public local: LocalStorageService, public session: SessionStorageService,
    private router: Router,
    public loginservice: LoginService,
    public autheventservice:AuthEventService) { }

  KEY = 'Token';
  value: any = null;

  public getUserLoggedIn(): boolean 
  { 
    return this.loginservice.getUserLoggedIn() 
  }

  ngOnInit() {
    //debugger;
    this.loggedIn = (this.user != null);
    // 
    // if (this.loggedIn == false) {
    //   this.authService.authState.subscribe((user) => {
    //     this.user = user;
    //     this.loggedIn = (user != null);
    //     if (this.loggedIn) {
    //       this.clearLocal();
    //       
    //       localStorage.setItem("UserId", user.id);
    //       localStorage.setItem("UserName", user.name);
    //       localStorage.setItem("UserEmail", user.email);
    //       localStorage.setItem("UserPhotoUrl", user.photoUrl);
    //       localStorage.setItem("UserToken", user.token);
    //       localStorage.setItem("UserProvier", user.provider);

    //       this.router.navigate(['/home']);
    //     }
    //   });
    // }
  }
  setToken(expired: any = 0) {
    this.local.set(this.KEY, { a: 1, now: +new Date }, expired, 's');
  }
  clearLocal(): void {
    localStorage.removeItem("UserId");
    localStorage.removeItem("UserName");
    localStorage.removeItem("UserEmail");
    localStorage.removeItem("UserPhotoUrl");
    localStorage.removeItem("UserToken");
    localStorage.removeItem("UserProvier");
  }
  signInWithGoogle(): void {
    //debugger;
    this.authService.signIn(GoogleLoginProvider.PROVIDER_ID);
    if (!this.loggedIn) {
      this.authService.authState.subscribe((user) => {
        this.user = user;
        this.loggedIn = (user != null);
        if (this.loggedIn) {
          this.clearLocal();
          //
          localStorage.setItem("UserId", user.id);
          localStorage.setItem("UserName", user.name);
          localStorage.setItem("UserEmail", user.email);
          localStorage.setItem("UserPhotoUrl", user.photoUrl);
          localStorage.setItem("UserToken", user.token);
          localStorage.setItem("UserProvier", user.provider);

          //Set login so that header will get refresh
          this.autheventservice.login('mdaslamknl@gmail.com','password');

          // Some code 
          // .....
          // After the user has logged in, emit the subject changes.
          //
          this.loginservice.setLoggedInUser(true);
          this.loginservice.SetUserInfo(user);
          //this.router.navigate(['/movies],{title:ring,si:true}]);
          this.router.navigate(['/home']);
          //this.router.navigate(['/home',this.user]);
        }
      });
    }
  }

  signInWithFB(): void {
    this.authService.signIn(FacebookLoginProvider.PROVIDER_ID);
    if (!this.loggedIn) {
      this.authService.authState.subscribe((user) => {
        this.user = user;
        this.loggedIn = (user != null);
        if (this.loggedIn) {
          this.clearLocal();
          //
          localStorage.setItem("UserId", user.id);
          localStorage.setItem("UserName", user.name);
          localStorage.setItem("UserEmail", user.email);
          localStorage.setItem("UserPhotoUrl", user.photoUrl);
          localStorage.setItem("UserToken", user.token);
          localStorage.setItem("UserProvier", user.provider);

          //
          this.loginservice.setLoggedInUser(true);
          this.loginservice.SetUserInfo(user);
          //this.router.navigate(['/movies],{title:ring,si:true}]);
          this.router.navigate(['/home']);
          //this.router.navigate(['/home',this.user]);
        }
      });
    }


  }

  signInWithLinkedIN(): void {

    this.authService.signIn(LinkedinLoginProvider.PROVIDER_ID);
    if (!this.loggedIn) {
      this.authService.authState.subscribe((user) => {
        this.user = user;
        this.loggedIn = (user != null);
        if (this.loggedIn) {
          this.clearLocal();
          //
          localStorage.setItem("UserId", user.id);
          localStorage.setItem("UserName", user.name);
          localStorage.setItem("UserEmail", user.email);
          localStorage.setItem("UserPhotoUrl", user.photoUrl);
          localStorage.setItem("UserToken", user.token);
          localStorage.setItem("UserProvier", user.provider);
          //
          this.loginservice.setLoggedInUser(true);
          this.loginservice.SetUserInfo(user);
          this.router.navigate(['/home']);
        }
      });
    }

  }

  signOut(): void {
    this.authService.signOut();
  }
}
