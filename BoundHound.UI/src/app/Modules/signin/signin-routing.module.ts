import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';

import { SigninComponent } from './components/signin/signin.component';
import { ForgetpasswordComponent } from './components/forgetpassword/forgetpassword.component';


const routes: Routes = [
  {
    path: '',
    component: SigninComponent
  },
  {
    path: 'forgetpassword',
    component: ForgetpasswordComponent
  }
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class SigninRoutingModule { }
