

import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';

import { ConnectionsRoutingModule } from './connections-routing.module';
import { ConnectionsListComponent } from './components/connections-list/connections-list.component';
import { ConnectionslayoutComponent } from './components/connectionslayout/connectionslayout.component';

import { SharedModule } from './../../Shared/shared/shared.module';

@NgModule({
  imports: [
    CommonModule,
    ConnectionsRoutingModule,
    SharedModule
  ],
  //entryComponents: [ConnectionsListComponent],
	declarations: [ConnectionsListComponent, ConnectionslayoutComponent],
  providers: []
})
export class ConnectionsModule { }
