import { ConnectionslayoutComponent } from './components/connectionslayout/connectionslayout.component';
import { ConnectionsListComponent } from './components/connections-list/connections-list.component';
import { NgModule, Component } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';



const routes: Routes = [
  {
    path: '', component: ConnectionslayoutComponent,
    // resolve: {
    //   academicYears: AcademicYearsResolver,
    // },
    children: [
      {
        path: '',
        component: ConnectionsListComponent
      },
      {
        path:'list',
        component: ConnectionsListComponent
      }
    ]
  }
];
// const routes: Routes = [
//   {
//     path: '',
//     component: ConnectionsListComponent
//   }
// ];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class ConnectionsRoutingModule { }
