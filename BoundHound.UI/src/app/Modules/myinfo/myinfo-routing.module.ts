import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { UserinfoComponent } from './components/userinfo/userinfo.component';
import { UserlayoutComponent } from './components/userlayout/userlayout.component';
const routes: Routes = [
  {
    path: '', component: UserlayoutComponent,
    // resolve: {
    //   academicYears: AcademicYearsResolver,
    // },
    children: [
      {
        path: '',
        component: UserinfoComponent
      },
      {
        path:'list',
        component: UserinfoComponent
      }
    ]
  }
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class MyinfoRoutingModule { }
