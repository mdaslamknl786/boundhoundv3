
import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';

import { MyinfoRoutingModule } from './myinfo-routing.module';

import { UserinfoComponent } from './components/userinfo/userinfo.component';
import { UserlayoutComponent } from './components/userlayout/userlayout.component';
@NgModule({
  imports: [
    CommonModule,
    MyinfoRoutingModule
  ],
  declarations: [UserlayoutComponent, UserinfoComponent]
})
export class MyinfoModule { }
