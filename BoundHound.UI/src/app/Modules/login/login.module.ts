import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';

import { LoginRoutingModule } from './login-routing.module';
import { LoginComponent } from './components/login/login.component';
import { LogoutComponent } from './components/logout/logout.component';
import { AppCustomMaterialModuleModule } from '../../Shared/app-custom-material-module/app-custom-material-module.module';

@NgModule({
  imports: [
    CommonModule,
    LoginRoutingModule,
    AppCustomMaterialModuleModule
  ],
  declarations: [LoginComponent, LogoutComponent]
})
export class LoginModule { }
