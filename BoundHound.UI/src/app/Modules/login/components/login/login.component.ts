import { Component, OnInit } from '@angular/core';
import { MatButtonModule, MatToolbarModule } from '@angular/material';
import { Router } from '@angular/router';
@Component({
  selector: 'app-login',
  templateUrl: './login.component.html',
  styleUrls: ['./login.component.css']
})
export class LoginComponent implements OnInit {

  constructor(private router : Router) { }

  ngOnInit() {
  }
  showSpinner:boolean=false;
  username : string
  password : string

  login() : void {
    if(this.username == 'admin' && this.password == 'admin'){
     this.router.navigate(["home"]);
    }else {
      alert("Invalid credentials");
    }
  }
}
