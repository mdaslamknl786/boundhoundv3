import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';

import { PopupRoutingModule } from './popup-routing.module';
import { PopupLayoutComponent } from './components/popup-layout/popup-layout.component';
import { PopupMenuComponent } from './components/popup-menu/popup-menu.component';

@NgModule({
  imports: [
    CommonModule,
    PopupRoutingModule
  ],
  declarations: [PopupLayoutComponent, PopupMenuComponent]
})
export class PopupModule { }
