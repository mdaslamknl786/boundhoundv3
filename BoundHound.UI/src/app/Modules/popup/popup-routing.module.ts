import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';

import { PopupLayoutComponent } from './components/popup-layout/popup-layout.component';
import { PopupMenuComponent } from './components/popup-menu/popup-menu.component';

const popupRoutes: Routes = [
  {
    path: '', component: PopupLayoutComponent,

    children: [
      {
        path: 'menu',
        component: PopupMenuComponent
        
      },
      
    ]
  }
];


@NgModule({
  imports: [RouterModule.forChild(popupRoutes)],
  exports: [RouterModule]
})
export class PopupRoutingModule { }
