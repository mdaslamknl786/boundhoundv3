import { CoreModule } from './../../Shared/core/core.module';
import { SessionStorageService } from './../../Shared/sessionstorage.service';
import { ApiService } from './../../Services/api.service';
import { SharedService } from './../../Services/shared.service';
import { LinkedinService } from './../../Services/linkedin.service';

import { SharedModule } from './../../Shared/shared/shared.module';
import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';

import { EmailRoutingModule } from './email-routing.module';
import { EmailListComponent, DialogOverviewExampleDialog } from './components/email-list/email-list.component';
import { EmailLayoutComponent } from './components/email-layout/email-layout.component';
import { EmailCreateComponent } from './components/email-create/email-create.component';

import { HttpModule } from '@angular/http';
import { HttpClientModule } from '@angular/common/http';
import { EmailDetailsComponent } from './components/email-details/email-details.component';

//import { CrisisService } from './../../Shared/crisis.service';
import { CrisisService } from './crisis.service';
import { EmailCenterComponent } from './components/email-center/email-center.component';
import { FormsModule } from '@angular/forms';

import { EmailService } from './email.service';
import { ListserviceService } from './../../Services/listservice.service';

import { ExcelEmailService } from './../../Services/excel.email.services';
import { FileUtil } from './../../Services/file.util';
import { PapaParseModule } from 'ngx-papaparse';
import { MovelistComponent } from './components/movelist/movelist.component';
import { AuthGuard } from '../../auth-guard.service';


@NgModule({
  imports: [
    //NgbModule,
    CommonModule,
    HttpModule,
    HttpClientModule,
    EmailRoutingModule,
    CoreModule,
    FormsModule,
    PapaParseModule
  ],
  entryComponents:[DialogOverviewExampleDialog,MovelistComponent],
  declarations: [EmailListComponent, EmailLayoutComponent, EmailCreateComponent, EmailDetailsComponent,
     EmailCenterComponent,DialogOverviewExampleDialog,  MovelistComponent],
  providers:[LinkedinService,SharedService,ApiService,SessionStorageService,
    CrisisService,EmailService,ExcelEmailService,FileUtil,ListserviceService,AuthGuard]
})
export class EmailModule { }
