import {
  Component, Directive, ElementRef, Input, AfterViewInit,
  OnInit, ViewChild, HostListener, EventEmitter, Inject
} from '@angular/core';
import { HttpHeaders } from '@angular/common/http';
import { HttpClientModule } from '@angular/common/http';
import { Observable } from 'rxjs/Observable';
import { debounce } from 'rxjs/operator/debounce';
import { MatTableDataSource, MatSort, MatPaginator, MatSnackBar } from '@angular/material';
import { ActivatedRoute, ParamMap } from '@angular/router';
import { SelectionModel } from '@angular/cdk/collections';
import { ExcelEmailService } from './../../../../Services/excel.email.services';
import { Angular2Csv } from 'angular2-csv/Angular2-csv';
import { utils, write, WorkBook } from 'xlsx';
import { saveAs } from 'file-saver';
import { PapaParseService } from 'ngx-papaparse';
import { Subscription } from "rxjs/Subscription";
import { Subject } from 'rxjs';
import { FormControl, Validators } from '@angular/forms';


import 'rxjs/add/operator/switchMap';
import { LinkedinService } from './../../../../Services/linkedin.service';
import { Email, Emails, ExportEmails } from '../../../../Models/Emails';
import { EmailList, EmailService } from './../../email.service';
import { SessionStorageService } from '../../../../Shared/sessionstorage.service';
import { ListserviceService } from './../../../../Services/listservice.service';
import { Constants } from './../../../../Constants/CSV.Constants';
import { FileUtil } from './../../../../Services/file.util';
import { CrisisService } from './../../crisis.service';
import { Crisis } from './../../crisis';
import {MatDialog, MatDialogRef, MAT_DIALOG_DATA} from '@angular/material';
import { Lists } from '../../../../Models/List';
import { MovelistComponent } from '../movelist/movelist.component';
@Component({
  selector: 'app-email-list',
  templateUrl: './email-list.component.html',
  styleUrls: ['./email-list.component.css'],
  host: {
    '(window:resize)': 'onResize($event)'
  }
})

// @Directive({ 
//   selector: '[showCoulmn]' 
// })
export class EmailListComponent implements OnInit, AfterViewInit {
  // @Input() showInput: string;
  //crises$: Observable<Crisis[]>;
  ShowSelectedProfileIDS:string;
  HeaderTitle:string;
  ListFormMode:string;
  animal: string;
  name: string;
  foods = [
    {value: '0', viewValue: 'Default'},
    {value: '1', viewValue: 'Programmer'},
    {value: '2', viewValue: 'Test'}
  ];

  //listsControl = new FormControl('', [Validators.required]);
  listsControl = new FormControl('');

  lists = [
    {value: '1', viewValue: 'Default'},
    {value: '2', viewValue: 'Programmers'},
    {value: '3', viewValue: 'Test'}
  ];
  highlightedRows = [];
  SelectedConnectionsIds=[];
  DefaultSelectedList:string = '1';
  MyListInfo:Lists;
  SelectedList:number;
  SelectedConnections:any;
  CSVExportFileName:string;
  ExcelExportFileName:string;
  emaillists$: Observable<EmailList[]>;
  public ImportCSVResult=[];
  CSVResult$:Observable<EmailList[]>;
  private itemSubscription: Subscription;
  private subject = new Subject<any>();
  Listss: Array<any> = [];
  subject1 = new Subject();
  EmailListId: number;
  selectedId: number;
  showdropdown: boolean = false;
  showActions:boolean=false;
  fileReaded:Blob;
  selectedImageFile:string;
  FileImportUpload:boolean=false;
  //'company','website','location','Url'
  displayedColumns = ['select', 'emaillistId', 'photourl', 'firstname', 'lastname', 'title', 'email', 'phone', 'mobile'];
  displayedColumns1 = ['position', 'name', 'weight', 'symbol'];
  displayedColumns2 = ['select', 'firstname'];
  displayedColumns3 = ['emaillistId', 'firstname'];
  dataSource = ELEMENT_DATA;;
  selectedRowIndex: number = -1;
  EmailDataSource = new MatTableDataSource<Email>();
  @ViewChild(MatSort) sort: MatSort;
  //@ViewChild('divTable') divTableRef:ElementRef;
  @ViewChild(MatPaginator) paginator: MatPaginator;
  @ViewChild("ImportFile", { read: ElementRef }) ImportFileRef: ElementRef;
  @ViewChild("divTable", { read: ElementRef }) divTableRef: ElementRef;
  @ViewChild("EmailListTable",{ read: ElementRef }) EmailListTable: ElementRef;
  @ViewChild('fileImportInput')
  hide = true;
  
  fileImportInput: any;
  csvRecords = [];

  //EmailsList = [];
  EmailsList: Email[] = [];
  CSVEmailsList: Email[] = [];
  ExportEmailsList:ExportEmails[]=[];
  ExportEmailInfo:ExportEmails;
  emaildetail: Emails;
  GBDynamicWidth: number = 350;
  GBDynamicHeight: number = 160;

  OnlyWithEmails: boolean = false;
  // const initialSelection = [];
  // const allowMultiSelect = true;
  selection = new SelectionModel<Email>(true, []);
  @Input() clicked = new EventEmitter<Emails>();
  constructor(private linkedservice: LinkedinService, private service: CrisisService, private route: ActivatedRoute,
    private es: EmailService, private elRef: ElementRef,
    private excelService: ExcelEmailService,
    private _fileUtil: FileUtil,
    private papa: PapaParseService,
    private sessionService: SessionStorageService,
    private listService: ListserviceService,
    public dialog: MatDialog ) {
    //this.excelService = excelService;
  }

  @HostListener('window:resize', ['$event'])

  //Dialog Window Code Starts Here
  // CreateList(event): void {
  //   let dialogRef = this.dialog.open(DialogOverviewExampleDialog, {
  //     width: '250px',
  //     data: { name: this.name, animal: this.animal }
  //   });

  //   dialogRef.afterClosed().subscribe(result => {
  //     console.log('The dialog was closed');
  //     this.animal = result;
  //   });
  // }

//Dialog Window Code Ends Here

  onResize(event) {
    //
    var WindowInnerWidth = event.target.innerWidth;
    var WindowInnerHeight = event.target.innerHeight
    //var windowwidth = $(window).width();
    //this.loadSolidGauge(this.WidgetType, this.numberformatservice);

    console.log(" Window InnerWidth " + WindowInnerWidth + " Window InnerHeight " + WindowInnerHeight);
    //
    var div = this.elRef.nativeElement.querySelector('div');
    if (this.divTableRef) {
      this.divTableRef.nativeElement.height = WindowInnerHeight;
    }
  }

  applyFilter(filterValue: string) {
    filterValue = filterValue.trim(); // Remove whitespace
    filterValue = filterValue.toLowerCase(); // MatTableDataSource defaults to lowercase matches
    console.log("Filter Value " +filterValue);
    this.EmailDataSource.filter = filterValue;
  }

  onListChange(event) {
    ////debugger;
    this.SelectedList = event.value;

    // switch (event.value) {
    //   case '"CreateList':
    //     confirm("Green is the color of balance and growth.");
    //     break;
    //   case "blue":
    //     confirm("Blue is the color of trust and peace.");
    //     break;
    //   default:
    //     confirm("Sorry, that color is not in the system yet!");
    // }
   
      if (this.SelectedList == undefined) {
        this.getEmail(0);
      }
      else {
        this.getEmail(this.SelectedList);
      }
  
  }
  ImportFileCancel(event)
  {
    this.FileImportUpload=false;
  }
  openImportToList(event){
    //this.ImportFileRef.nativeElement.top=event.clientX+100;
    //this.ImportFileRef.nativeElement.left=event.clientY+10;
  //  this.ImportFileRef.nativeElement.width=event.target.parentElement.parentElement.clientWidth/2;
    this.FileImportUpload=true;
  }

  SaveEmailDetail(emails: Emails) {
    //
    let dd = emails;
  }
  CreateList(Mode): void {
    debugger;
    if(Mode == undefined)
    {
      return;
    }
    if(Mode==1)
    {
      this.HeaderTitle="Create ";
      this.MyListInfo= new Lists();
      this.MyListInfo.Id=0;
      this.MyListInfo.ListId=0;
      this.MyListInfo.listName='';
      this.MyListInfo.listDesc='';
      this.MyListInfo.TenantId=1;
      this.MyListInfo.UserId=1;
      this.MyListInfo.LastUpdate=new Date();
      this.MyListInfo.IsActive=true;
    }
    else
    {
      debugger;
      let EmailInfo=this.listsControl.value;
      let ListInfo=this.Listss.find(x=>x.ListId==this.listsControl.value);
      let ListName=ListInfo.ListName;
      this.HeaderTitle="Edit " + ListName;

      this.MyListInfo= new Lists();
      this.MyListInfo.Id=ListInfo.ListId;
      this.MyListInfo.ListId=ListInfo.ListId;
      this.MyListInfo.listName=ListInfo.ListName;
      this.MyListInfo.listDesc=ListInfo.ListDesc;
      this.MyListInfo.TenantId=ListInfo.TenantId;
      this.MyListInfo.UserId=ListInfo.UserId;
      this.MyListInfo.LastUpdate=ListInfo.LastUpdate;
      this.MyListInfo.IsActive=ListInfo.IsActive;

    }
    let dialogRef = this.dialog.open(DialogOverviewExampleDialog, {
      width: '350px',
      height:'300px',
      disableClose: true,
      data: { name: this.name, animal: this.animal,FormMode:Mode,HeaderTitle:this.HeaderTitle,ListData:this.MyListInfo }
    });

    //Dialog before close
    dialogRef.beforeClose().subscribe(result => {
      let mlistname=dialogRef
      // var Result = this.isSpclChar(dialogRef.componentInstance.data.PageName)
      // if (Result == true) {
      //   $('#ProfileName_Validation').show();

      // }
      // else {
      //   $('#ProfileName_Validation').hide();
      //   console.log(dialogRef.componentInstance.data.btnVal);
      //   dialogRef.disableClose = false;
      // }

    });
    //Dialog after closed
    dialogRef.afterClosed().subscribe(result => {
      //debugger;
      if (dialogRef.componentInstance.data.SaveClick == "NO") {
          console.log('in No btnClick');
      }
      else if (dialogRef.componentInstance.data.SaveClick == 'YES') {
          console.log('in Yes btnClick');
          this.getList()
      }
      // 
  });

  }
  gotupdated(emails: Emails): void {

    let m = emails;
    var updateItem = this.EmailsList.find(x => x.emaillistId == emails.emaillistId);
    let index = this.EmailsList.indexOf(updateItem);

    this.EmailsList[index].firstname = emails.firstname;
    this.EmailsList[index].lastname = emails.lastname;
    this.EmailsList[index].email = emails.email;
    // this.EmailsList[index].phone=emails.phone;
    // this.EmailsList[index].mobile=emails.mobile;
    // this.EmailsList[index].title=emails.title;
    // this.EmailsList[index].company=emails.company;
    // this.EmailsList[index].website=emails.website;
    //this.clicked.emit(emails);


    const self = this;
    this.linkedservice.UpdateEmail(emails).subscribe(
      (data: any) => {
        //
        //this.messageUtils.showSuccessMessage('Vendor updated successfully');
        //this.router.navigate(['/finance/settings/vendor-grid']);
      },
      err => {
        console.log(err);
        // self.snackBar.open(err.error.ExceptionMessage, null, {
        //   duration: 5000, verticalPosition: 'top',
        //   horizontalPosition: 'right', panelClass: 'patasala-snackbar'
        // });
        //
      }
    );


    //this.linkedservice.UpdateEmail(emails);
  }
  ngAfterViewInit(): void {
    //this.dataSource.paginator = this.paginator;
    //
    this.EmailDataSource.paginator = this.paginator;
    //this.elRef.nativeElement.style.display = this.showInput;
  }
  ngOnInit() {
    //this.GetEmails();
    this.CSVExportFileName="EmailsLists_CSV",
    this.ExcelExportFileName="EmailsLists.xlsx";
  
    this.getList();
    //this.getEmail(this.DefaultSelectedList);


    this.service.addCrisis('Crisis 2');
    this.service.addCrisis('Crisis 3');
    this.service.addCrisis('Crisis 4');

    this.es.addEmails('test');

    // this.emaillists$ = this.route.paramMap
    //   .switchMap((params: ParamMap) => {
    //     //
    //     this.selectedId = +params.get('id');
    //     return this.es.getEmailsList();
    //   });

    // this.crises$ = this.route.paramMap
    //   .switchMap((params: ParamMap) => {
    //     
    //     this.selectedId = +params.get('id');
    //     return this.service.getCrises();
    //     //return 1;
    //   });

  }

  showDropDown() {
    ////debugger;
    this.showdropdown = !this.showdropdown
  }

  showActionsMenu(event) {
    this.showActions=!this.showActions;
  }
  FromLinkedIn(event) {
    this.GetFromLinkedin(5);
  }
  /** Selects all rows if they are not all selected; otherwise clear selection. */
  masterToggle() {
    this.isAllSelected() ?
      this.selection.clear() :
      //this.dataSource.data.forEach(row => this.selection.select(row));
      this.EmailDataSource.data.forEach(row => this.selection.select(row));
  }
  isAllSelected():any {
    const numSelected = this.selection.selected.length;
    //const numRows = this.dataSource.data.length;
    const numRows = this.EmailDataSource.data.length;
    return numSelected == numRows;
  }
  //Actions Menu Events Starts here
  SearchBy(event,FieldName){

  }
  toCSV(obj, separator):any {
    var arr = [];

    for (var key in obj) {
        if (obj.hasOwnProperty(key)) {
            arr.push(obj[key]);
        }
    }

    return arr.join(separator || ",");
}
  openDeleteList(event){
    alert("Delete List " + this.GetSelectedProfileIDS());
  }

  GetSelectedProfileIDS():string {
    this.SelectedConnectionsIds=[];
    let SelectedProfileIds:string;
    let SelectedRows=this.selection.selected;
    SelectedRows.forEach(SelectedConnections => {
      this.SelectedConnectionsIds.push(SelectedConnections.emaillistId)
    });
    SelectedProfileIds=this.toCSV(this.SelectedConnectionsIds,',');

    return SelectedProfileIds;
  }

  NoOfProfilesSelected(): number {
    debugger;
    let noofprofileselected:number;
    let ShowSelectedProfileIDS=this.GetSelectedProfileIDS();
    if (ShowSelectedProfileIDS != "") {
      let SplitStr = ShowSelectedProfileIDS.split(',');
      noofprofileselected = this.GetSelectedProfileIDS().split(',').length;
    }
    else {
      noofprofileselected = 0;
    }
    return noofprofileselected;
  }

  MoveToList(event) {
    debugger;
    if(this.NoOfProfilesSelected()>0)
    {
      this.SelectedConnections=this.GetSelectedProfileIDS();
      //alert("Selected Profiles " +this.SelectedConnections);
      
    }
    else
    {
      alert("Please select atleast 1 profile ");
      return;
    }

    //debugger;
    this.HeaderTitle="Move to list ";
    
    let dialogRef = this.dialog.open(MovelistComponent, {
      width: '350px',
      height:'300px',
      disableClose: true,
      data: {CurrentListID:this.SelectedConnections, HeaderTitle:this.HeaderTitle,ListData:this.Listss,
        SelectedConnections:this.GetSelectedProfileIDS() }
    });

    //Dialog before close
    dialogRef.beforeClose().subscribe(result => {
      let mlistname=dialogRef
      //debugger;
      if(this.SelectedList==dialogRef.componentInstance.data.SelectedListID)
      {
        alert("You Selected Current List Id" +this.SelectedList);
        return;
      }
      // var Result = this.isSpclChar(dialogRef.componentInstance.data.PageName)
      // if (Result == true) {
      //   $('#ProfileName_Validation').show();

      // }
      // else {
      //   $('#ProfileName_Validation').hide();
      //   console.log(dialogRef.componentInstance.data.btnVal);
      //   dialogRef.disableClose = false;
      // }

    });
    //Dialog after closed
    dialogRef.afterClosed().subscribe(result => {
      //debugger;
      let selectedlistid=dialogRef.componentInstance.data.SelectedListID;
      if (dialogRef.componentInstance.data.SaveClick == "NO") {
          console.log('in No btnClick');
      }
      else if (dialogRef.componentInstance.data.SaveClick == 'YES') {
          console.log('in Yes btnClick');
          this.getList()
      }
      // 
  });


  }
  SendMessage(event) {
    //debugger;
    if(this.NoOfProfilesSelected()>0)
    {
      let SendProfileIDS=this.GetSelectedProfileIDS();
      //alert("Selected Profiles " +SendProfileIDS);
      this.linkedservice.SendMessage(SendProfileIDS).subscribe(
        (data: any) => {
          //debugger;
          //
          //this.messageUtils.showSuccessMessage('Vendor updated successfully');
          //this.router.navigate(['/finance/settings/vendor-grid']);
        },
        err => {
          console.log(err);
          // self.snackBar.open(err.error.ExceptionMessage, null, {
          //   duration: 5000, verticalPosition: 'top',
          //   horizontalPosition: 'right', panelClass: 'patasala-snackbar'
          // });
          //
        }
      );
    }
    else
    {
      alert("Please select atleast 1 profile ");
    }
  }

  SendInvitations(event) {
    alert("Send Invitations");
  }

  //Actions Menu Events Ends here

  getList(): void {
    ////debugger;
    this.listService.GetList(1,1).subscribe((listResult) => {
      this.Listss=listResult;
      ////debugger;
      if (this.Listss.length > 0) {
        
        this.DefaultSelectedList = this.Listss[0].ListId;
        this.listsControl.setValue(this.DefaultSelectedList);
        this.getEmail(Number(this.DefaultSelectedList));
      }
    });
    // this.locationService.getAllLocations().subscribe((locationResult) => {
    //   this.locations = locationResult;
    // });
  }

  getEmail(listId:number): void {
    let reqHeaders = new HttpHeaders();
    let emails = <Observable<Array<Email>>>this.linkedservice.GetEmailList(1,1,listId);
    emails.subscribe((emailsListResult) => {
      ////debugger;
      this.EmailsList=[];
      if (emailsListResult.length > 0) {
        for (let i in emailsListResult) {
          this.EmailsList.push(emailsListResult[i]);
        }

        //this.EmailDataSource=null;

        this.EmailDataSource = new MatTableDataSource<Email>(this.EmailsList);
        this.EmailDataSource.sort = this.sort;
        this.EmailDataSource.paginator = this.paginator;

        this.selectRow(this.EmailsList[0]);
      }
      else
      {
        this.EmailDataSource=null;
      }
      //
    });
  }

  GetFromLinkedin(noofconnections: number): void {
    let reqHeaders = new HttpHeaders();

    let emails = <Observable<Array<Email>>>this.linkedservice.GetFromLinkedin(noofconnections);
    emails.subscribe((emailsListResult) => {
      //
      for (let i in emailsListResult) {
        this.EmailsList.push(emailsListResult[i]);
      }
      
      this.EmailDataSource = new MatTableDataSource<Email>(this.EmailsList);
      this.EmailDataSource.sort = this.sort;

      this.getEmail(Number(this.DefaultSelectedList));
      //
    });
  }

  fileChangeListener($event): void {

    var text = [];
    
    var target = $event.target || $event.srcElement;
    var files = target.files;
    
    if (Constants.validateHeaderAndRecordLengthFlag) {
      if (!this._fileUtil.isCSVFile(files[0])) {
        alert("Please import valid .csv file.");
        this.fileReset();
      }
    }
    var input = $event.target;
    let file = input.files[0];
    var dataset = {}; 
    this.papa.parse(file, {
      header: true,
      dynamicTyping: true,
      complete: function(results,file) {
        ////debugger;
        console.log('Parsed: ', results, file);
        this.ImportCSVResult=results;
        this.CSVResult$=results;
        dataset=results.data;
        this.itemSubscription=results.data;
        let mresults=results.data;

        //this.subject.next({ text: mresults });
        this.subject1.next(mresults);
        //this.ImportCSVSuccess(results);
        //this.sessionService.setImportCSV(mresults);
      },
      error:function(data,file) {
          alert("Error occured ");
      }
    });

    this.subject1.subscribe((value) => {
      ////debugger;
      console.log("Subscription got", value); // Subscription wont get 
                                              // anything at this point
    });
  
    // this.ImportCSVResult$.Subject(data=>{

    // })
    

    let timeoutId = setTimeout(() => {
      ////debugger;
      let k =this.subject.asObservable();
     if(this.CSVResult$ !=null)
     {
       ////debugger;
       let im=this.sessionService.getImportCSV();
       console.log("Import CSV Results " +this.CSVResult$);
       this.ImportCSVSuccess(this.CSVResult$);
     }
    }, 500);
    
    // var reader = new FileReader();
    // reader.readAsText(input.files[0]);

    // reader.onload = (data) => {
    //   ////debugger;
    //   let csvData = reader.result;
    //   let csvRecordsArray = csvData.split(/\r\n|\n/);
    //   let csvAllRecords=csvData.replace(/\r\n|\r/g,'\n');

    //   this.papa.parse(csvAllRecords,{
    //     complete: (results, file) => {
    //       ////debugger;
    //         console.log('Parsed: ', results, file);
    //     }
    // });

    //   //Modified code from Mohammed Aslam on 29-03-2018
    // //   var headerLength = -1;
    // //   if (Constants.isHeaderPresentFlag) {
    // //     let headersRow = this._fileUtil.getHeaderArray(csvAllRecords, Constants.tokenDelimeter);
    // //     headerLength = headersRow.length;
    // //   }

    // //   var content = {
    // //     csv: scope.content,
    // //     header: !scope.header,
    // //     separator: scope.separator
    // // };
    // //   let resultImport=this.csvToJSON(csvAllRecords);
    // //   this.csvRecords = this._fileUtil.getDataRecordsArrayFromCSVFile(csvAllRecords,
    // //     headerLength, Constants.validateHeaderAndRecordLengthFlag, Constants.tokenDelimeter);
      
    //   //Original Code
    //   // var headerLength = -1;
    //   // if (Constants.isHeaderPresentFlag) {
    //   //   let headersRow = this._fileUtil.getHeaderArray(csvRecordsArray, Constants.tokenDelimeter);
    //   //   headerLength = headersRow.length;
    //   // }

    //   // this.csvRecords = this._fileUtil.getDataRecordsArrayFromCSVFile(csvRecordsArray,
    //   //   headerLength, Constants.validateHeaderAndRecordLengthFlag, Constants.tokenDelimeter);

    //   console.log(this.csvRecords);
    //   let allcsvrecords=this.csvRecords;
    //   allcsvrecords.forEach(element => {
        
    //     let a=element.firstname;
        
    //   });
    //   if (this.csvRecords == null) {
    //     //If control reached here it means csv file contains error, reset file.
    //     this.fileReset();
    //     this.FileImportUpload=false;
    //   }
    // }

    // reader.onerror = function () {
    //   alert('Unable to read ' + input.files[0]);
      
    // };
  };

  fileReset() {
    this.fileImportInput.nativeElement.value = "";
    this.csvRecords = [];
  }

  ImportCSVSuccess(ImportResult:any): void {
    alert("Result : "+ImportResult);
  }


  //********************************************************************************************************************************
  //Convert CSV to JSON
  //********************************************************************************************************************************
  csvToJSON (content:any) :any {
    var lines=content.csv.split(new RegExp('\n(?![^"]*"(?:(?:[^"]*"){2})*[^"]*$)'));
    var result = [];
    var start = 0;
    var columnCount = lines[0].split(content.separator).length;

    var headers = [];
    if (content.header) {
      headers=lines[0].split(content.separator);
      start = 1;
    }

    for (var i=start; i<lines.length; i++) {
      var obj = {};
      var currentline=lines[i].split(new RegExp(content.separator+'(?![^"]*"(?:(?:[^"]*"){2})*[^"]*$)'));
      if ( currentline.length === columnCount ) {
        if (content.header) {
          for (var j=0; j<headers.length; j++) {
            obj[headers[j]] =this.cleanCsvValue(currentline[j]);
          }
        } else {
          for (var k=0; k<currentline.length; k++) {
            obj[k] =this.cleanCsvValue(currentline[k]);
          }
        }
        result.push(obj);
      }
    }
    return result;
}
cleanCsvValue (value:any) {
  return value
    .replace(/^\s*|\s*$/g,"") // remove leading & trailing space
    .replace(/^"|"$/g,"") // remove " on the beginning and end
    .replace(/""/g,'"'); // replace "" with "
};
  //********************************************************************************************************************************

  ConvertFile(csv: any) {
    
    this.fileReaded = csv.target.files[0];

    let reader: FileReader = new FileReader();
    reader.readAsText(this.fileReaded);

    reader.onload = (e) => {
      let csv: string = reader.result;
      let allTextLines = csv.split(/\r|\n|\r/);
      let headers = allTextLines[0].split(',');
      let lines = [];

      for (let i = 0; i < allTextLines.length; i++) {
        // split content based on comma  
        let data = allTextLines[i].split(',');
        if (data.length === headers.length) {
          let tarr = [];
          for (let j = 0; j < headers.length; j++) {
            tarr.push(data[j]);
          }

          // log each row to see output  
          console.log(tarr);
          lines.push(tarr);
        }
      }
      // all rows in the csv file  
      console.log(">>>>>>>>>>>>>>>>>", lines);
    }
  }
  selectRow(row) {
    this.highlightedRows.push(row);
    this.selectedRowIndex = row.emaillistId;
    this.EmailListId = row.emaillistId;
    console.log(row);
    //
    let emailinfo = this.EmailsList.find(x => x.emaillistId == this.EmailListId);

    this.emaildetail = new Emails();
    this.emaildetail.emaillistId = row.emaillistId;
    this.emaildetail.firstname = row.firstname;
    this.emaildetail.lastname = row.lastname;
    this.emaildetail.email = row.email;
    this.emaildetail.phone = row.phone;
    this.emaildetail.mobile = row.mobile;
    this.emaildetail.title = row.title;
    this.emaildetail.company = row.company;
    this.emaildetail.website = row.website;
    this.emaildetail.location = row.location;
    this.emaildetail.Url = row.Url;
    this.emaildetail.photourl = row.photourl;
  }

  ExportAsCSV(event) {
    this.showDropDown();
    debugger;
    var options = {
      fieldSeparator: ',',
      quoteStrings: '"',
      decimalseparator: '.',
      showLabels: true,
      showTitle: false,
      headers: ['First Name', 'Last Name', 'Email','Phone','Mobile','Title','Company','Website','Location','URL','Photo Url']
    };
    this.ExportEmailsList=[];
    this.EmailsList.forEach(element => {
      if (element.email != "") {
        this.ExportEmailInfo = new ExportEmails();

        this.ExportEmailInfo.firstname =element.firstname==null? "" :element.firstname;
        this.ExportEmailInfo.lastname  =element.lastname ==null? "" :element.lastname ;
        this.ExportEmailInfo.email     =element.email    ==null? "" :element.email    ;
        this.ExportEmailInfo.phone     =element.phone    ==null? "" :element.phone    ;
        this.ExportEmailInfo.mobile    =element.mobile   ==null? "" :element.mobile   ;
        this.ExportEmailInfo.title     =element.title    ==null? "" :element.title    ;
        this.ExportEmailInfo.company   =element.company  ==null? "" :element.company  ;
        this.ExportEmailInfo.website   =element.website  ==null? "" :element.website  ;
        if(element.location !=null)
        {
          //this.ExportEmailInfo.location = element.location.replace(',','=');
          this.ExportEmailInfo.location = element.location;
        }
        else{
          this.ExportEmailInfo.location="";
        }
        this.ExportEmailInfo.Url = element.Url  ==null? "" :element.Url;
        this.ExportEmailInfo.photourl = element.photourl  ==null? "" :element.photourl;
        this.ExportEmailsList.push(this.ExportEmailInfo);
      }
    });

    
    if (this.OnlyWithEmails) {
      let OnlyWithEmailsList = this.ExportEmailsList.filter(it => {
        return it.email != "";
      });
      new Angular2Csv(OnlyWithEmailsList, this.CSVExportFileName, options);
    }
    else {
      new Angular2Csv(this.ExportEmailsList, this.CSVExportFileName, options);
    }
    
    
    //new Angular2Csv(this.allItems, 'My Report', options);
    //new Angular2Csv(dummyData, 'My Report',options);
  }

  ExportAsExcel(event) {
    this.showDropDown();
    
    //this.excelService.exportAsExcelFile(this.matData.dataArray, this.matData.FileName);
    //let EmailData=JSON.parse(this.EmailsList.toString());
    this.ExportEmailsList=[];
    this.EmailsList.forEach(element => {
      if (element.email != "") {
        this.ExportEmailInfo = new ExportEmails();

        this.ExportEmailInfo.firstname = element.firstname;
        this.ExportEmailInfo.lastname = element.lastname;
        this.ExportEmailInfo.email = element.email;
        this.ExportEmailInfo.phone = element.phone;
        this.ExportEmailInfo.mobile = element.mobile;
        this.ExportEmailInfo.title = element.title;
        this.ExportEmailInfo.company = element.company;
        this.ExportEmailInfo.website = element.website;
        this.ExportEmailInfo.location = element.location;
        this.ExportEmailInfo.Url = element.Url;
        this.ExportEmailInfo.photourl = element.photourl;

        this.ExportEmailsList.push(this.ExportEmailInfo);
      }
    });

    const ws_name = 'Emails List';
    const wb: WorkBook = { SheetNames: [], Sheets: {} };
    const ws: any = utils.json_to_sheet(this.ExportEmailsList);
    wb.SheetNames.push(ws_name);
    wb.Sheets[ws_name] = ws;
    const wbout = write(wb, { bookType: 'xlsx', bookSST: true, type: 'binary' });

    function s2ab(s) {
      const buf = new ArrayBuffer(s.length);
      const view = new Uint8Array(buf);
      for (let i = 0; i !== s.length; ++i) {
        view[i] = s.charCodeAt(i) & 0xFF;
      };
      return buf;
    }
    saveAs(new Blob([s2ab(wbout)], { type: 'application/octet-stream' }), this.ExcelExportFileName);

    // let EmailData=JSON.stringify(this.EmailsList);
    // let EmailDataList=JSON.parse(EmailData);
    // this.excelService.exportAsExcelFile(this.EmailsList, this.ExcelExportFileName);

    // var options = {
    //   fieldSeparator: ',',
    //   quoteStrings: '"',
    //   decimalseparator: '.',
    //   showLabels: true,
    //   showTitle: false,
    //   headers: ['ID','First Name', 'Last Name', 'Email','Phone','Mobile','Title','Company','Website','Location','URL','Photo Url']
    // };
    // new Angular2Csv(this.EmailsList, this.CSVExportFileName, options);
    
  }


 }

export interface Element {
  name: string;
  position: number;
  weight: number;
  symbol: string;
}

const ELEMENT_DATA: Element[] = [
  { position: 1, name: 'Hydrogen', weight: 1.0079, symbol: 'H' },
  { position: 2, name: 'Helium', weight: 4.0026, symbol: 'He' },
  { position: 3, name: 'Lithium', weight: 6.941, symbol: 'Li' },
  { position: 4, name: 'Beryllium', weight: 9.0122, symbol: 'Be' },
  { position: 5, name: 'Boron', weight: 10.811, symbol: 'B' },
  { position: 6, name: 'Carbon', weight: 12.0107, symbol: 'C' },
  { position: 7, name: 'Nitrogen', weight: 14.0067, symbol: 'N' },
  { position: 8, name: 'Oxygen', weight: 15.9994, symbol: 'O' },
  { position: 9, name: 'Fluorine', weight: 18.9984, symbol: 'F' },
  { position: 10, name: 'Neon', weight: 20.1797, symbol: 'Ne' },
  { position: 11, name: 'Sodium', weight: 22.9897, symbol: 'Na' },
  { position: 12, name: 'Magnesium', weight: 24.305, symbol: 'Mg' },
  { position: 13, name: 'Aluminum', weight: 26.9815, symbol: 'Al' },
  { position: 14, name: 'Silicon', weight: 28.0855, symbol: 'Si' },
  { position: 15, name: 'Phosphorus', weight: 30.9738, symbol: 'P' },
  { position: 16, name: 'Sulfur', weight: 32.065, symbol: 'S' },
  { position: 17, name: 'Chlorine', weight: 35.453, symbol: 'Cl' },
  { position: 18, name: 'Argon', weight: 39.948, symbol: 'Ar' },
  { position: 19, name: 'Potassium', weight: 39.0983, symbol: 'K' },
  { position: 20, name: 'Calcium', weight: 40.078, symbol: 'Ca' },
];

export interface Email {
  emaillistId: number;
  firstname: string;
  lastname: string;
  email: string;
  phone: string;
  mobile: string;
  title: string;
  company: string;
  website: string;
  location: string;
  Url: string;
  photourl: string;
}

@Component({
  selector: 'dialog-overview-example-dialog',
  templateUrl: 'dialog-overview-example-dialog.html',
})
export class DialogOverviewExampleDialog {

  MyListID:any;
  constructor(
    public dialogRef: MatDialogRef<DialogOverviewExampleDialog>,
    @Inject(MAT_DIALOG_DATA) public data: any,
    private listService: ListserviceService,
    public snackBar: MatSnackBar, ) { }

  onNoClick(): void {
    //debugger;
    this.data.SaveClick="NO";
    this.dialogRef.close(this.MyListID);
  }
  YesClick(event,listData): void {
    const self = this;
    //debugger;
    if (this.data.FormMode == 1) {
      //Create
      this.listService.CreateList(listData, this.data.ListData.TenantId, this.data.ListData.UserId).subscribe(
        (data: any) => {
          this.MyListID = data.Data;
          this.dialogRef.close(this.MyListID);
          if(data.StatusCode=="SUCCESS")
          {
            this.data.SaveClick="YES";
          }
          self.snackBar.open(data.Message, null, {
            duration: 5000, verticalPosition: 'top',
            horizontalPosition: 'right', panelClass: 'boundhound-snackbar'
          });
        },
        err => {
          console.log(err);
          this.data.SaveClick="NO";
          self.snackBar.open("Error Occured", null, {
            duration: 5000, verticalPosition: 'top',
            horizontalPosition: 'right', panelClass: 'boundhound-snackbar'
          });
        }
      );
    }
    else {
      //Update
      this.listService.UpdateList(listData, this.data.ListData.TenantId, this.data.ListData.UserId).subscribe(
        (data: any) => {
          //debugger;
          this.MyListID = data;
          this.data.ListData=data;
          this.dialogRef.close(this.MyListID);
          this.data.SaveClick="YES";
          self.snackBar.open("Updated Successfully", null, {
            duration: 5000, verticalPosition: 'top',
            horizontalPosition: 'right', panelClass: 'boundhound-snackbar'
          });
        },
        err => {
          console.log(err);
          this.data.SaveClick="NO";
          self.snackBar.open("Error Occured", null, {
            duration: 5000, verticalPosition: 'top',
            horizontalPosition: 'right', panelClass: 'boundhound-snackbar'
          });
        }
      );
    }
  }
}