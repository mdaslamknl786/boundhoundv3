import { Component, OnInit, Inject,AfterViewInit } from '@angular/core';
import { MAT_DIALOG_DATA, MatSnackBar, MatDialogRef } from '@angular/material';
import { ListserviceService } from '../../../../Services/listservice.service';
import { FormControl } from '@angular/forms';

@Component({
  selector: 'dialog-movielist',
  templateUrl: './movelist.component.html',
  styleUrls: ['./movelist.component.css']
})
export class MovelistComponent implements OnInit,AfterViewInit {

  ngAfterViewInit(): void {
    //debugger;
    this.MoveLists=this.data.ListData;
    
  }
  MoveListsControl = new FormControl('');
  
  constructor(public dialogRef: MatDialogRef<MovelistComponent>,
    @Inject(MAT_DIALOG_DATA) public data: any,
    private listService: ListserviceService,
    public snackBar: MatSnackBar, ) { }
    
    MoveListSelected:string = '1';
    MoveLists: Array<any> = [];
    SelectedList:number;
    TenantId:number;
    UserId:number;
    MyListID:any;
    
  ngOnInit() {
    //this.MoveLists==this.data.ListData;
  }
  onMoveListChange(event) {
    //debugger;
    this.SelectedList = event.value;
   
    // if (this.SelectedList == undefined) {
    //   this.getEmail(0);
    // }
    // else {
    //   this.getEmail(this.SelectedList);
    // }
  
  }
  getList(): void {
    //debugger;
    this.listService.GetList(1,1).subscribe((listResult) => {
      this.MoveLists=listResult;
      //debugger;
      if (this.MoveLists.length > 0) {
        
        this.MoveListSelected = this.MoveLists[0].ListId;
        this.MoveListsControl.setValue(this.MoveLists);
        
      }
    });
  }

  onNoClick(): void {
    //debugger;
    this.data.SaveClick = "NO";
    this.dialogRef.close();
  }
  YesClick(event, listData): void {
    const self = this;

    //debugger;
    this.SelectedList=Number(this.MoveListsControl.value);
    this.TenantId=this.data.ListData[0].TenantId;
    this.UserId=this.data.ListData[0].UserId;
    this.listService.MoveToList(this.data.SelectedConnections, this.SelectedList, this.TenantId, this.UserId).subscribe(
      (data: any) => {
        //debugger;
        if(data.StatusCode=="SUCCESS")
        {
          this.data.SaveClick="YES";
        }
        self.snackBar.open(data.Message, null, {
          duration: 5000, verticalPosition: 'top',
          horizontalPosition: 'right', panelClass: 'boundhound-snackbar'
        });
      },
      err => {
        console.log(err);
        this.data.SaveClick="NO";
        self.snackBar.open("Error Occured", null, {
          duration: 5000, verticalPosition: 'top',
          horizontalPosition: 'right', panelClass: 'boundhound-snackbar'
        });
      }
    );
    this.data.SelectedListID=this.SelectedList;
  }
}

