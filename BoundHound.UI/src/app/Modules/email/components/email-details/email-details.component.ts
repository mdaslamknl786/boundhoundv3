import { LinkedinService } from './../../../../Services/linkedin.service';
import { Emails } from './../../../../Models/Emails';
import { Component, OnInit, HostBinding, Input, EventEmitter, Output } from '@angular/core';
import { ActivatedRoute, Router } from '@angular/router';
import { Observable } from 'rxjs/Observable';
// import { Crisis }         from './crisis.service';
// import { DialogService }  from '../dialog.service';
import { slideInDownAnimation } from './../../../../Shared/animations';
import { DialogService } from './../../../../Shared/dialog.service';
import { Crisis } from './../../crisis';
import { EmailList } from '../../email.service';
import { MatSnackBar } from '@angular/material';
import { Validators, FormGroup, FormBuilder, FormArray, FormControl } from '@angular/forms';
import { STRING_PATERN } from '../../../../Shared/generic';
import { Email } from '../email-list/email-list.component';
import { EmailValidator } from '@angular/forms';
@Component({
   selector: 'app-email-details',
   templateUrl: './email-details.component.html',
   styleUrls: ['./email-details.component.css'],
//   template: `
//   <div *ngIf="crisis">
//   <h3>"{{ editName }}"</h3>
//   <div>
//     <label>Id: </label>{{ crisis.id }}</div>
//   <div>
//     <label>Name: </label>
//  <input [(ngModel)]="editName" placeholder="name"/>
//   </div>
//   <p>
//     <button (click)="save()">Save</button>
//     <button (click)="cancel()">Cancel</button>
//   </p>
// </div>
//   `,
//   styles: ['input {width: 20em}'],
  animations: [ slideInDownAnimation ]
})
export class EmailDetailsComponent implements OnInit {
  IsValidEmail:boolean=false;
  isValidFormSubmitted = null;
  emailPattern = "^[a-z0-9._%+-]+@[a-z0-9.-]+\.[a-z]{2,4}$";
  userForm = this.fb.group({
     primaryEmail: ['', Validators.email],
     secondaryEmail: '',
     officialEmail: ['', [Validators.required, Validators.pattern(this.emailPattern)]]
  });
  emailForm=this.fb.group({
    emaillistId:'0',
    firstname:['', { validators: [Validators.pattern(STRING_PATERN), Validators.required] }],
    lastname:['', { validators: [Validators.pattern(STRING_PATERN), Validators.required] }],
    email: ['', [Validators.required, Validators.pattern(this.emailPattern)]],
    phone:'',
    mobile:'',
    title:'',
    company:'',
    website:'',
    location:'',
  });

  @HostBinding('@routeAnimation') routeAnimation = true;
  @HostBinding('style.display')   display = 'block';
 // @HostBinding('style.position')  position = 'absolute';
  @Input() EmailDetails: any;
  @Output() UpatedEmailDetails: EventEmitter<Emails> = new EventEmitter();
  //crisis: Crisis;
  emails:Emails;
  editName: string;
  showDetail:boolean=true;
  editCurrentShow:boolean=true;
  findCEmailSearching:boolean=true;
  EmailPresent:boolean=true;
  linkedin_url:boolean=false;
  emaillistid:number;
  SearchEmailFound:boolean=false;
  
  constructor(
    private route: ActivatedRoute,
    private router: Router,
    public dialogService: DialogService,
    private linkedinservice:LinkedinService,
    public snackBar: MatSnackBar,
    private fb:FormBuilder
  ) {}

  onSubmit(emailForm:Email)
  {

    debugger;
  }
  onFormSubmit() {
    debugger;
    this.isValidFormSubmitted = false;
    if (this.emailForm.invalid) {
       return;
    }
    this.isValidFormSubmitted = true;
    
    // this.user = this.userForm.value;
    // this.userService.createUser(this.user);
    // this.userForm.reset();
 }


  ngOnChanges(EmailDetails: any) {
    
    this.emails=new Emails();
    if (EmailDetails.EmailDetails.currentValue != null) {
      this.emaillistid = EmailDetails.EmailDetails.currentValue.emaillistId;
      this.emails.emaillistId = EmailDetails.EmailDetails.currentValue.emaillistId;
      this.emails.firstname = EmailDetails.EmailDetails.currentValue.firstname;
      this.emails.lastname = EmailDetails.EmailDetails.currentValue.lastname;

      this.emails.email = EmailDetails.EmailDetails.currentValue.email;
      if (this.emails.email) {
        this.EmailPresent = true;
        this.IsValidEmail=true;
      }
      else{
        this.EmailPresent=false;
        
      }
      this.emails.phone = EmailDetails.EmailDetails.currentValue.phone;
      this.emails.mobile = EmailDetails.EmailDetails.currentValue.mobile;
      this.emails.title = EmailDetails.EmailDetails.currentValue.title;
      this.emails.company = EmailDetails.EmailDetails.currentValue.company;
      this.emails.website = EmailDetails.EmailDetails.currentValue.website;
      this.emails.location = EmailDetails.EmailDetails.currentValue.location;
      this.emails.Url = EmailDetails.EmailDetails.currentValue.Url;
      if (this.emails.Url) {
        this.linkedin_url = true;
      }
      this.emails.photourl = EmailDetails.EmailDetails.currentValue.photourl;

      this.emailForm.patchValue(this.emails);
      

    }
  }
  findCEmailClick(event) {
    const self = this;
    //debugger;
    if (this.EmailPresent == false) {
      this.findCEmailSearching = false;
      //debugger;
      this.linkedinservice.FindEmail(this.emaillistid,1,1).subscribe(
        (data: any) => {
          //
          //this.messageUtils.showSuccessMessage('Vendor updated successfully');
          //this.router.navigate(['/finance/settings/vendor-grid']);
          //debugger;
          if (data.StatusCode == "NOTFOUND") {
            this.EmailPresent = false;
            self.snackBar.open(data.Message, null, {
              duration: 5000, verticalPosition: 'top',
              horizontalPosition: 'right', panelClass: 'boundhound-snackbar'
            });
          }
          else {
            this.emails.email = data.Data;
            this.EmailPresent = true;
            this.findCEmailSearching=true;

            self.snackBar.open(data.Message, null, {
              duration: 5000, verticalPosition: 'top',
              horizontalPosition: 'right', panelClass: 'boundhound-snackbar'
            });
            
          }
        },
        err => {
          //debugger;
          console.log(err);
          this.EmailPresent = false;
          this.findCEmailSearching=true;
          self.snackBar.open(err.error.ExceptionMessage, null, {
            duration: 5000, verticalPosition: 'top',
            horizontalPosition: 'right', panelClass: 'boundhound-snackbar'
          });    
        }
      );

      if (this.EmailPresent) {
        let emailNotFoundTimeOut = setTimeout(() => {
          this.EmailPresent = true;
        }, 500);
      }

    }

  }

  DeleteLead(event){
    alert("Delete Lead");
  }

  EditClick(event){
    
    this.editCurrentShow=!this.editCurrentShow;
  }
  SaveEmailDetail(event,memails){
    
    //this.clicked.emit(searchTerm);
    debugger;
    this.editCurrentShow=true;
    this.UpatedEmailDetails.emit(this.emails);
  }
  ngOnInit() {
    //this.editCurrentShow=false;
    // this.route.data
    //   .subscribe((data : {emails:Emails}) =>{
    //     
    //     //this.editName=data.emails.firstname;
    //     this.editName=data.emails.firstname;
    //   });

    // this.route.data
    //   .subscribe((data: { crisis: Crisis }) => {
    //     
    //     this.editName = data.crisis.name;
    //     this.crisis = data.crisis;
    //   });
    this.emailForm.get('email').setValidators(Validators.email);
  }

  cancel() {
    this.gotoCrises();
  }

  save() {
    //this.crisis.name = this.editName;
    this.gotoCrises();
  }

  canDeactivate(): Observable<boolean> | boolean {
    // Allow synchronous navigation (`true`) if no crisis or the crisis is unchanged
    // if (!this.crisis || this.crisis.name === this.editName) {
    //   return true;
    // }
    // Otherwise ask the user with the dialog service and return its
    // observable which resolves to true or false when the user decides
    return this.dialogService.confirm('Discard changes?');
  }

  gotoCrises() {
    //let crisisId = this.crisis ? this.crisis.id : null;
    // Pass along the crisis id if available
    // so that the CrisisListComponent can select that crisis.
    // Add a totally useless `foo` parameter for kicks.
    // Relative navigation back to the crises
    this.router.navigate(['../', { id: 1, foo: 'foo' }], { relativeTo: this.route });
  }
}
