import { Emails } from './../../Models/Emails';
import 'rxjs/add/observable/of';
import 'rxjs/add/operator/map';
import { BehaviorSubject } from 'rxjs/BehaviorSubject';

export class EmailList {
  emaillistId:number;
  firstname:string;
  lastname:string;
  email:string;
  phone:string;
  mobile:string;
  title:string;
  company:string;
  website:string;
  location:string;
  Url:string;
  photourl:string;
  constructor(public _emaillistId:number,public _firstname:string,public _lastname:string,public _email:string,
    public _phone:string,public _mobile:string,public _title:string,public _company:string,public _website:string,
    public _location:string,public _Url:string,public _photourl:string) {
      this.emaillistId=_emaillistId;
      this.firstname=_firstname;
     }
}

const EMAILLISTS = [
  // new EmailList(1, 'Mohammed','Aslam','mdaslamknl@gmail.com','9618958912','9618958912',
  //               'Tech Lead','Sparsh Communicatins Pvt Ltd.','www.sparsh.com','Hyderabad','www.sparsh.com'
  //               ,'www.sparshcom.com'),
  //               new EmailList(2, 'Mohammed','Aslam','mdaslamknl@gmail.com','9618958912','9618958912',
  //               'Tech Lead','Sparsh Communicatins Pvt Ltd.','www.sparsh.com','Hyderabad','www.sparsh.com'
  //               ,'www.sparshcom.com'),
];
// class Greeter {
//   greeting: string;
//   constructor(message: string) {
//       this.greeting = message;
//   }
//   greet() {
//       return "Hello, " + this.greeting;
//   }
// }
import { Injectable } from '@angular/core';
import { LinkedinService } from './../../Services/linkedin.service';
//import { environment } from '../../../environments/environment.dev';
import { ApiService } from './../../Services/api.service';
import { HttpHeaders } from '@angular/common/http';
import { Observable } from 'rxjs/Observable';
import { environment } from './../../../environments/environment';
@Injectable()
export class EmailService {
  environment
  private _webApiUrl = environment.apiEndPoint;
  static nextCrisisId = 100;
  private emailslists$: BehaviorSubject<EmailList[]> = new BehaviorSubject<EmailList[]>(EMAILLISTS);

  constructor(private apiService:ApiService,private linkedservice:LinkedinService){}
  
  getEmailsList() { 
    let getReqHeaders = new HttpHeaders();
    let emails = <Observable<Array<EmailList>>>this.linkedservice.GetEmailList(1,1,1);
    for (let i in emails) 
      {
        this.addEmails(emails[i].firstname);
      }
      return this.emailslists$;
  
    //return this.emailslists$; 
  }

  
  getEmailById(id: number) {

    let emaildetail=<Observable<Array<Emails>>>this.linkedservice.getEmailById(id);
    //
    //this.linkedservice.getEmailById(id).take(1);

    return emaildetail;
    //return this.getEmailsList().map(emailslists => emailslists.find(emailslists => emailslists.emaillistId === +id));
  }

  
  getEmail(id: number | string) {
    return this.getEmailsList()
      .map(emailslists => emailslists.find(emailslists => emailslists.emaillistId === +id));
  }

  addEmails(name: string) {
    //name = name.trim();
    if (name) {
      let crisis = new EmailList(EmailService.nextCrisisId++, name,name,'','','','','','','','','');
      EMAILLISTS.push(crisis);
      this.emailslists$.next(EMAILLISTS);
    }
  }





}