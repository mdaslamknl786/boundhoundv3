import { EmailCreateComponent } from './components/email-create/email-create.component';
import { EmailListComponent } from './components/email-list/email-list.component';
import { EmailLayoutComponent } from './components/email-layout/email-layout.component';

import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';

import { CanDeactivateGuard } from './../../Services/can-deactivate-guard.service';

import { EmailCenterComponent } from './components/email-center/email-center.component';

import { EmailDetailsResolver } from './email-details-resolver.service';
import { EmailDetailsComponent } from './components/email-details/email-details.component';
import { AuthGuard } from '../../auth-guard.service';


// const crisisCenterRoutes: Routes = [
//   {
//     path: '',
//     component: EmailLayoutComponent,
//     children: [
//       {
//         path: '',
//         component: EmailListComponent,
//         children: [
//           {
//             path: ':id',
//             component: EmailDetailsComponent,
//             data: { preload: true },
//             resolve: {
//               crisis: EmailDetailsResolver
//             }
//           },
//           {
//             path: '',
//             component: EmailLayoutComponent
//           }
//         ]
//       }
//     ]
//   }
// ];

const crisisCenterRoutes: Routes = [
  {
    path: '', component: EmailLayoutComponent,

    children: [
      {
        path: '',
        component: EmailListComponent
        
      },
      {
        path:'list',
        component: EmailListComponent
      },
      {
        path:'create',
        component: EmailCreateComponent
      }

    ]
  }
];


@NgModule({
  imports: [RouterModule.forChild(crisisCenterRoutes)],
  exports: [RouterModule],
  providers:[EmailDetailsResolver,AuthGuard]

})
export class EmailRoutingModule { }
