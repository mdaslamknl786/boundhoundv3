import { Emails } from './../../Models/Emails';
import 'rxjs/add/operator/map';
import 'rxjs/add/operator/take';
import { Injectable }             from '@angular/core';
import { Observable }             from 'rxjs/Observable';
import { Router, Resolve, RouterStateSnapshot,
         ActivatedRouteSnapshot } from '@angular/router';

//import { Crisis, CrisisService }  from './crisis.service';
import { HttpClientModule } from '@angular/common/http'; 
import { HttpModule } from '@angular/http';
import { CrisisService } from './crisis.service';

import {Crisis}  from './crisis';
import { EmailList, EmailService } from './email.service';

@Injectable()
//export class EmailDetailsResolver implements Resolve<Crisis> {
export class EmailDetailsResolver implements Resolve<Emails[]> {
  constructor(private cs: CrisisService, private router: Router,private es:EmailService) {}

    resolve(route: ActivatedRouteSnapshot, state: RouterStateSnapshot): Observable<Emails[]> {
        //
        let id =Number(route.paramMap.get('id'));
         return this.es.getEmailById(id).take(1).map(emaillist => {
            if (emaillist) {
                return emaillist;
            }
            else {
                this.router.navigate(['/list']);
                return null;
            }
        });

        // return this.es.getEmail(id).take(1).map(emaillist => {
        //     if (emaillist) {
        //         return emaillist;
        //     }
        //     else {
        //         this.router.navigate(['/list']);
        //         return null;
        //     }
        // });
    }

  // resolve(route: ActivatedRouteSnapshot, state: RouterStateSnapshot): Observable<Crisis> {
  //     
  //   let id = route.paramMap.get('id');

  //   return this.cs.getCrisis(id).take(1).map(crisis => {
  //     if (crisis) {
  //       return crisis;
  //     } else { // id not found
  //       this.router.navigate(['/email']);
  //       return null;
  //     }
  //   });
  // }



}
