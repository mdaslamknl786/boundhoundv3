import 'rxjs/add/observable/of';
import 'rxjs/add/operator/map';
import { BehaviorSubject } from 'rxjs/BehaviorSubject';

export class Crisis {
    id:number;
    name:string;
   constructor(public _id: number, public _name: string) { 
       this.id=_id;
        this.name=_name;
   }
}


