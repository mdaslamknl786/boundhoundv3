import { MessagingListComponent } from './components/messaging-list/messaging-list.component';
import { MessaginglayoutComponent } from './components/messaginglayout/messaginglayout.component';
import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';



const routes: Routes = [
  {
    path: '', component: MessaginglayoutComponent,
    // resolve: {
    //   academicYears: AcademicYearsResolver,
    // },
    children: [
      {
        path: '',
        component: MessagingListComponent
      },
      {
        path:'list',
        component: MessagingListComponent
      }
    ]
  }
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class MessagingRoutingModule { }
