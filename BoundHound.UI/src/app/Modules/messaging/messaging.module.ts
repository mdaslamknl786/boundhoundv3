import { SharedModule } from './../../Shared/shared/shared.module';
import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';

import { MessagingRoutingModule } from './messaging-routing.module';
import { MessaginglayoutComponent } from './components/messaginglayout/messaginglayout.component';
import { MessagingListComponent } from './components/messaging-list/messaging-list.component';


@NgModule({
  imports: [
    CommonModule,
    MessagingRoutingModule,
    SharedModule
  ],
  declarations: [MessaginglayoutComponent, MessagingListComponent]
})
export class MessagingModule { }
