import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { MessaginglayoutComponent } from './messaginglayout.component';

describe('MessaginglayoutComponent', () => {
  let component: MessaginglayoutComponent;
  let fixture: ComponentFixture<MessaginglayoutComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ MessaginglayoutComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(MessaginglayoutComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
