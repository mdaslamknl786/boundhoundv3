import { LoginService } from './../../../../Services/login.service';
import { Component, OnInit } from '@angular/core';
import { SocialUser } from 'ng4-social-login';
import { Router } from '@angular/router';


@Component({
  selector: 'app-dashboard',
  templateUrl: './dashboard.component.html',
  styleUrls: ['./dashboard.component.css']
})
export class DashboardComponent implements OnInit {
  isUserLoggedIn: boolean;
  MyUserInfo:SocialUser;
  constructor(public logservice: LoginService,
    private router: Router) {
   
   }
   public getUserLoggedIn(): boolean 
   { 
     this.isUserLoggedIn=this.logservice.getUserLoggedIn();
     return this.isUserLoggedIn;
     
    }
  ngOnInit() {
    this.getUserLoggedIn();
    if(!this.isUserLoggedIn)
    {
      //this.router.navigate(['/signin']);
    }
  }

}
