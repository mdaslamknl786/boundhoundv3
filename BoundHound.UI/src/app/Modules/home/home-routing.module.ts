import { DashboardComponent } from './components/dashboard/dashboard.component';
import { HomelayoutComponent } from './components/homelayout/homelayout.component';
import { ProfileListComponent } from './../profile/components/profile-list/profile-list.component';
import { ConnectionsModule } from './../connections/connections.module';

import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';


const routes: Routes = [
  {
    path: '', component: HomelayoutComponent,
    children: [
      {
        path: '',
        component: DashboardComponent,
      },
      {
        path: 'home',
        component: DashboardComponent,
      }

    ]
  }
];

// const routes: Routes = [
//   {
//     path: '',
//     component: HomeComponent
//   },
//   {
//     path:'profile',
//     loadChildren:'app/Modules/profile/profile.module#ProfileModule'
//   },
//   {
//     path:'connection',
//     component:ProfileListComponent
//   },

//   {
//     path: '',
//     redirectTo: '',
//     pathMatch: 'full'
//   }
// ];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class HomeRoutingModule { }
