

import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';

import { HomeRoutingModule } from './home-routing.module';

import { HomelayoutComponent } from './components/homelayout/homelayout.component';
import { DashboardComponent } from './components/dashboard/dashboard.component';
import { SharedModule } from './../../Shared/shared/shared.module';

@NgModule({
  imports: [
    CommonModule,
    HomeRoutingModule,
    SharedModule
  ],
  declarations: [HomelayoutComponent, DashboardComponent],
  exports :[]
})
export class HomeModule { }
