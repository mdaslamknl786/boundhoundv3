import { ProfilelayoutComponent } from './components/profilelayout/profilelayout.component';


import { ProfileListComponent } from './components/profile-list/profile-list.component';
import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';


const routes: Routes = [
  {
    path: '', component: ProfilelayoutComponent,
    // resolve: {
    //   academicYears: AcademicYearsResolver,
    // },
    children: [
      {
        path: '',
        component: ProfileListComponent
      },
      {
        path:'list',
        component: ProfileListComponent
      }
    ]
  }
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class ProfileRoutingModule { }
