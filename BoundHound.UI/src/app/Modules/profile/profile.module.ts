

import { SharedModule } from './../../Shared/shared/shared.module';
import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';

import { ProfileRoutingModule } from './profile-routing.module';
import { ProfileListComponent } from './components/profile-list/profile-list.component';
import { ProfilelayoutComponent } from './components/profilelayout/profilelayout.component';


@NgModule({
  imports: [
    CommonModule,
    ProfileRoutingModule,
    SharedModule
  ],
  declarations: [ProfileListComponent, ProfilelayoutComponent, ProfilelayoutComponent]
})
export class ProfileModule { }
