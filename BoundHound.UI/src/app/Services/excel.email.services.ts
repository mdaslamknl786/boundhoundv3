import { Injectable } from '@angular/core';
import * as FileSaver from 'file-saver';
import * as XLSX from 'xlsx';

import { SampleDataJSON } from './../Models/SampleDataJSON';
import { EmailListExport } from './../Models/EmailListExport';

import {JsonConvert, OperationMode, ValueCheckingMode} from "json2typescript"

const EXCEL_TYPE = 'application/vnd.openxmlformats-officedocument.spreadsheetml.sheet;charset=UTF-8';
const EXCEL_EXTENSION = '.xlsx';

@Injectable()
export class ExcelEmailService {

  constructor() { }
  public exportAsExcelFile(json: any[], excelFileName: string): void {

    let jsonConvert: JsonConvert = new JsonConvert();

    let sampledatalist : EmailListExport;
    //sampledatalist=jsonConvert.deserialize(json,SampleDataJSON);

    //Deserialize To Emails[]
    sampledatalist=jsonConvert.deserialize(json,EmailListExport);

    const worksheet: XLSX.WorkSheet = XLSX.utils.json_to_sheet(json);
    
    const workbook: XLSX.WorkBook = { Sheets: { 'data': worksheet }, SheetNames: ['data'] };
    const excelBuffer: any = XLSX.write(workbook, { bookType: 'xlsx', type: 'buffer' });
    this.saveAsExcelFile(excelBuffer, excelFileName);
  }

  private saveAsExcelFile(buffer: any, fileName: string): void {
    const data: Blob = new Blob([buffer], {
      type: EXCEL_TYPE
    });
    FileSaver.saveAs(data, fileName + '_export_' + new Date().getTime() + EXCEL_EXTENSION);
  }

}