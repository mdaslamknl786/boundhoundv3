import { ApiService } from './api.service';
import { SharedService } from './shared.service';
import { environment } from './../../environments/environment';
import { Injectable } from '@angular/core';
import { Http, Headers, Response, RequestOptions } from '@angular/http';
import { Observable } from "rxjs/Rx";
import 'rxjs/add/operator/map'

import 'rxjs/add/operator/map';
import { HttpHeaders } from '@angular/common/http';
import { BehaviorSubject } from 'rxjs/BehaviorSubject';
import { Lists } from './../Models/List';

@Injectable()
export class ListserviceService {
  static nextEmailListId = 100;
  private _webApiUrl = environment.apiEndPoint;

  constructor(private http: Http, private service: SharedService,private apiService: ApiService) { }
  
  // IEnumerable<ListDTO> GetList(UserInfo MyUserInfo);
  // ListDTO GetListInfo(int Id, UserInfo MyUserInfo);
  // int CreateList(ListDTO listDTO, UserInfo MyUserInfo);
  // ListDTO UpdateList(ListDTO listDTO, UserInfo MyUserInfo);
  // int DeleteList(int Id, UserInfo MyUserInfo);
  // int MoveToList(string EmailIds, int listId, UserInfo MyUserInfo);


  CreateList(listDTO :any ,tenantId:number,userId: number)
  {
    const getReqHeaders = new HttpHeaders();
    return  this.apiService.postData(`${this._webApiUrl}/list/CreateList/${tenantId}/${userId}`,listDTO, getReqHeaders);  
  }

  UpdateList(listDTO :any ,tenantId:number,userId: number)
  {
    const getReqHeaders = new HttpHeaders();
    return  this.apiService.postData(`${this._webApiUrl}/list/UpdateList/${tenantId}/${userId}`,listDTO, getReqHeaders);  
  }
  MoveToList(EmailIds: any, listId: number, tenantId: number, userId: number) {

    const getReqHeaders = new HttpHeaders();
    return this.apiService.getResults(`${this._webApiUrl}/list/MoveToList/${EmailIds}/${listId}/${tenantId}/${userId}`,  getReqHeaders);
  }
  GetList(tenantId:number,userId: number): Observable<any> {
    const getReqHeaders = new HttpHeaders();
    return this.apiService.getResults(`${this._webApiUrl}list/getlist/${tenantId}/${userId}`, getReqHeaders); 

  }

  GetList1(tenantId:number,userId: number) {
    let getReqHeaders = new HttpHeaders();
    return this.apiService.getResults(`${this._webApiUrl}list/getlist/${tenantId}/${userId}`, getReqHeaders); 
    //return this.apiService.getResults(this._webApiUrl+'GetEmails', getReqHeaders); 
  } 
  
  GetListById(tenantId:number,userId: number,listId:number) {
    let getReqHeaders = new HttpHeaders();
    return this.apiService.getResults(`${this._webApiUrl}list/getlist/${tenantId}/${userId}`, getReqHeaders); 
    //return this.apiService.getResults(this._webApiUrl+'GetEmails', getReqHeaders); 
  } 

  private extractData(res: Response) {
    let body = <any[]>res.json();
    return body || [];
  }

  private extractString(res: Response) {
    let body = res.json();
    return body || [];
  }

  private extractPageData(res: Response) {
    let body = <Lists>res.json();
    return body;
  }

  private extractUpdateData(res: Response) {
    let body = res.json();
    return body || [];
  }

  private handleError(error: any) {
    let errMsg = (error.message) ? error.message :
      error.status ? `${error.status} - ${error.statusText}` : 'Server error';
    console.error(errMsg); // log to console instead
    return Observable.throw(errMsg);
  }



}
