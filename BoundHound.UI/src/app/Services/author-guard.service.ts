import { Injectable } from '@angular/core';
import { Router, ActivatedRouteSnapshot, CanActivateChild } from '@angular/router';
import { AuthApiService } from './auth-api.service';
import { SessionStorageService } from './sessionstorage.service.';
import { AuthorApiService } from './author-api.service';

@Injectable()
export class AuthorGuardService implements CanActivateChild {
  userRoles: any = this.sessionService.getUserRole();
  isAutherized: boolean;
  constructor(
    private auth: AuthApiService,
    private router: Router,
    private sessionService: SessionStorageService,
    private authApiService: AuthApiService,
    private authorApiService: AuthorApiService
  ) {}
  canActivateChild(route: ActivatedRouteSnapshot): boolean {
      debugger;
        const expectedPermissions = route.data.expectedPermissions;
        this.isAutherized = this.authorApiService.checkIsAutherized(expectedPermissions);
      // Todo: true should replace with this.isAutherized, once user permissions are set in DB
         return true;
        //return this.isAutherized;
  }
}
