
import { ApiService } from './api.service';
import { SharedService } from './shared.service';
import { environment } from './../../environments/environment';
import { Injectable } from '@angular/core';
import { Http, Headers, Response, RequestOptions } from '@angular/http';
import { Observable } from "rxjs/Rx";
import 'rxjs/add/operator/map'

import { Emails } from './../Models/Emails';


// import { HttpClient } from '@angular/common/http';
// import { HttpHeaders } from '@angular/common/http';
// import { HttpParams } from '@angular/common/http';

import 'rxjs/add/operator/map';
import { HttpHeaders } from '@angular/common/http';
import { BehaviorSubject } from 'rxjs/BehaviorSubject';


@Injectable()
export class LinkedinService {
  static nextEmailListId = 100;
  private _webApiUrl = environment.apiEndPoint;
  //private crises$: BehaviorSubject<Emaill[]> = new BehaviorSubject<Emaill[]>(EMAILS);
  constructor(private http: Http, private service: SharedService,private apiService: ApiService) { }

  // getEmails(): Promise<Emails[]> {
  //   let headers = new Headers({ 'Content-Type': 'application/json' });
  //   let options = new RequestOptions({ headers: headers });
  //   return this.http.post(this._webApiUrl + "/widgets/chartfilterdata", pagewidget, options).toPromise()
  //     .then(this.extractData)
  //     .catch(this.handleErrorPromise);
  // }

  GetEmailList(tenantId:number,userId: number,listId:number) {
    let getReqHeaders = new HttpHeaders();
    return this.apiService.getResults(`${this._webApiUrl}GetEmailList/${tenantId}/${userId}/${listId}`, getReqHeaders); 
    //return this.apiService.getResults(this._webApiUrl+'GetEmails', getReqHeaders); 
  } 

  getEmailById(id:number) {
    let getReqHeaders = new HttpHeaders();
    return this.apiService.getResults(this._webApiUrl+'GetEmailById/'+id, getReqHeaders); 
  } 

  GetFromLinkedin(noofconnections:number) {
    let getReqHeaders = new HttpHeaders();
    return this.apiService.getResults(this._webApiUrl+'GetFromLinkedin/'+noofconnections, getReqHeaders); 
  } 
  UpdateEmail(vendorObj:any): Observable<any> {
    //
    let reqHeaders = new HttpHeaders();
    //return  this.apiService.putData(`${this._webApiUrl}UpdateEmail`, vendorObj, reqHeaders);  
    return  this.apiService.postData(`${this._webApiUrl}UpdateEmail`, vendorObj, reqHeaders);  

  }  

  SendMessage(profileId:any)
  {
    let reqHeaders = new HttpHeaders();
    return  this.apiService.postData(`${this._webApiUrl}SendMessage/${profileId}`, profileId, reqHeaders);  
    
  }

  FindEmail(Id:number | string,tenantId:number,userId: number)
  {
    let getReqHeaders = new HttpHeaders();
    return this.apiService.getResults(`${this._webApiUrl}findemailbyid/${Id}/${tenantId}/${userId}`, getReqHeaders); 
  }
  


  // UpdateEmail(email:Emails)
  // {
  //   let getReqHeaders=new HttpHeaders();
  //   return this.apiService.putData(this._webApiUrl+'UpdateEmail',email,getReqHeaders);
  // }
  
  private extractData(res: Response) {
    let body = <any[]>res.json();
    return body || [];
  }

  private extractString(res: Response) {
    let body = res.json();
    return body || [];
  }

  private extractPageData(res: Response) {
    let body = <Emails>res.json();
    return body;
  }

  private extractUpdateData(res: Response) {
    let body = res.json();
    return body || [];
  }

  private handleError(error: any) {
    let errMsg = (error.message) ? error.message :
      error.status ? `${error.status} - ${error.statusText}` : 'Server error';
    console.error(errMsg); // log to console instead
    return Observable.throw(errMsg);
  }
  GetEmailList1(tenantId:number,userId: number): Promise<any> {
    return this.http.get(`${this._webApiUrl}GetEmailList/${tenantId}/${userId}`)
      .toPromise()
      .then(this.extractData)
      .catch(this.handleError);
  }


}
