import { Injectable } from '@angular/core';

import { HttpHeaders, HttpClient } from '@angular/common/http';
import { HttpParams } from '@angular/common/http';
import {Observable } from 'rxjs/Observable';
import 'rxjs/add/operator/map';
import { AuthApiService } from './auth-api.service';
import { SessionStorageService } from './sessionstorage.service.';
@Injectable()
export class ApiService {

  constructor(
    private httpClient: HttpClient,
    private sessionService: SessionStorageService
  ) { }
  results: string[];
  // This is a generic function that makes the get calls and adds
  // any necessary intermitters.
  // IMPORTANT: append returns a cloned header object. Original header is immutable
  //  Scure APIs which includes PatasalaAuthToken in header, use only for secure routes
  getResults(url: string, reqHeaders: HttpHeaders, reqParams: HttpParams = null) {
    
   const token = this.sessionService.getToken();
   //reqHeaders = reqHeaders.append('PatasalaAuthToken', `${token}`);
   const results =   this.httpClient.get(url, {headers: reqHeaders, params: reqParams});
   return results;
  }

  getPDFResults(url: string, reqHeaders: HttpHeaders, reqParams: HttpParams = null) {
    // const token = this.localstorageService.getToken();
    const token = this.sessionService.getToken();
    reqHeaders = reqHeaders.append('PatasalaAuthToken', `${token}`);
    const results =   this.httpClient.get(url, {headers: reqHeaders, params: reqParams});
    return results;
   }

  postData(url: string, body, reqHeaders: HttpHeaders) {
    const token = this.sessionService.getToken();
    reqHeaders = reqHeaders.append('PatasalaAuthToken', `${token}`);
    reqHeaders = reqHeaders.append('Content-Type', 'application/json');
    return this.httpClient.post(url, body, {headers: reqHeaders});
  }

  putData(url: string, body, reqHeaders: HttpHeaders) {
    const token = this.sessionService.getToken();
    reqHeaders = reqHeaders.append('PatasalaAuthToken', `${token}`);
    reqHeaders = reqHeaders.append('Content-Type', 'application/json');
    return this.httpClient.put(url, body, {headers: reqHeaders});
  }

  deleteData(url: string, reqHeaders: HttpHeaders) {
    const token = this.sessionService.getToken();
    reqHeaders = reqHeaders.append('PatasalaAuthToken', `${token}`);
    return this.httpClient.delete(url, {headers: reqHeaders});
  }

  deleteDataWithBody(url: string, body, reqHeaders: HttpHeaders) {
    const token = this.sessionService.getToken();
    reqHeaders = reqHeaders.append('PatasalaAuthToken', `${token}`);
    return this.httpClient.request('DELETE', url, { body, headers: reqHeaders });
  }

  postMedia(url: string, body, reqHeaders: HttpHeaders) {
    const token = this.sessionService.getToken();
    reqHeaders = reqHeaders.append('PatasalaAuthToken', `${token}`);
    return this.httpClient.post(url, body, {headers: reqHeaders});
  }

  postWithResponseType(url: string, body, reqHeaders: HttpHeaders, respType: any) {
    const token = this.sessionService.getToken();
    reqHeaders = reqHeaders.append('PatasalaAuthToken', `${token}`);
    return this.httpClient.post(url, body, {headers: reqHeaders, responseType: respType});
  }

// All public APIs will goes here, these can use for public outes
  getPublicResults(url: string, reqHeaders: HttpHeaders, reqParams: HttpParams = null) {
    const results =   this.httpClient.get(url, {headers: reqHeaders, params: reqParams});
    return results;
   }

   postPublicData(url: string, body, reqHeaders: HttpHeaders) {
     reqHeaders = reqHeaders.append('Content-Type', 'application/json');
     return this.httpClient.post(url, body, {headers: reqHeaders});
   }

   putPublicData(url: string, body, reqHeaders: HttpHeaders) {
     reqHeaders = reqHeaders.append('Content-Type', 'application/json');
     return this.httpClient.put(url, body, {headers: reqHeaders});
   }

   deletePublicData(url: string, reqHeaders: HttpHeaders) {
     return this.httpClient.delete(url, {headers: reqHeaders});
   }

}



//Old Code
// import { Injectable } from '@angular/core';
// import { HttpClient } from '@angular/common/http';
// import { HttpHeaders } from '@angular/common/http';
// import { HttpParams } from '@angular/common/http';
// import {Observable } from 'rxjs/Observable';
// import 'rxjs/add/operator/map';
// //import { AuthApiService } from './auth-api.service';
// //import { SessionStorageService } from './sessionstorage.service.';
// import {SessionStorageService} from './../Shared/sessionstorage.service'
// import { Emails } from '../Models/Emails';
// import { RequestOptions } from '@angular/http';
// @Injectable()
// export class ApiService {

//   constructor(
//     private httpClient: HttpClient,
//     private sessionService: SessionStorageService
//   ) { }

//   // This is a generic function that makes the get calls and adds
//   // any necessary intermitters.
//   // IMPORTANT: append returns a cloned header object. Original header is immutable
//   //  Scure APIs which includes PatasalaAuthToken in header, use only for secure routes



//   getResults(url: string, reqHeaders: HttpHeaders, reqParams: HttpParams = null) {
//    const token = this.sessionService.getToken();
//    //reqHeaders = reqHeaders.append('PatasalaAuthToken', `${token}`);
//    const results =   this.httpClient.get(url, {headers: reqHeaders, params: reqParams});
//    return results;
//   }

//   getResult(url: string, reqHeaders: HttpHeaders, reqParams: HttpParams = null) {
//     const token = this.sessionService.getToken();
//     //reqHeaders = reqHeaders.append('PatasalaAuthToken', `${token}`);
//     const result= this.httpClient.get(url, {headers: reqHeaders, params: reqParams});
//     return result;
//    }

//   getPDFResults(url: string, reqHeaders: HttpHeaders, reqParams: HttpParams = null) {
//     // const token = this.localstorageService.getToken();
//     const token = this.sessionService.getToken();
//     reqHeaders = reqHeaders.append('PatasalaAuthToken', `${token}`);
//     const results =   this.httpClient.get(url, {headers: reqHeaders, params: reqParams});
//     return results;
//    }

//   postData(url: string, body, reqHeaders: HttpHeaders) {
//     const token = this.sessionService.getToken();
//     //reqHeaders = reqHeaders.append('BoundHoundAuthToken', `${token}`);
//     reqHeaders = reqHeaders.append('BoundHoundAuthToken', 'mdaslam');
//     reqHeaders = reqHeaders.append('Content-Type', 'application/json');
//     return this.httpClient.post(url, body, {headers: reqHeaders});
//   }

//   putData(url: string, body, reqHeaders: HttpHeaders) {
//     //let reqHeaders = new HttpHeaders();
//     //reqHeaders = reqHeaders.append('PatasalaAuthToken', `${token}`);
//     reqHeaders = reqHeaders.append('Content-Type', 'application/json');
//     return this.httpClient.put(url, body, {headers: reqHeaders});
//   }

//   deleteData(url: string, reqHeaders: HttpHeaders) {
//     const token = this.sessionService.getToken();
//     reqHeaders = reqHeaders.append('PatasalaAuthToken', `${token}`);
//     return this.httpClient.delete(url, {headers: reqHeaders});
//   }

//   deleteDataWithBody(url: string, body, reqHeaders: HttpHeaders) {
//     const token = this.sessionService.getToken();
//     reqHeaders = reqHeaders.append('PatasalaAuthToken', `${token}`);
//     return this.httpClient.request('DELETE', url, { body, headers: reqHeaders });
//   }

//   postMedia(url: string, body, reqHeaders: HttpHeaders) {
//     const token = this.sessionService.getToken();
//     reqHeaders = reqHeaders.append('PatasalaAuthToken', `${token}`);
//     return this.httpClient.post(url, body, {headers: reqHeaders});
//   }

//   postWithResponseType(url: string, body, reqHeaders: HttpHeaders, respType: any) {
//     const token = this.sessionService.getToken();
//     reqHeaders = reqHeaders.append('PatasalaAuthToken', `${token}`);
//     return this.httpClient.post(url, body, {headers: reqHeaders, responseType: respType});
//   }

// // All public APIs will goes here, these can use for public outes
//   getPublicResults(url: string, reqHeaders: HttpHeaders, reqParams: HttpParams = null) {
//     const results =   this.httpClient.get(url, {headers: reqHeaders, params: reqParams});
//     return results;
//    }

//    postPublicData(url: string, body, reqHeaders: HttpHeaders) {
//      reqHeaders = reqHeaders.append('Content-Type', 'application/json');
//      return this.httpClient.post(url, body, {headers: reqHeaders});
//    }

//    putPublicData(url: string, body, reqHeaders: HttpHeaders) {
//      reqHeaders = reqHeaders.append('Content-Type', 'application/json');
//      return this.httpClient.put(url, body, {headers: reqHeaders});
//    }

//    deletePublicData(url: string, reqHeaders: HttpHeaders) {
//      return this.httpClient.delete(url, {headers: reqHeaders});
//    }

// }
