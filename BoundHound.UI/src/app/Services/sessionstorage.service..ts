import { Injectable } from '@angular/core';
import { Router } from '@angular/router';

@Injectable()
export class SessionStorageService {

  constructor(
    private router: Router,
  ) { }
// Store Token in session storage
getToken() {
  if (sessionStorage.getItem('token')) {
    return sessionStorage.getItem('token');
  } else {
    return false;
  }
}

setToken(token) {
  sessionStorage.setItem('token', token);
}

removeToken() {
  sessionStorage.removeItem('token');
}

refreshToken(token) {
  if (sessionStorage.getItem('token')) {
    sessionStorage.removeItem('token');
  }
  sessionStorage.setItem('token', token);
}

// Store User in session storage
setUser(user) {
  sessionStorage.setItem('user', JSON.stringify(user));
}

getUser() {
  if (sessionStorage.getItem('user')) {
    return JSON.parse(sessionStorage.getItem('user'));
  } else {
    return false;
  }
}
removeUser() {
  sessionStorage.removeItem('user');
}

refreshUser(branchId) {
  const user = this.getUser();
  user.BranchId = branchId;
  this.removeUser();
  this.setUser( user);
}

//  Store User roles in session storage
setUserRole(user) {
  sessionStorage.setItem('UserRole', JSON.stringify(user));
}

getUserRole() {
  if (sessionStorage.getItem('UserRole')) {
    return JSON.parse(sessionStorage.getItem('UserRole'));
  } else {
    return false;
  }
}

removeUserRole() {
  sessionStorage.removeItem('school');
}

//  Store school in session storage
setSchool(school) {
  sessionStorage.setItem('school', JSON.stringify(school));
}

getSchool() {
  if (sessionStorage.getItem('school')) {
    return JSON.parse(sessionStorage.getItem('school'));
  } else {
    return false;
  }
}

removeSchool() {
  sessionStorage.removeItem('school');
}

}
