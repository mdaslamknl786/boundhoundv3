import { Injectable } from '@angular/core';
import { Subject } from 'rxjs/Subject';
import { Observable } from 'rxjs/Observable';

@Injectable()
export class AuthEventService {
  public getLoggedInName = new Subject();
  fullName = 'Mohammed Aslam';
  currentDate: string;
  startDate: any;
  constructor() { }
  login(email: string, password: string) {
    //debugger;
    this.startDate = new Date();
    this.currentDate = Date.UTC.toString();
    if (this.successfulLogIn(email, password)) {
      this.getLoggedInName.next(this.fullName + '  ' + this.startDate);
      // next() method is alternate to emit().
      return 'YES';
    } else {
      this.getLoggedInName.next('Sign In');
      return 'NO';
    }
  }
  logout(): void {
    this.getLoggedInName.next('Sign In');
  }
  successfulLogIn(email: string, password: string) {
    return true;
  }
}
