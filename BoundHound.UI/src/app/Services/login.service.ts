import { SocialUser } from 'ng4-social-login';
import { Injectable } from '@angular/core';
import {Subject} from 'rxjs';
@Injectable()
export class LoginService {
  private isUserLoggedIn: boolean = false;
  public IsUserLoggedIn: Subject<boolean> = new Subject<boolean>();

  public UserDetails:Subject<SocialUser>= new Subject<SocialUser>();
  
  private UserInfo:SocialUser;
  public setLoggedInUser(flag) { // you need set header flag true false from other components on basis of your requirements, header component will be visible as per this flag then
    this.isUserLoggedIn = flag;
  }


  public getUserLoggedIn(): boolean {
    return this.isUserLoggedIn;
  }
  public SetUserInfo(userinfo:SocialUser){
    this.UserInfo=userinfo;
  }

  public GetUserInfo():SocialUser{
    return this.UserInfo;
  }
  constructor() { }

}
