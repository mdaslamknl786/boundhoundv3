import { Injectable } from '@angular/core';
import { Router } from '@angular/router';
import { SessionStorageService } from './sessionstorage.service.';
import { AuthApiService } from './auth-api.service';
import { HttpHeaders } from '@angular/common/http';
import { ApiService } from './api.service';
import { environment } from './../../environments/environment';

@Injectable()
export class AuthorApiService {
  userRoles: any = this.sessionService.getUserRole();
  isAutherized: boolean;
  private userRoleEndpoint = `${environment.apiEndPoint}/userPermissions`;
  constructor(
    private router: Router,
    private sessionService: SessionStorageService,
    private authApiService: AuthApiService,
    private apiService: ApiService
  ) { }

  getUserRole(): any {
    const reqHeaders = new HttpHeaders();
    return this.apiService.getResults(this.userRoleEndpoint, reqHeaders);
  }


  checkIsAutherized( expectedPermissions ): boolean {
    this.isAutherized = false;
    if (!this.userRoles || this.userRoles == null ) {
      this.getUserRole().subscribe(response => {
        this.sessionService.setUserRole(response);
        this.userRoles = this.sessionService.getUserRole();
      });
    }
    if (this.userRoles && expectedPermissions) {
      this.userRoles.forEach(element => {
        if ( Number(element.Permission) === Number(expectedPermissions.Permission)
          && Number(element.AccessLevel) >= Number(expectedPermissions.AccessLevel) ) {
          this.isAutherized = true;
        }
      });
    }
    // Todo: true should replace with this.isAutherized, once user permissions are set in DB
    return true;
    // return this.isAutherized;
  }
}
