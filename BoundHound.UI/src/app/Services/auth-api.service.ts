import { Injectable } from '@angular/core';
import { HttpHeaders } from '@angular/common/http';
// import { environment } from '../../../environments/environment';
 import { ApiService } from './api.service';
import { Router } from '@angular/router';
import { Observable } from 'rxjs/Observable';
import { SessionStorageService } from './sessionstorage.service.';

import { environment } from './../../environments/environment';
//import { ApiService } from './api.service';




@Injectable()
export class AuthApiService {
  private signinEndpoint = `${environment.apiEndPoint}/authentication/signIn`;
  constructor(
    private router: Router,
    private apiService:ApiService,
    private sessionService: SessionStorageService
  ) { }

  signin(signinForm) {
    const reqHeaders = new HttpHeaders();
    return this.apiService.postPublicData(this.signinEndpoint, signinForm, reqHeaders);
  }
  forgotPassword(forgotPasswordForm) {
    const reqHeaders = new HttpHeaders();
    const forgotPasswordEndpoint = `${environment.apiEndPoint}/authentication/forgotpassword?username=${forgotPasswordForm.Email}`;
    return this.apiService.postPublicData(forgotPasswordEndpoint, forgotPasswordForm, reqHeaders);
  }

  signout() {
    this.sessionService.removeToken();
    this.sessionService.removeUser();
    this.router.navigate(['/home/signin']);
  }
  changePassword(setPasswordForm) {
    const reqHeaders = new HttpHeaders();
    const setPasswordEndpoint = `${environment.apiEndPoint}/users/changepassword`;
    return this.apiService.postData(setPasswordEndpoint, setPasswordForm, reqHeaders);
  }
  changeBranchContext(branchId) {
    const reqHeaders = new HttpHeaders();
    const endpoint = `${environment.apiEndPoint}/branches/${branchId}/changeContext`;
    return this.apiService.postData(endpoint, null, reqHeaders);
  }

  isLoggedIn() {
    if (this.sessionService.getToken() && this.sessionService.getUser()) {
      return true;
    } else {
      return false;
    }
  }
}
