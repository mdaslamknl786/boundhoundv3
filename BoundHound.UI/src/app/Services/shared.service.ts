import { Injectable } from '@angular/core';
//import { pagewidget } from '../model/pagewidget';
@Injectable()
export class SharedService {

  IsLoggedIn: boolean;
  LoggedinUser: string;
  UserInfo:any;  
  pageID: number;
  constructor() {   
  }

  setData(logged:boolean ) {
    this.IsLoggedIn =logged;  
  }

   setloggedinuser(user:string ) {
    this.LoggedinUser = user;  
  }

  SetUserDetails(data:any)
  {
    this.UserInfo=data;
  }

  GetUserDetails()
  {
    return this.UserInfo;
  }

  SetDefaultPage(id: number)
  {
    this.pageID=id;
  }

  GetDefaultPage()
  {
     return this.pageID;
  }
}