﻿using BoundHound.Model;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace BoundHound.DbService.Interfaces
{
    public interface IEmailListDbService
    {
        int UpdateEmail(EmailDTO email);
        IEnumerable<EmailDTO> GetEmailList(UserInfo MyUserInfo,int listId);
        IEnumerable<EmailDTO> GetConnectionByProfileId(string ProfileId);
        IEnumerable<ListDTO> GetEmailListByListId(UserInfo MyUserInfo, int listId);
        IEnumerable<EmailDTO> GetEmailInfoById(string Id);
        int UpdateConnectionEmail(int Id, string email);
        IEnumerable<EmailDTO> GetEmailprofileInfoById(string id);
    }
}
