﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace BoundHound.DbService.Interfaces
{
    public enum AccessType
    {
        Read,
        Write
    }
}
