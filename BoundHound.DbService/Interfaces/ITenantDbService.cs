﻿using BoundHound.Model;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace BoundHound.DbService.Interfaces
{
    public interface ITenantDbService
    {
        TenantDTO GetTenantInfo(int TenantId);
    }
}
