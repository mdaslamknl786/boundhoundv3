﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace BoundHound.DbService.Interfaces
{
    public interface IDbProfilingBlock : IDisposable
    {
        IDbProfilingBlock Initialize(string sql, IDbConnection conn);
    }
}
