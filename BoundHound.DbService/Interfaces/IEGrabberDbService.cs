﻿using BoundHound.DbService.DTOs;
using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Data;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace BoundHound.DbService.Interfaces
{

    public interface IEGrabberDbService
    {
        void SetShouldCheckQueryLevel(bool b);
        void SetShouldTrackQueries(bool b);
        void SetDetectLiteralsInQueries(bool b);
        void SetShouldCompareParametersWithSql(bool b);
        void SetShouldDetectDateFunctionsInQueries(bool b);
        void CheckQueryLevel();

        DataTable ExecuteDataTable(IDbConnection conn, string sql, IDictionary<string, object> whereConditions, int? timeout = null, bool skipQueryLevelCheck = false);
        DbResultWithCount<T> GetResultSetWithFullCount<T>(IDbConnection conn, string sql, IDictionary<string, object> parameters = null, int? timeout = null);
        IEnumerable<T> Query<T>(IDbConnection conn, string sql, IDictionary<string, object> parameters = null, int? timeout = null);

        IEnumerable<T> Query<T>(int schoolId, string sql, IDictionary<string, object> whereConditions = null, int? timeout = null);

        IEnumerable<TReturn> Query<TFirst, TSecond, TReturn>(int schoolId, string sql, Func<TFirst, TSecond, TReturn> map,
            IDictionary<string, object> parameters = null, int? timeout = null);

        IEnumerable<TReturn> Query<TFirst, TSecond, TReturn>(int schoolId, string sql,
            Func<TFirst, TSecond, TReturn> map, string splitOn, IDictionary<string, object> parameters = null,
            int? timeout = null);

        IEnumerable<TReturn> Query<TFirst, TSecond, TReturn>(IDbConnection conn, string sql, Func<TFirst, TSecond, TReturn> map,
            IDictionary<string, object> parameters = null, int? timeout = null);

        IEnumerable<TReturn> Query<TFirst, TSecond, TThird, TReturn>(IDbConnection conn, string sql,
            Func<TFirst, TSecond, TThird, TReturn> map, IDictionary<string, object> parameters, int? timeout = null);

        IEnumerable<TReturn> Query<TFirst, TSecond, TThird, TReturn>(int schoolId, string sql,
            Func<TFirst, TSecond, TThird, TReturn> map, IDictionary<string, object> parameters, int? timeout = null);

        IEnumerable<TReturn> Query<TFirst, TSecond, TThird, TFourth, TReturn>(IDbConnection conn, string sql,
            Func<TFirst, TSecond, TThird, TFourth, TReturn> map, IDictionary<string, object> parameters, int? timeout = null);
        DataRow ExecuteDataRowWithoutValidation(IDbConnection conn, string sql, IDictionary<string, object> whereConditions = null);

        T ExecuteInsertStatement<T>(IDbConnection conn, IDictionary<string, object> fields, string tableName, bool ignoreDuplicates = false);

        int ExecuteUpdateStatement(IDbConnection conn, IDictionary<string, object> fields, IDictionary<string, object> whereConditions, string tableName, int? expectedCount = null);

        int ExecuteDeleteStatement(IDbConnection conn, IDictionary<string, object> whereConditions, string tableName, int? expectedCount = null);

        int ExecuteNonQuery(IDbConnection conn, string sql, IDictionary<string, object> parameters, int? timeout = null);

        object ExecuteScalar(IDbConnection conn, string sql, IDictionary<string, object> parameters = null);
        T ExecuteScalar<T>(IDbConnection conn, string sql, IDictionary<string, object> parameters = null);

        IDbConnection PrepareConnection(string connString);
        void CloseAndDisposeConnection(IDbConnection conn);

        IDbTransaction BeginTransaction(IDbConnection conn);
        void CommitTransaction(IDbTransaction transaction);
        void RollbackTransaction(IDbTransaction transaction);
        void DisposeTransaction(IDbTransaction transaction);

        void ProcessZeroRowsAffectedException(IDictionary<string, object> whereConditions, string tableName);

        ReadOnlyCollection<QueryInfo> Queries { get; }
        void ResetTrackQueries();

        IDbConnection GetMasterDbWriteConnection();
        IDbConnection GetMasterDbReadConnection();

        IDbConnection GetReadConnection(int schoolId);
        IDbConnection GetWriteConnection(int schoolId);
        IDbConnection GetSchoolReadConnection();
    }
}
