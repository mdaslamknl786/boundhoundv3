﻿using BoundHound.Model;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace BoundHound.DbService.Interfaces
{
    public interface ILinkedInDbService
    {
        IEnumerable<EmailDTO> GetConnections(int noOfConnections, UserInfo MyUserInfo);
        IEnumerable<EmailDTO> GetConnectionEmailByProfileId(string profileid, UserInfo MyUserInfo);
        IEnumerable<EmailDTO> GetConnectionEmailByProfileList(IEnumerable<EmailDTO> EmailsList, UserInfo MyUserInfo);
        IEnumerable<EmailDTO> GetFromLinkedin(int noOfConnections, UserInfo MyUserInfo);

        IEnumerable<EmailDTO> GetFromLinkedInWithEmails(int noOfConnections, UserInfo MyUserInfo);

        bool SendMessage(string ProfileURL, string Message, UserInfo MyUserInfo);
        IEnumerable<EmailDTO> SendInvitations(string SearchStr, string ConnectionType, UserInfo MyUserInfo);

        IEnumerable<SendInvitationDTO> ClickSendInvitaion(List<String> ProfileURl, string[] connectionid, string Message, UserInfo MyUserInfo);
        IEnumerable<SendInvitationDTO> FindStatusofInvitationById(List<String> ProfileURl, string[] connectionid, string Message, UserInfo MyUserInfo);
    }
}
