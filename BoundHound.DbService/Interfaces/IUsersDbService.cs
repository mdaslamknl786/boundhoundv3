﻿using BoundHound.Framework;
using BoundHound.Model;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace BoundHound.DbService.Interfaces
{
    public interface IUsersDbService
    {
        UserDTO IsUserCredentialsValid(string userName);
        bool IsUserExist(string userName);
        bool ResetPassword(BoundHoundContext context, int id);
        bool ChangePassword(BoundHoundContext context, LoginDTO dto);
        UserDTO GetUserInfo(BoundHoundContext context, int id);
        UserInfo Authenticate(string UserName, string Password);
        UserInfo TokenValidation(string Email, int UserId, int TenantId);
    }
}
