﻿using BoundHound.Model;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace BoundHound.DbService.Interfaces
{
    public interface IListDbService
    {
        IEnumerable<ListDTO> GetList(UserInfo MyUserInfo);
        ListDTO GetListInfo(int Id, UserInfo MyUserInfo);
        int CreateList(ListDTO listDTO, UserInfo MyUserInfo);
        ListDTO UpdateList(ListDTO listDTO, UserInfo MyUserInfo);
        int DeleteList(int Id, UserInfo MyUserInfo);
        int MoveToList(string EmailIds, int listId, UserInfo MyUserInfo);
    }

}
