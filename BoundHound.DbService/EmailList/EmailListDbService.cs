﻿using BoundHound.DbService.Interfaces;
using BoundHound.Linkedin;
using BoundHound.Model;
using Dapper;
using MySql.Data.MySqlClient;
using System;
using System.Collections.Generic;
using System.Configuration;
using System.Data;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
namespace BoundHound.DbService.EmailList
{
    public class EmailListDbService : IEmailListDbService
    {
        private readonly IDbConnection _db;
        string connection = ConfigurationManager.ConnectionStrings["LinkedInConnection"].ConnectionString;
        public EmailListDbService()
        {
            _db = new MySqlConnection(ConfigurationManager.ConnectionStrings["LinkedInConnection"].ConnectionString);
        }

        public IEnumerable<EmailDTO> GetConnectionByProfileId(string ProfileId)
        {
            //const string sql =
            //      @"SELECT * FROM emaillist
            // WHERE tenantId = @tenantId AND userId = @userId";
            IEnumerable<EmailDTO> EmailList;

            const string sql =
                 @"select em.Id  emaillistId,em.firstname,em.lastname,em.email,em.phone,
                    em.mobile,em.title,em.company,em.website,em.location,em.Url,em.photourl,
                    em.profileid,em.lastupdate,em.tenantId,em.userId,em.listId from emaillist em
                    where em.Id in (@profileId)";

            EmailList = this._db.Query<EmailDTO>(sql, new Dictionary<string, object> { { "profileId", ProfileId } }).ToList();

            return EmailList;
        }

        public IEnumerable<EmailDTO> GetEmailInfoById(string Id)
        {
            //const string sql =
            //      @"SELECT * FROM emaillist
            // WHERE tenantId = @tenantId AND userId = @userId";
            IEnumerable<EmailDTO> EmailInfo = new List<EmailDTO>();

            const string sql =
                 @"select em.Id  emaillistId,em.firstname,em.lastname,em.email,em.phone,
                    em.mobile,em.title,em.company,em.website,em.location,em.Url,em.photourl,
                    em.profileid,em.lastupdate,em.tenantId,em.userId,em.listId from emaillist em
                    where em.Id=(@Id)";

            EmailInfo = this._db.Query<EmailDTO>(sql, new Dictionary<string, object> { { "Id", Id } }).ToList();

            return EmailInfo;
        }

        public IEnumerable<EmailDTO> GetEmailList(UserInfo MyUserInfo,int listId)
        {
            //const string sql =
            //      @"SELECT * FROM emaillist
            // WHERE tenantId = @tenantId AND userId = @userId";
            List<EmailDTO> EmailList = new List<EmailDTO>();

            const string sql =
                 @"select lst.Id,lst.listName,lst.listDesc,em.Id  emaillistId,em.firstname,em.lastname,em.email,em.phone,
                    em.mobile,em.title,em.company,em.website,em.location,em.Url,em.photourl,
                    em.profileid,em.lastupdate,em.tenantId,em.userId,em.listId from list lst
                    inner join emaillist em on em.listId=lst.Id
                    And em.tenantId=@tenantId and em.userId=@userId and em.listId=@listId;";


            const string sql1 =
                 @"select lst.Id,lst.listName,lst.listDesc,em.Id  emaillistId,em.firstname,em.lastname,em.email,em.phone,
                    em.mobile,em.title,em.company,em.website,em.location,em.Url,em.photourl,
                    em.profileid,em.lastupdate,em.tenantId,em.userId,em.listId from list lst
                    inner join emaillist em on em.listId<>lst.Id
                    And em.tenantId=@tenantId and em.userId=@userId and em.listId=@listId;";
            //WHERE tenantId = @tenantId AND userId = @userId";

            if (listId > 0)
            {
                EmailList = this._db.Query<EmailDTO>(sql, new Dictionary<string, object> { { "tenantId", MyUserInfo.TenantId }, { "userId", MyUserInfo.UserId }, { "listId", listId } }).ToList();
            }
            else
            {
                EmailList = this._db.Query<EmailDTO>(sql1, new Dictionary<string, object> { { "tenantId", MyUserInfo.TenantId }, { "userId", MyUserInfo.UserId }, { "listId", listId } }).ToList();
            }
            return EmailList;

        }

        public IEnumerable<ListDTO> GetEmailListByListId(UserInfo MyUserInfo, int listId)
        {
            //const string sql =
            //      @"SELECT * FROM emaillist
            // WHERE tenantId = @tenantId AND userId = @userId";


            const string sql =
                 @"select lst.Id,lst.listName,lst.listDesc,em.Id ,em.firstname,em.lastname,em.email,em.phone,
                    em.mobile,em.title,em.company,em.website,em.location,em.Url,em.photourl,
                    em.profileid,em.lastupdate,em.tenantId,em.userId,em.listId from list lst
                    inner join emaillist em on em.listId=lst.Id
                    And em.tenantId=@tenantId and em.userId=@userId and em.listId=@listId;";


            var parameters = new Dictionary<string, object>
            {
                {"tenantId", MyUserInfo.TenantId},
                {"userId", MyUserInfo.UserId},
                {"listId", listId},
                
            };

            //WHERE tenantId = @tenantId AND userId = @userId";
            var vendorDictionary = new Dictionary<int, ListDTO>();

            var listwithemails = this._db.Query<ListDTO, EmailDTO, ListDTO>(sql,
                    (list, emaillist) =>
                    {
                        if (!vendorDictionary.TryGetValue(list.ListId, out ListDTO vv))
                        {
                            vv = list;
                            vv.Emails = new List<EmailDTO>();
                            vendorDictionary.Add(vv.ListId, vv);
                        }

                        if (emaillist != null)
                        {
                            if (!vv.Emails.Contains(emaillist))
                            {
                                vv.Emails.Add(emaillist);
                            }
                        }

                       
                        return vv;
                    }, parameters).Distinct().ToList();


            return listwithemails;
            //return this._db.Query<EmailListDTO>(sql, new Dictionary<string, object> { { "tenantId", MyUserInfo.TenantId }, { "userId", MyUserInfo.UserId }, { "listId", listId } });

        }

        public int UpdateEmail(EmailDTO email)
        {
            int rowsAffected = this._db.Execute(
                        "UPDATE emaillist SET firstname = @firstname ,lastname = @lastname , email = @email ,phone=@phone,website=@website, title = @title  , location = @location  , company = @company  , website = @website WHERE Id = " +
                        email.emaillistId, email);

            string UpdateEmailQuery = @"UPDATE emaillist SET firstname = @firstname, lastname = @lastname, email = @email, phone = @phone,
                                    website = @website, title = @title, location = @location, company = @company WHERE Id =@Id ";

            rowsAffected = this._db.Execute(UpdateEmailQuery, new
            {
                Id=email.emaillistId,
                firstname=email.firstname,
                lastname=email.lastname,
                email=email.email,
                phone=email.phone,
                website=email.website,
                title=email.title,
                location=email.location,
                company=email.company
            });
            if(rowsAffected>0)
            {
                rowsAffected = email.emaillistId;
            }
            return rowsAffected;
        }

        public int UpdateConnectionEmail(int Id,string email)
        {
            //int rowsAffected = this._db.Execute("UPDATE [emaillist] SET [email] = @email WHERE Id = " + Id, new Dictionary<string, object> { { "email", email } });

            //int rowsAffected = this._db.Execute("UPDATE [emaillist] SET [email] = @email WHERE Id = @Id", new Dictionary<string, object> { { "Id", Id }, { "email",email} });

            //return rowsAffected;

            string updateQuery = @"UPDATE emaillist SET email = @email WHERE Id = @Id";

            int result =this._db.Execute(updateQuery, new
            {
                email,
                Id
                
            });
            return result;
        }


        public IEnumerable<EmailDTO> GetEmailprofileInfoById(string id)
        {
            string[] Totalid = id.Split(new string[] { "," }, StringSplitOptions.None);
            List<EmailDTO> EmailInfo = new List<EmailDTO>();

            const string sql =
                 @"select em.Id  emaillistId,em.firstname,em.lastname,em.email,em.phone,
                    em.mobile,em.title,em.company,em.website,em.location,em.Url,em.photourl,
                    em.profileid,em.lastupdate,em.tenantId,em.userId,em.listId from emaillist em
                    where em.Id in (@Id)";
            //var invoices = this._db.Query<EmailDTO>(sql, new { Kind = new[] { InvoiceKind.StoreInvoice, InvoiceKind.WebInvoice } }).ToList();
            //EmailInfo = this._db.Query<EmailDTO>(sql, new  { "Id", (Totalid)  }).ToList();

            //EmailInfo = this._db.Query<EmailDTO>(sql, new  { Id=new[] { Id } } ).ToList();
            for (int i = 0; i < Totalid.Length; i++)
            {
                var profileid = this._db.Query<EmailDTO>(sql, new Dictionary<string, object> { { "Id", (Totalid[i]) } }).ToList();
                EmailInfo.Add(profileid[0]);
            }

            return EmailInfo;
        }



    }
}
