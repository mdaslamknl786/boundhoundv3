﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace BoundHound.DbService
{
    public class QueryInfo
    {
        public string SqlQuery { get; set; }
        public long ElapsedMilliseconds { get; set; }

        public QueryInfo(string sqlQuery, long elapsedMilliseconds)
        {
            SqlQuery = sqlQuery;
            ElapsedMilliseconds = elapsedMilliseconds;
        }
    }
}
