﻿using BoundHound.DbService.Interfaces;
using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace BoundHound.DbService
{
    public class PassThroughDbProfilingBlock : IDbProfilingBlock
    {
        public IDbProfilingBlock Initialize(string sql, IDbConnection conn)
        {
            return this;
        }

        public void Dispose()
        {
        }
    }
}
