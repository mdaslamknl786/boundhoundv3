﻿using BoundHound.DbService.Interfaces;
using BoundHound.Linkedin;
using BoundHound.Model;
using Dapper;
using MySql.Data.MySqlClient;
using System;
using System.Collections.Generic;
using System.Configuration;
using System.Data;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace BoundHound.DbService.List
{
    public class ListDbService : IListDbService
    {
        private readonly IDbConnection _db;
        string connection = ConfigurationManager.ConnectionStrings["LinkedInConnection"].ConnectionString;
        public ListDbService()
        {
            _db = new MySqlConnection(ConfigurationManager.ConnectionStrings["LinkedInConnection"].ConnectionString);
        }

        public IEnumerable<ListDTO> GetList(UserInfo MyUserInfo)
        {
           const string sql =
                 @"Select lst.Id ListId,lst.listName,lst.listDesc,lst.tenantId TenantId,lst.userId UserId,lst.lastUpdate,lst.isActive,tn.tenantName from list lst
                inner join tenants tn on tn.Id=lst.tenantId
                Where tn.Id=@tenantId;";

            return this._db.Query<ListDTO>(sql, new Dictionary<string, object> { { "tenantId", MyUserInfo.TenantId } });

        }

        public ListDTO GetListInfo(int Id, UserInfo MyUserInfo)
        {
            const string sql =
                 @"Select lst.Id ListId,lst.listName,lst.listDesc,lst.tenantId,lst.userId,tn.tenantName from list lst
                inner join tenants tn on tn.Id=lst.tenantId
                Where tn.Id=@tenantId and lst.Id=@Id;";

            return this._db.Query<ListDTO>(sql, new Dictionary<string, object> { { "tenantId", MyUserInfo.TenantId }, { "Id", Id} }).FirstOrDefault();
        }
        public int CreateList(ListDTO listDTO, UserInfo MyUserInfo)
        {

            int NewListId = 0;
            //int rowsAffected = _db.Execute(@"INSERT list(listName,listDesc,tenantId,userId,lastupdate) values (@listName, @listDesc, @tenantId,@userId,@lastupdate)",
            //       new { listName = listDTO.ListName , listDesc = listDTO.ListDesc, tenantId = MyUserInfo.TenantId, userId = MyUserInfo.UserId, lastupdate = DateTime.Now });


            //return rowsAffected;

            //Insert and get id value
            //string insertUserSql = @"INSERT INTO dbo.[User](Username, Phone, Email)
            //            OUTPUT INSERTED.[Id]
            //            VALUES(@Username, @Phone, @Email);";

            //int newUserId = conn.QuerySingle<int>(insertUserSql,
            //                                new
            //                                {
            //                                    Username = "lorem ipsum",
            //                                    Phone = "555-123",
            //                                    Email = "lorem ipsum"
            //                                }, tran);

            //Insert and get whole row
            //string insertUserSql = @"INSERT INTO dbo.[User](Username, Phone, Email)
            //            OUTPUT INSERTED.*
            //            VALUES(@Username, @Phone, @Email);";

            //User newUser = conn.QuerySingle<User>(insertUserSql,
            //                                new
            //                                {
            //                                    Username = "lorem ipsum",
            //                                    Phone = "555-123",
            //                                    Email = "lorem ipsum"
            //                                }, tran);

            

            string insertUserSql = @"INSERT list(listName,listDesc,tenantId,userId,lastupdate) values (@listName, @listDesc, @tenantId,@userId,@lastupdate);
                                    SELECT LAST_INSERT_ID();";
            NewListId= this._db.QuerySingle<int>(insertUserSql,
                                            new
                                            {
                                                ListId = 0,
                                                listName = listDTO.ListName,
                                                listDesc = listDTO.ListDesc,
                                                tenantId=MyUserInfo.TenantId,
                                                userId=MyUserInfo.UserId,
                                                lastupdate=DateTime.Now
                                            });

            //if(RetlistDTO!=null)
            //{
            //    NewListId = RetlistDTO.ListId;
            //}

            //const string InsertQuery = @"
            //                    DECLARE @InsertedRows AS TABLE (Id int);
            //                    INSERT list(listName,listDesc,tenantId,userId,lastupdate) values (@listName, @listDesc, @tenantId,@userId,@lastupdate);
            //                    SELECT Id FROM @InsertedRows";

            //int NewListId = this._db.Query<int>(InsertQuery, new { listName = listDTO.ListName, listDesc = listDTO.ListDesc, tenantId = MyUserInfo.TenantId, userId = MyUserInfo.UserId, lastupdate = DateTime.Now }).Single();



            return NewListId;



        }

        public int DeleteList(int Id, UserInfo MyUserInfo)
        {
            string updateQuery = @"UPDATE [list] SET [isActive] = 0 WHERE Id =@Id";

            int result = this._db.Execute(updateQuery, new
            {
                Id
            });
            return result;
        }

        public int MoveToList(string EmailIds, int listId, UserInfo MyUserInfo)
        {
            int Id = 0;
            int i;
            int ReturnValue = 0;
            string[] AllEmailIDS;
            try
            {
                AllEmailIDS = EmailIds.Split(',');
                for (i = 0; i <= AllEmailIDS.Length-1; i++)
                {
                    Id = Convert.ToInt32(AllEmailIDS[i].ToString());

                    string moveQuery = @"UPDATE emaillist SET listId = @listId WHERE Id =@Id";

                    //string moveQuery1 = @"UPDATE emaillist SET [listId] = @listId WHERE Id  In (@Id);";

                    int result = this._db.Execute(moveQuery, new
                    {
                        listId,
                        Id
                    });

                }
                
                ReturnValue = 1;
            }
            catch(Exception exp)
            {
                ReturnValue = 0;
            }
            return ReturnValue;
        }

        public ListDTO UpdateList(ListDTO listDTO, UserInfo MyUserInfo)
        {
            string listName = "";
            string listDesc = "";
            int Id = 0;

            listName = listDTO.ListName;
            listDesc = listDTO.ListDesc;
            Id = listDTO.ListId;
            string updateQuery = @"UPDATE list SET listname = @listname ,listdesc = @listdesc WHERE Id =@Id";

            int result = this._db.Execute(updateQuery, new
            {
                listName,
                listDesc,
                Id

            });

            ListDTO updatedlistDTO;
            updatedlistDTO =GetListInfo(listDTO.ListId, MyUserInfo);
            return updatedlistDTO;
            
        }

        
    }
}
