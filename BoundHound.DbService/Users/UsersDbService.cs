﻿using BoundHound.DbService.Interfaces;
using BoundHound.Framework;
using BoundHound.Model;
using Dapper;
using MySql.Data.MySqlClient;
using System;
using System.Collections.Generic;
using System.Configuration;
using System.Data;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace BoundHound.DbService.Users
{
    public class UsersDbService : IUsersDbService
    {
        private readonly IDbConnection _db;
        string connection = ConfigurationManager.ConnectionStrings["LinkedInConnection"].ConnectionString;

        public UsersDbService()
        {
            _db = new MySqlConnection(ConfigurationManager.ConnectionStrings["LinkedInConnection"].ConnectionString);
        }

        public UserInfo TokenValidation(string Email, int UserId, int TenantId)
        {
            UserInfo MyUserInfo = new UserInfo();

            const string sql =
                 @"Select usr.Id as UserId,usr.userTypeId,usr.UserName,usr.userEmail,usr.userPassword,usr.TenantId,usr.roleId,
                    tn.Id,tn.tenantName,tn.linkedinEmail,tn.linkedinPassword,tn.subscriptionId
                    From Users usr
                    Inner Join tenants tn on tn.Id=usr.tenantId
                    where usr.userEmail=@email and usr.TenantId=@TenantId and usr.Id=@userId;";

            MyUserInfo = this._db.Query<UserInfo>(sql, new Dictionary<string, object> { { "email", Email }, { "TenantId", TenantId }, { "userId", UserId } }).FirstOrDefault();

            return MyUserInfo;

        }

        public UserInfo Authenticate(string UserName, string Password)
        {
            UserInfo MyUserInfo = new UserInfo();

            const string sql =
                 @"Select usr.Id as UserId,usr.userTypeId,usr.UserName,usr.userEmail,usr.userPassword,usr.TenantId,usr.roleId,
                    tn.Id,tn.tenantName,tn.linkedinEmail,tn.linkedinPassword,tn.subscriptionId
                    From Users usr
                    Inner Join tenants tn on tn.Id=usr.tenantId
                    where usr.userEmail=@email and usr.userPassword=@password;";

            MyUserInfo = this._db.Query<UserInfo>(sql, new Dictionary<string, object> { { "email", UserName }, { "password", Password } }).FirstOrDefault();

            return MyUserInfo;

        }

        public bool ChangePassword(BoundHoundContext context, LoginDTO dto)
        {
            throw new NotImplementedException();
        }

        public UserDTO GetUserInfo(BoundHoundContext context, int id)
        {
            throw new NotImplementedException();
        }

        public UserDTO IsUserCredentialsValid(string userName)
        {
            throw new NotImplementedException();
        }

        public bool IsUserExist(string userName)
        {
            throw new NotImplementedException();
        }

        public bool ResetPassword(BoundHoundContext context, int id)
        {
            throw new NotImplementedException();
        }
    }
}
