﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Runtime.Serialization;
using System.Text;
using System.Threading.Tasks;

namespace BoundHound.DbService.Exceptions
{
    [Serializable]
    public class ParameterMismatchException : Exception
    {
        public ParameterMismatchException()
        {
        }

        public ParameterMismatchException(string message) : base(message)
        {
        }

        public ParameterMismatchException(string message, Exception inner) : base(message, inner)
        {
        }

        public ParameterMismatchException(SerializationInfo info, StreamingContext context) : base(info, context)
        {

        }
    }
}
