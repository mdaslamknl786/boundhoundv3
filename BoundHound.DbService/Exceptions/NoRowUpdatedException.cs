﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Runtime.Serialization;
using System.Text;
using System.Threading.Tasks;

namespace BoundHound.DbService.Exceptions
{
    public class NoRowUpdatedException : Exception
    {
        public NoRowUpdatedException()
        {
        }

        public NoRowUpdatedException(string message) : base(message)
        {
        }

        public NoRowUpdatedException(string message, Exception inner) : base(message, inner)
        {
        }

        public NoRowUpdatedException(SerializationInfo info, StreamingContext context) : base(info, context)
        {

        }
    }
}
