﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace BoundHound.Enterprise.Framework
{
    namespace Constant
    {
        public struct AppSettingKey
        {
            public const string MasterDbReadConnectionString = "MasterDbReadConnectionString";
            public const string MasterDbWriteConnectionString = "MasterDbWriteConnectionString";
            public const string SchoolDbReadConnectionString = "SchoolDbReadConnectionString";
            public const string SchoolDbWriteConnectionString = "SchoolDbWriteConnectionString";
            public const string SharedFilesFolderPath = "SharedFilesFolderPath";
        }
    }
}
