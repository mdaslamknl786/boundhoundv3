﻿using BoundHound.DbService.DTOs;
using BoundHound.DbService.Exceptions;
using BoundHound.DbService.Interfaces;
using BoundHound.DbService.Utils;
using BoundHound.Enterprise.Framework.Constant;
using Dapper;
using MySql.Data.MySqlClient;
using NodaTime;
using NodaTime.Text;
using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.ComponentModel.DataAnnotations;
using System.Configuration;
using System.Data;
using System.Diagnostics;
using System.Linq;
using System.Text;
using System.Text.RegularExpressions;
using System.Threading.Tasks;


namespace BoundHound.DbService
{
    public class DatabaseService : IDatabaseService
    {
        private readonly IDbProfilingBlock m_dbProfilingBlock;

        /// <summary>
        /// Bad characaters which break sql
        /// </summary>
        internal static List<char> BadCharacters = new List<char> { (char)8216, (char)8217, (char)697, (char)700, (char)712, (char)8242 };

        protected int MaximumTransactionLevel;
        protected int CurrentTransactionLevel;

        private bool m_shouldCheckQueryLevel;

        private bool m_shouldTrackQueries;

        private bool m_detectLiteralsInQueries;

        private bool m_shouldDetectDateFunctionsInQueries;

        private static bool _shouldCompareParametersWithSql;


        public virtual void CheckQueryLevel()
        {
            if (m_shouldCheckQueryLevel && CurrentTransactionLevel > MaximumTransactionLevel)
            {
                throw new Exception("Queries are not allowed during a transaction");
            }
        }

        protected virtual void CheckQueryForLiterals(string sql)
        {
            if (m_detectLiteralsInQueries)
            {
                PerformCheckQueryForLiterals(sql);
            }
        }

        private void PerformCheckQueryForLiterals(string sql)
        {
            // Each line that is concatenated is looking for a different case            
            const string literalRegex = @"(=\s*'[^'])" +            // = followed by ', unless it is ''
                                        @"|(=\s*\d)" +              // = followed by a digit
                                        @"|(IN\s*\(\s*['|\d])" +    // IN ( followed by ' or a digit
                                        @"|(<>\s*'[^'])" +          // <> followed by ', unless it is ''
                                        @"|(<>\s*\d)" +             // <> followed by a digit
                                        @"|(LIKE\s*'\w)" +           // LIKE ' followed by a letter or digit
                                        @"|(LIKE\s*'\%\w)";          // LIKE '% followed by a letter or digit

            // Timeout after 5 seconds
            var match = Regex.Match(sql, literalRegex, RegexOptions.IgnoreCase,
                TimeSpan.FromSeconds(5));
            if (match.Success)
            {
                throw new Exception($"Queries with literals are not allowed: \"{match.Captures[0]}\" was found in {sql}");
            }
        }
        protected virtual void CheckQueryForDateFunctions(string sql)
        {
            if (m_shouldDetectDateFunctionsInQueries)
            {
                PerformCheckQueryForDateFunctions(sql);
            }
        }


        private void PerformCheckQueryForDateFunctions(string sql)
        {
            var mySqlDateFunctions = new List<string>()
            {
                "CURDATE()",
                "CURRENT_DATE",
                "CURRENT_TIME",
                "CURTIME()",
                "CURRENT_TIMESTAMP,",
                "NOW()",
                "LOCALTIME",
                "SYSDATE()"
            };

            foreach (var mySqlDateFunction in mySqlDateFunctions)
            {
                if (sql.IndexOf(mySqlDateFunction, StringComparison.InvariantCultureIgnoreCase) >= 0)
                {
                    throw new Exception($"Queries with {mySqlDateFunction} are not allowed: found in {sql}");
                }
            }

        }
        private static void CheckInvalidDateTimeParameter(IEnumerable<KeyValuePair<string, object>> whereConditions)
        {
#if DEBUG
            if (whereConditions == null)
            {
                return;
            }

            // Get values that are typed as DateTimes that only have Date component, that is, are being used as 
            //  dates instead of datetimes. Exclude DateTime.MinValue because that is actually a DateTime, just with
            //  a time component of 0.
            var datesThatArePassedAsDateTimes = whereConditions.Where(
                pair => pair.Value is DateTime
                        && ((DateTime)pair.Value).Date == (DateTime)pair.Value
                        && (DateTime)pair.Value != DateTime.MinValue);

            foreach (var pair in datesThatArePassedAsDateTimes)
            {
                throw new ValidationException(
                    $"Date parameter {pair.Key} should be passed as string, not DateTime. It has a value of {pair.Value}.");
            }
#endif
        }

        private static void CheckInvalidStringParameters(IEnumerable<KeyValuePair<string, object>> whereConditions)
        {
#if DEBUG
            if (whereConditions == null)
            {
                return;
            }
            foreach (var pair in whereConditions.Where(
                pair => pair.Value is string && IsStringCommaListIds(pair.Value.ToString())))
            {
                throw new ValidationException(
                    $"String parameter {pair.Key} is a list of integers. It has a value of {pair.Value}.");
            }
#endif
        }

        public static bool IsStringCommaListIds(string input)
        {
            int value;
            const char comma = ',';
            if (!input.Contains(comma)) return false;
            var array = input.Split(comma);
            return array.All(item => int.TryParse(item, out value));
        }

        private readonly List<QueryInfo> m_queries = new List<QueryInfo>();

        public ReadOnlyCollection<QueryInfo> Queries => m_queries.Count == 0 ? null : m_queries.AsReadOnly();

        public void ResetTrackQueries()
        {
            ClearTrackedQueries();
        }

        protected void ClearTrackedQueries() => m_queries.Clear();

        protected virtual void TrackQuery(string loggedSql, long elapsedMilliseconds)
        {
            if (m_shouldTrackQueries)
            {
                m_queries.Add(new QueryInfo(loggedSql, elapsedMilliseconds));
            }
        }

        /// <summary>
        /// Executes the data table without running account Id validation.
        /// </summary>
        /// <param name="conn">Database connection to use</param>
        /// <param name="sql">The SQL.</param>
        /// <param name="whereConditions">The where conditions.</param>
        /// <param name="timeout">The timeout.</param>
        /// <param name="skipQueryLevelCheck"></param>
        /// <returns></returns>
        /// <remarks>
        /// This avoids any kind of issue with validation, whether it be that
        /// validation doesn't need to happen, or that validation otherwise
        /// causes more problems than it solves.
        /// </remarks>
        public virtual DataTable ExecuteDataTable(IDbConnection conn, string sql, IDictionary<string, object> whereConditions, int? timeout = null, bool skipQueryLevelCheck = false)
        {
            Func<IDbConnection, IDictionary<string, object>, IEnumerable<DataTable>> performQuery =
                (connection, localWhereConditions) =>
                {
                    using (var cmd = PrepareCommandObject(sql, localWhereConditions))
                    {
                        cmd.Connection = connection;
                        if (timeout.HasValue && timeout.Value > 0)
                        {
                            cmd.CommandTimeout = timeout.Value;
                        }

                        var table = new DataTable();
                        var adapt = new MySqlDataAdapter { SelectCommand = (MySqlCommand)cmd };
                        adapt.Fill(table);
                        return new[] { table };
                    }
                };
            var results = ExecuteQuery(sql, whereConditions, conn, performQuery, skipQueryLevelCheck: skipQueryLevelCheck);

            return results.Single();
        }

        public virtual DbResultWithCount<T> GetResultSetWithFullCount<T>(IDbConnection conn, string sql,
            IDictionary<string, object> whereConditions = null, int? timeout = null)
        {

            var gridReader = conn.QueryMultiple(sql, whereConditions, commandTimeout: timeout);

            var results = gridReader.Read<T>().ToList();
            var count = (ulong)gridReader.Read<long>().Single();

            return new DbResultWithCount<T>(results, count);
        }

        public virtual IEnumerable<T> Query<T>(IDbConnection conn, string sql, IDictionary<string, object> parameters, int? timeout = null)
        {
            Func<IDbConnection, IDictionary<string, object>, IEnumerable<T>> performQuery =
                (connection, whereConditions) => connection.Query<T>(sql, whereConditions, commandTimeout: timeout);
            var results = ExecuteQuery(sql, parameters, conn, performQuery);

            return results;
        }

        public IEnumerable<T> Query<T>(int schoolId, string sql, IDictionary<string, object> parameters = null,
            int? timeout = null)
        {
            Func<IDbConnection, IDictionary<string, object>, IEnumerable<T>> performQuery =
                (connection, whereConditions) => connection.Query<T>(sql, whereConditions, commandTimeout: timeout);
            var results = ExecuteQuery(sql, parameters, GetReadConnection(schoolId), performQuery);

            return results;
        }

        public IEnumerable<TReturn> Query<TFirst, TSecond, TReturn>(int schoolId, string sql, Func<TFirst, TSecond, TReturn> map, IDictionary<string, object> parameters = null,
            int? timeout = null)
        {
            IEnumerable<TReturn> PerformQuery(IDbConnection connection, IDictionary<string, object> whereConditions) => connection.Query(sql, map, whereConditions, commandTimeout: timeout);
            var results = ExecuteQuery(sql, parameters, GetReadConnection(schoolId), (Func<IDbConnection, IDictionary<string, object>, IEnumerable<TReturn>>)PerformQuery);

            return results;
        }

        public IEnumerable<TReturn> Query<TFirst, TSecond, TReturn>(int schoolId, string sql, Func<TFirst, TSecond, TReturn> map, string splitOn, IDictionary<string, object> parameters = null,
            int? timeout = null)
        {
            IEnumerable<TReturn> PerformQuery(IDbConnection connection, IDictionary<string, object> whereConditions) => connection.Query(sql, map, whereConditions, splitOn: splitOn, commandTimeout: timeout);
            var results = ExecuteQuery(sql, parameters, GetReadConnection(schoolId), (Func<IDbConnection, IDictionary<string, object>, IEnumerable<TReturn>>)PerformQuery);

            return results;
        }

        public virtual IEnumerable<TReturn> Query<TFirst, TSecond, TReturn>(IDbConnection conn, string sql, Func<TFirst, TSecond, TReturn> map, IDictionary<string, object> parameters = null, int? timeout = null)
        {
            Func<IDbConnection, IDictionary<string, object>, IEnumerable<TReturn>> performQuery =
                (connection, whereConditions) => connection.Query(sql, map, whereConditions, commandTimeout: timeout);
            var results = ExecuteQuery(sql, parameters, conn, performQuery);

            return results;
        }



        public virtual IEnumerable<TReturn> Query<TFirst, TSecond, TThird, TReturn>(IDbConnection conn, string sql,
            Func<TFirst, TSecond, TThird, TReturn> map, IDictionary<string, object> parameters = null,
            int? timeout = null)
        {
            Func<IDbConnection, IDictionary<string, object>, IEnumerable<TReturn>> performQuery =
                (connection, whereConditions) => connection.Query(sql, map, whereConditions, commandTimeout: timeout);
            var results = ExecuteQuery(sql, parameters, conn, performQuery);

            return results;
        }

        public IEnumerable<TReturn> Query<TFirst, TSecond, TThird, TReturn>(int schoolId, string sql, Func<TFirst, TSecond, TThird, TReturn> map, IDictionary<string, object> parameters,
            int? timeout = null)
        {
            Func<IDbConnection, IDictionary<string, object>, IEnumerable<TReturn>> performQuery =
                (connection, whereConditions) => connection.Query(sql, map, whereConditions, commandTimeout: timeout);
            var results = ExecuteQuery(sql, parameters, GetReadConnection(schoolId), performQuery);

            return results;
        }

        public virtual IEnumerable<TReturn> Query<TFirst, TSecond, TThird, TFourth, TReturn>(IDbConnection conn, string sql,
            Func<TFirst, TSecond, TThird, TFourth, TReturn> map, IDictionary<string, object> parameters = null,
            int? timeout = null)
        {
            Func<IDbConnection, IDictionary<string, object>, IEnumerable<TReturn>> performQuery =
                (connection, whereConditions) => connection.Query(sql, map, whereConditions, commandTimeout: timeout);
            var results = ExecuteQuery(sql, parameters, conn, performQuery);

            return results;
        }

        public virtual DataRow ExecuteDataRowWithoutValidation(IDbConnection conn, string sql, IDictionary<string, object> parameters = null)
        {
            var table = ExecuteDataTable(conn, sql, parameters);
            ValidateNoMoreThanOneRow(table);
            return table.Rows.Count == 1 ? table.Rows[0] : null;
        }

        protected static void ValidateNoMoreThanOneRow(DataTable table)
        {
            if (table.Rows.Count > 1)
            {
                throw new DataException($"Expected one row; found {table.Rows.Count} rows.");
            }
        }

        public virtual int ExecuteUpdateStatement(IDbConnection conn, IDictionary<string, object> fields, IDictionary<string, object> whereConditions, string tableName, int? expectedCount = null)
        {
            // Create a new dictionary of fields called updateFields that are used in the actual update statement.
            // It does not include any fields that also appeared in whereConditions if the value is the same.
            var updateFields = fields.Where(item => !whereConditions.ContainsKey(item.Key) || !Equals(whereConditions[item.Key], item.Value)).ToDictionary(item => item.Key, item => item.Value);

            // prepare command with tablename and fieldnames
            var sql = PrepareUpdateStatement(tableName, updateFields.Keys, whereConditions.Keys);

            // Detect any conflicts between fields and whereConditions where the key and value differ since we can't have
            // the sql parameter have the same name, but different values. If the value is the same, we can keep just one instance.
            var overlap = fields.Keys.Intersect(whereConditions.Keys).ToList();
            foreach (var key in overlap)
            {
                // Use object.Equals to handle unboxing
                if (fields[key].Equals(whereConditions[key]))
                {
                    fields.Remove(key);
                }
                else
                {
                    throw new ArgumentException($"{key} has already been defined");
                }
            }

            // Combine both fields and where conditions into a single dictionary of parameters that are added to the command object
            var parameters =
                new[] { fields, whereConditions }
                    .SelectMany(dict => dict)
                    .ToDictionary(pair => pair.Key, pair => pair.Value);

            var cmd = PrepareCommandObject(sql, parameters);

            cmd.Connection = conn;

            try
            {
                var actualCount = cmd.ExecuteNonQuery();
                if (expectedCount.HasValue && actualCount != expectedCount.Value)
                {
                    throw new NoRowUpdatedException(
                        $"Expected to update {expectedCount} records, but actually updated {actualCount} records.");
                }
                return actualCount;
            }
            catch (Exception ex)
            {
                if (ex is NoRowUpdatedException)
                {
                    throw;
                }
                else
                {
                    throw new Exception(
                        $"Error in ExecuteUpdateStatement: {ex.Message}; SQL: {cmd.CommandText} with Parameters {SerializeParameters(parameters)}", ex);
                }
            }
            finally
            {
                cmd.Dispose();
            }
        }

        protected static string PrepareUpdateStatement(string tableName, IEnumerable<string> fieldNames, IEnumerable<string> whereConditions)
        {
            var fieldParams = fieldNames.Aggregate(string.Empty, (current, name) => string.Format("{0}{1} = ?{1},", current, name));
            var whereParams = whereConditions.Aggregate(string.Empty, (current, name) => string.Format("{0} {1} = ?{1} AND", current, name));

            return
                $"UPDATE {tableName} SET {fieldParams.TrimEnd(Convert.ToChar(","))} WHERE {whereParams.Remove(whereParams.Length - 3)} ";
        }

        public virtual T ExecuteInsertStatement<T>(IDbConnection conn, IDictionary<string, object> fields, string tableName, bool ignoreDuplicates = false)
        {
            // prepare command with tablename and fieldnames
            var cmd = PrepareCommandObject(PrepareInsertStatement(tableName, fields.Keys.ToList(), ignoreDuplicates), fields);

            try
            {
                cmd.Connection = conn;

                var opened = false;
                try
                {
                    if (conn.State != ConnectionState.Open)
                    {
                        conn.Open();
                        opened = true;
                    }

                    //execute
                    cmd.ExecuteNonQuery();

                    cmd.CommandText = "SELECT last_insert_id()";

                    return (T)Convert.ChangeType(cmd.ExecuteScalar(), typeof(T));
                }
                finally
                {
                    cmd.Dispose();

                    if (opened)
                    {
                        conn.Close();
                        conn.Dispose();
                    }
                }
            }
            catch (Exception ex)
            {
                throw new Exception(
                    $"Error in ExecuteInsertStatement: {ex.Message}; SQL: {cmd.CommandText}", ex);
            }
        }

        protected string PrepareInsertStatement(string tableName, IEnumerable<string> fields, bool ignoreDuplicates)
        {
            var names = new List<string>();
            var values = new List<string>();
            foreach (var field in fields)
            {
                names.Add(field);
                values.Add("?" + field);
            }

            var ignoreClause = ignoreDuplicates ? "IGNORE " : string.Empty;
            var nameClause = string.Join(", ", names);
            var valuesClause = string.Join(", ", values);
            return $"INSERT {ignoreClause}INTO {tableName} ({nameClause}) VALUES ({valuesClause})";

        }

        public virtual int ExecuteDeleteStatement(IDbConnection conn, IDictionary<string, object> whereConditions, string tableName, int? expectedCount = null)
        {
            // prepare command with tablename and fieldnames
            var sql = PrepareDeleteStatement(tableName, whereConditions.Keys);

            var actualCount = ExecuteNonQuery(conn, sql, whereConditions);
            if (expectedCount.HasValue && actualCount != expectedCount.Value)
            {
                throw new Exception(
                    $"Expected to delete {expectedCount} records, but actually deleted {actualCount} records.");
            }
            return actualCount;
        }

        protected static string PrepareDeleteStatement(string tableName, IEnumerable<string> whereConditions)
        {
            var whereParams = whereConditions.Aggregate(string.Empty, (current, name) => string.Format("{0} {1} = ?{1} AND", current, name));

            return $"DELETE FROM {tableName} WHERE {whereParams.Remove(whereParams.Length - 3)} ";
        }

        public virtual int ExecuteNonQuery(IDbConnection conn, string sql, IDictionary<string, object> parameters, int? timeout = null)
        {
            int numRowsAffected;
            var opened = false;
            var cmd = PrepareCommandObject(sql, parameters);

            try
            {
                // Track whether we opened the connection
                if (conn.State != ConnectionState.Open)
                {
                    opened = true;
                    conn.Open();
                }

                cmd.Connection = conn;
                if (timeout.HasValue)
                {
                    cmd.CommandTimeout = timeout.Value;
                }

                // execute non query
                numRowsAffected = cmd.ExecuteNonQuery();

            }
            catch (Exception ex)
            {
                var serializedParameters = SerializeParameters(parameters);
                throw new Exception(
                    $"Error in ExecuteNonQuery: {ex.Message}; SQL: {sql} with Parameters {serializedParameters}", ex);
            }
            finally
            {
                cmd.Dispose();
                if (opened)
                {
                    conn.Close();
                    conn.Dispose();
                }
            }

            return numRowsAffected;
        }

        public virtual T ExecuteScalar<T>(IDbConnection conn, string sql, IDictionary<string, object> parameters = null)
        {
            return (T)Convert.ChangeType(ExecuteScalar(conn, sql, parameters), typeof(T));
        }

        public virtual object ExecuteScalar(IDbConnection conn, string sql, IDictionary<string, object> parameters = null)
        {
            var opened = false;
            using (var cmd = PrepareCommandObject(sql, parameters))
            {
                if (conn.State != ConnectionState.Open)
                {
                    conn.Open();
                    opened = true;
                }
                cmd.Connection = conn;

                try
                {
                    return cmd.ExecuteScalar();
                }
                finally
                {
                    cmd.Dispose();

                    if (opened)
                    {
                        conn.Close();
                        conn.Dispose();
                    }
                }
            }

        }

        /// <summary>
        /// This function holds the common code between ExecuteDataTable and Query<typeparam name="T"></typeparam>. The
        /// actual query statement is controlled by the behavior of the passed <see cref="performQuery"/> lambda.
        /// </summary>
        /// <param name="sql"></param>
        /// <param name="whereConditions"></param>
        /// <param name="conn"></param>
        /// <param name="performQuery"></param>
        /// <param name="needsConnectionOpen"></param>
        /// <param name="skipQueryLevelCheck"></param>
        /// <returns></returns>
        private IEnumerable<T> ExecuteQuery<T>(string sql, IDictionary<string, object> whereConditions,
            IDbConnection conn, Func<IDbConnection, IDictionary<string, object>, IEnumerable<T>> performQuery,
            bool needsConnectionOpen = false, bool skipQueryLevelCheck = false)
        {
            var serializedWhereConditions = SerializeParameters(whereConditions);
            var loggedSql = string.Concat(sql, "\nWhereConditions:\n", serializedWhereConditions);

            if (!skipQueryLevelCheck)
            {
                CheckQueryLevel();
            }
            CheckQueryForLiterals(sql);
            CheckQueryForDateFunctions(sql);
            CheckInvalidDateTimeParameter(whereConditions);
            CheckInvalidStringParameters(whereConditions);

            var watch = Stopwatch.StartNew();

            var opened = false;

            try
            {
                if (conn.State != ConnectionState.Open)
                {
                    opened = true;
                    conn.Open();
                }

                using (m_dbProfilingBlock.Initialize(sql, conn))
                {
                    var results = performQuery(conn, whereConditions);

                    return results;
                }
            }
            catch (Exception ex)
            {
                throw new Exception(
                    $"Error in ExecuteQuery: {ex.Message}; SQL: {sql} with Parameters: {serializedWhereConditions}", ex);
            }
            finally
            {
                // If the caller needs the connection to remain open, we need to skip the closing.
                if (opened && !needsConnectionOpen)
                {
                    CloseAndDisposeConnection(conn);
                }

                watch.Stop();
                TrackQuery(loggedSql, watch.ElapsedMilliseconds);
            }
        }

        private static string SerializeParameters(IEnumerable<KeyValuePair<string, object>> parameters)
        {
            if (parameters == null)
            {
                return "[ ]";
            }
            return string.Format("[ {0}\n]", parameters.Aggregate(string.Empty,
                                                                 (result, pair) =>
                                                                 result == string.Empty
                                                                     ? $"\n\t{{\"{pair.Key}\" : {SerializeValue(pair.Value)}}}"
                                                                     : $"{result},\n\t{{\"{pair.Key}\" : {SerializeValue(pair.Value)}}}"));
        }

        private static string SerializeValue(object value)
        {
            if (value == null)
            {
                return string.Empty;
            }

            var idList = value as IEnumerable<int>;
            var uintList = value as IEnumerable<uint>;
            var stringList = value as IEnumerable<string>;
            var stringValue = value as string;
            var isDate = value is DateTime;

            if (idList != null)
            {
                return $"[ {string.Join(", ", idList)} ]";
            }
            if (uintList != null)
            {
                return $"[ {string.Join(", ", uintList)} ]";
            }
            if (stringValue != null)
            {
                return string.Concat("\"", stringValue, "\"");
            }
            if (isDate)
            {
                var dateValue = (DateTime)value;
                var formatString = (dateValue == dateValue.Date) ? "yyyy-MM-dd" : "yyyy-MM-dd HH:mm:ss";
                return string.Concat("\"", ((DateTime)value).ToString(formatString), "\"");
            }

            if (stringList != null)
            {
                return string.Format("[ {0} ]", string.Join(",", stringList.Select(str => $"\"{str}\"").ToArray()));
            }

            return value.ToString();
        }

        private static IDbCommand PrepareCommandObject(string sql) => new MySqlCommand { CommandText = sql };

        protected static IDbCommand PrepareCommandObject(string sql, IDictionary<string, object> parameters)
        {
            // A list of parameters to add to the command
            var sqlParams = new List<IDataParameter>();

            // Loop over all of the where names
            if (parameters != null)
            {
                foreach (var item in parameters)
                {
                    var name = item.Key;
                    var value = item.Value;

                    var intList = value as IEnumerable<int>;
                    var uintList = value as IEnumerable<uint>;
                    var ulongList = value as IEnumerable<ulong>;
                    var stringList = value as IEnumerable<string>;
                    var isLocalDate = value is LocalDate;
                    var isInstant = value is Instant;
                    var isZonedDateTime = value is ZonedDateTime;

                    var elementType = value.GetElementType();

                    // If a caller passes in uint[] in the dictionary, then both intList and uintList
                    // have non-null values, but ToList() fails because we cannot change the uint in 
                    // the array into the int in List<int>. Therefore, we need to check the underlying
                    // element type    
                    if (intList != null && (elementType == typeof(int) || elementType.BaseType == typeof(System.Enum)))
                    {
                        var list = intList.ToList(); // avoid multiple enumerations

                        // If the list is empty the sql will break which is not our intention - instead we add one integer -1
                        if (!list.Any())
                        {
                            list = new List<int> { -1 };
                        }

                        // If the value is a list of IDs, we create a different parameter
                        // for each ID in the list
                        var parameterNames = new List<string>();

                        var j = 0;
                        foreach (var id in list)
                        {
                            // Generate a new parameter name based on the original parameter name and a new index.
                            var parameterName = $"{name}{j}";

                            // Create a new parameter for this ID.
                            sqlParams.Add(PrepareSqlParameter(parameterName, id));

                            // Add the new parameter name (with a preceding question mark) to the list of parameter names
                            // used to update the original SQL.
                            parameterNames.Add($"?{parameterName}");
                            j++;
                        }

                        // Expand the original SQL to have a list of comma delimited parameter names instead of the original parameter
                        sql = Regex.Replace(sql, $"\\?{name}", string.Join(",", parameterNames.ToArray()), RegexOptions.IgnoreCase);
                    }
                    else if (uintList != null && elementType == typeof(uint))
                    {
                        var list = uintList.ToList(); // avoid multiple enumerations

                        // If the list is empty the SQL statement will throw an error, which is not our intention - instead we add MaxValue because there probably isn't a row with MaxValue
                        if (!list.Any())
                        {
                            list = new List<uint> { uint.MaxValue };
                        }

                        // If the value is a list of IDs, we create a different parameter
                        // for each ID in the list
                        var parameterNames = new List<string>();

                        var j = 0;
                        foreach (var id in list)
                        {
                            // Generate a new parameter name based on the original parameter name and a new index.
                            var parameterName = $"{name}{j}";

                            // Create a new parameter for this ID.
                            sqlParams.Add(PrepareSqlParameter(parameterName, id));

                            // Add the new parameter name (with a preceding question mark) to the list of parameter names
                            // used to update the original SQL.
                            parameterNames.Add($"?{parameterName}");
                            j++;
                        }

                        // Expand the original SQL to have a list of comma delimited parameter names instead of the original parameter
                        sql = Regex.Replace(sql, $"\\?{name}", string.Join(",", parameterNames.ToArray()), RegexOptions.IgnoreCase);
                    }
                    else if (ulongList != null && elementType == typeof(ulong))
                    {
                        var list = ulongList.ToList(); // avoid multiple enumerations

                        // If the list is empty the SQL statement will throw an error, which is not our intention - instead we add MaxValue because there probably isn't a row with MaxValue
                        if (!list.Any())
                        {
                            list = new List<ulong> { ulong.MaxValue };
                        }

                        // If the value is a list of IDs, we create a different parameter
                        // for each ID in the list
                        var parameterNames = new List<string>();

                        var j = 0;
                        foreach (var id in list)
                        {
                            // Generate a new parameter name based on the original parameter name and a new index.
                            var parameterName = $"{name}{j}";

                            // Create a new parameter for this ID.
                            sqlParams.Add(PrepareSqlParameter(parameterName, id));

                            // Add the new parameter name (with a preceding question mark) to the list of parameter names
                            // used to update the original SQL.
                            parameterNames.Add($"?{parameterName}");
                            j++;
                        }

                        // Expand the original SQL to have a list of comma delimited parameter names instead of the original parameter
                        sql = Regex.Replace(sql, $"\\?{name}", string.Join(",", parameterNames.ToArray()), RegexOptions.IgnoreCase);
                    }
                    else if (stringList != null && elementType == typeof(string))
                    {
                        var list = stringList.ToList(); // avoid multiple enumerations

                        // If the list is empty the sql will break which is not our intention - instead we add one integer "-1"
                        if (!list.Any())
                        {
                            list = new List<string> { "-1" };
                        }

                        // If the value is a list of strings, we create a different parameter
                        // for each string in the list
                        var parameterNames = new List<string>();

                        var j = 0;
                        foreach (var str in list)
                        {
                            // Generate a new parameter name based on the original parameter name and a new index.
                            var parameterName = $"{name}{j}";

                            // Create a new parameter for this string.
                            sqlParams.Add(PrepareSqlParameter(parameterName, str));

                            // Add the new parameter name (with a preceding question mark) to the list of parameter names
                            // used to update the original SQL.
                            parameterNames.Add($"?{parameterName}");
                            j++;
                        }

                        // Expand the original SQL to have a list of comma delimited parameter names instead of the original parameter
                        sql = Regex.Replace(sql, $"\\?{name}", string.Join(",", parameterNames.ToArray()), RegexOptions.IgnoreCase);
                    }
                    else if (isLocalDate)
                    {
                        sqlParams.Add(PrepareSqlParameter(name, LocalDatePattern.Iso.Format((LocalDate)value)));
                    }
                    else if (isInstant)
                    {
                        sqlParams.Add(PrepareSqlParameter(name, ((Instant)value).ToDateTimeUtc()));
                    }
                    else if (isZonedDateTime)
                    {
                        sqlParams.Add(PrepareSqlParameter(name, ((ZonedDateTime)value).ToString("yyyy'-'MM'-'dd'T'hh':'mm':'ss", null)));
                    }
                    else
                    {
                        sqlParams.Add(PrepareSqlParameter(name, value));
                    }
                }
            }

            var cmd = PrepareCommandObject(sql);
            foreach (var parameter in sqlParams)
            {
                cmd.Parameters.Add(parameter);
            }

            CompareParametersWithSql(sql, sqlParams);

            return cmd;
        }

        private static void CompareParametersWithSql(string sql, IEnumerable<IDataParameter> sqlParams)
        {
            if (!_shouldCompareParametersWithSql)
            {
                return;
            }
            var parameterNames = sqlParams.Select(param => param.ParameterName);
            var parametersFromSql = ExtractParametersFromSql(sql);

            var distinctNames = parameterNames.ToHashSet();
            var distinctNamesFromSql = parametersFromSql.ToHashSet();

            if (distinctNames.SetEquals(distinctNamesFromSql))
            {
                return;
            }

            var parametersMissingFromSql = distinctNames.Except(distinctNamesFromSql, StringComparer.InvariantCultureIgnoreCase).ToList();

            string errorMessageFragment;
            if (parametersMissingFromSql.Any())
            {
                errorMessageFragment =
                    "Parameters missing from sql, but provided in parameter list";
                var errorMessage = string.Format("{0}: {1}",
                    errorMessageFragment,
                    parametersMissingFromSql.Aggregate("", (total, current) => total + ", " + current)
                        .Trim(',', ' '));

                throw new ParameterMismatchException(errorMessage);
            }

            var parametersMissingFromInputParameters = distinctNamesFromSql.Except(distinctNames, StringComparer.InvariantCultureIgnoreCase).ToList();
            if (parametersMissingFromInputParameters.Any())
            {
                errorMessageFragment =
                    "Parameters are missing from parameter list, but provided in sql";
                var errorMessage = string.Format("{0}: {1}",
                    errorMessageFragment,
                    parametersMissingFromInputParameters.Aggregate("", (total, current) => total + ", " + current)
                        .Trim(',', ' '));

                throw new ParameterMismatchException(errorMessage);
            }
        }

        private static readonly Regex ParamRegex = new Regex(@"(\?[A-Za-z0-9]*)", RegexOptions.Compiled | RegexOptions.IgnoreCase);

        public DatabaseService(IDbProfilingBlock dbProfilingBlock)
        {
            m_dbProfilingBlock = dbProfilingBlock;
        }

        private static IEnumerable<string> ExtractParametersFromSql(string sql)
        {
            var allMatches = ParamRegex.Matches(sql);

            return from Match match in allMatches select match.ToString();
        }

        public static IDataParameter PrepareSqlParameter(string name, object value)
        {
            // replace smart quotes from word 
            var s = value as string;
            if (s != null)
            {
                var newString = s;
                newString = PrepareSqlParameterForString(newString);
                value = newString;
            }
            else if (value is LocalDate)
            {
                return PrepareSqlParameter(name, LocalDatePattern.Iso.Format((LocalDate)value));
            }

            var massagedValue = value is DateTime
                ? TruncateFractionalSeconds((DateTime)value)
                : value;

            return new MySqlParameter
            {
                ParameterName = $"?{name}",
                Value = massagedValue
            };
        }

        /// <summary>
        /// Takes a date and removes any fractional part. This is because 1) we don't rely on fractions
        /// of a second for any business logic, and 2) because MySQL does not store them reliably.
        /// </summary>
        /// <param name="dateTime"></param>
        /// <returns></returns>
        private static DateTime TruncateFractionalSeconds(DateTime dateTime)
        {
            const int ticksToTruncate = 10000 * 1000; // milliseconds in a second * ticks in 1ms
            var truncatedTicks = dateTime.Ticks - (dateTime.Ticks % ticksToTruncate);
            return new DateTime(truncatedTicks);
        }

        /// <summary>
        /// Prepare and replace sql parameter for invalid  characters in string
        /// </summary>
        /// <param name="text"></param>
        /// <returns></returns>
        public static string PrepareSqlParameterForString(string text)
            =>
                !string.IsNullOrWhiteSpace(text)
                    ? BadCharacters.Aggregate(text, (current, chr) => current.Replace(chr, '\''))
                    : string.Empty;

        public virtual IDbConnection PrepareConnection(string connString) => new MySqlConnection { ConnectionString = connString };

        public virtual void CloseAndDisposeConnection(IDbConnection conn)
        {
            if (conn?.State == ConnectionState.Open)
            {
                conn.Close();
            }
            conn?.Dispose();
        }

        public virtual IDbTransaction BeginTransaction(IDbConnection conn)
        {
            CurrentTransactionLevel++;
            if (conn.State != ConnectionState.Open)
            {
                conn.Open();
            }
            return conn.BeginTransaction();
        }

        #region Select

        protected string GetConnectionString(AccessType accessType)
        {
            var connectionType =
                accessType == AccessType.Write
                    ? AppSettingKey.MasterDbWriteConnectionString
                    : AppSettingKey.MasterDbReadConnectionString;

            return ConfigurationManager.AppSettings[connectionType];
        }

        protected string GetSchoolConnectionString(AccessType accessType)
        {
            var connectionType =
                accessType == AccessType.Write
                    ? AppSettingKey.SchoolDbWriteConnectionString
                    : AppSettingKey.SchoolDbReadConnectionString;
            return ConfigurationManager.AppSettings[connectionType];
        }

        #endregion

        public virtual IDbConnection GetMasterDbWriteConnection()
        {
            return PrepareConnection(GetConnectionString(AccessType.Write));
        }

        public virtual IDbConnection GetMasterDbReadConnection()
        {
            return PrepareConnection(GetConnectionString(AccessType.Read));
        }

        public IDbConnection GetReadConnection(int schoolId)
        {
            return PrepareConnection(GetSchoolConnectionString(AccessType.Read));
        }

        public IDbConnection GetSchoolReadConnection()
        {
            return PrepareConnection(GetSchoolConnectionString(AccessType.Read));
        }

        public IDbConnection GetWriteConnection(int schoolId)
        {
            return PrepareConnection(GetSchoolConnectionString(AccessType.Write));
        }

        public virtual void CommitTransaction(IDbTransaction transaction) => transaction.Commit();

        public virtual void RollbackTransaction(IDbTransaction transaction) => transaction.Rollback();

        public virtual void DisposeTransaction(IDbTransaction transaction)
        {
            CurrentTransactionLevel--;
            transaction.Dispose();
        }

        public void ProcessZeroRowsAffectedException(IDictionary<string, object> whereConditions, string tableName)
        {
            // throw exception for zero effected rows
            var conditions = whereConditions.Aggregate(string.Empty,
                (current, item) => current + $"{item.Key}: {item.Value}, ").TrimEnd(',', ' ');
            throw new Exception(
                $"An update or delete statement produced zero affected rows for table name : {tableName}  where conditions : {conditions}");
        }

        public static string GetRawSqlQuery(string sql, IDictionary<string, object> parameters) => GetRawSqlQuery(PrepareCommandObject(sql, parameters));

        public static string GetRawSqlQuery(IDbCommand command)
        {
            var query = command.CommandText;

            foreach (MySqlParameter p in command.Parameters)
            {
                string value;
                if (p.Value is string)
                {
                    value = "'" + MySqlHelper.EscapeString(p.Value.ToString()) + "'";
                }
                else if (p.Value is System.Enum)
                {
                    value = Convert.ToInt32(p.Value).ToString();
                }
                else
                {
                    value = p.Value.ToString();
                }

                query = query.Replace(p.ParameterName, value);
            }

            return query;
        }

        public void SetShouldCheckQueryLevel(bool b)
        {
            m_shouldCheckQueryLevel = b;
        }

        public void SetShouldTrackQueries(bool b)
        {
            m_shouldTrackQueries = b;
        }

        public void SetDetectLiteralsInQueries(bool b)
        {
            m_detectLiteralsInQueries = b;
        }

        public void SetShouldCompareParametersWithSql(bool b)
        {
            _shouldCompareParametersWithSql = b;
        }

        public void SetShouldDetectDateFunctionsInQueries(bool b)
        {
            m_shouldDetectDateFunctionsInQueries = b;
        }
    }
}
