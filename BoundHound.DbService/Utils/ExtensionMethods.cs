﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace BoundHound.DbService.Utils
{
    public static class ExtensionMethods
    {
        /// <summary>
        /// Get underlying element type for enumerables. Non-enumerables will return null. String will return null instead of char.
        /// 
        /// Some of the original logic came from http://benohead.com/c-get-element-type-enumerable-reflection/
        /// </summary>
        /// <param name="o"></param>
        /// <returns></returns>
        public static Type GetElementType(this object o)
        {
            // Even though string can be treated as IEnumerable<char>, we want
            // to treat this as a non-enumerable type when getting the element type
            if (o is string)
            {
                return null;
            }

            var enumerable = o as IEnumerable;
            if (enumerable == null)
            {
                return null;
            }

            var interfaces = enumerable.GetType().GetInterfaces();
            var elementType = interfaces
                .Where(i => i.IsGenericType && i.GetGenericTypeDefinition() == typeof(IEnumerable<>))
                .Select(i => i.GetGenericArguments()[0]).FirstOrDefault();

            // Peek at the first element in the list if we couldn't determine the element type
            if (elementType == null || elementType == typeof(object))
            {
                var firstElement = enumerable.Cast<object>().FirstOrDefault();
                if (firstElement != null)
                {
                    elementType = firstElement.GetType();
                }
            }
            return elementType;
        }

        public static HashSet<T> ToHashSet<T>(this IEnumerable<T> source)
        {
            return new HashSet<T>(source);
        }
    }
}
