﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace BoundHound.DbService.Utils
{
    public struct HashCode
    {
        private readonly int m_hashCode;
        private const int SeedHashCode = 17;
        private const int PrimeMultiplier = 31;

        public static HashCode Start => new HashCode(SeedHashCode);

        public HashCode(int hashCode)
        {
            this.m_hashCode = hashCode;
        }

        public static implicit operator int(HashCode hashCode)
        {
            return hashCode.GetHashCode();
        }

        public HashCode Hash<T>(T obj)
        {
            return new HashCode((EqualityComparer<T>.Default.Equals(obj, default(T)) ? 0 : obj.GetHashCode()) + this.m_hashCode * PrimeMultiplier);
        }

        public override int GetHashCode()
        {
            return this.m_hashCode;
        }
    }
}
