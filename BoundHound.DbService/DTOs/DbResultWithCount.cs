﻿using BoundHound.DbService.Utils;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace BoundHound.DbService.DTOs
{
    public class DbResultWithCount<T>
    {
        public DbResultWithCount(IEnumerable<T> results, ulong count)
        {
            Results = results;
            Count = count;
        }

        public ulong Count { get; }
        public IEnumerable<T> Results { get; }

        public override bool Equals(object obj)
        {
            var other = obj as DbResultWithCount<T>;
            if (other == null)
            {
                return false;
            }

            var isEnumerableSetEqual = new HashSet<T>(other.Results).SetEquals(Results);

            return Count == other.Count && isEnumerableSetEqual;
        }

        public override int GetHashCode()
        {
            var hashCode = HashCode.Start
                .Hash(Count)
                .Hash(Results);

            return hashCode;
        }
    }
}
