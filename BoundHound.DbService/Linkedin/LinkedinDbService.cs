﻿using BoundHound.DbService.EmailList;
using BoundHound.DbService.Interfaces;
using BoundHound.Linkedin;
using BoundHound.Model;
using Dapper;
using MySql.Data.MySqlClient;
using System;
using System.Collections.Generic;
using System.Configuration;
using System.Data;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace BoundHound.DbService.Linkedin
{
    public class LinkedinDbService : ILinkedInDbService
    {
        //= new MySqlConnection();
        //string con = ConfigurationManager.AppSettings["BoundHoundConnection"];
        private readonly IDbConnection _db;
        string connection = ConfigurationManager.ConnectionStrings["LinkedInConnection"].ConnectionString;
        public LinkedinDbService()
        {
            _db = new MySqlConnection(ConfigurationManager.ConnectionStrings["LinkedInConnection"].ConnectionString);
        }


        public IEnumerable<EmailDTO> GetConnections(int noOfConnections, UserInfo MyUserInfo)
        {
            LinkedinData mLinkedinData = new LinkedinData();
            return mLinkedinData.GetConnections(noOfConnections, MyUserInfo);
        }

        public IEnumerable<EmailDTO> GetConnectionEmailByProfileId(string ProfileURL, UserInfo MyUserInfo)
        {
            LinkedinData mLinkedinData = new LinkedinData();
            return  mLinkedinData.GetConnectionEmailByProfileId(ProfileURL, MyUserInfo);
        }

        public IEnumerable<EmailDTO> GetConnectionEmailByProfileList(IEnumerable<EmailDTO> _EmailsList, UserInfo MyUserInfo)
        {
            LinkedinData mLinkedinData = new LinkedinData();
            return  mLinkedinData.GetConnectionEmailByProfileList(_EmailsList,MyUserInfo);
        }

        public IEnumerable<EmailDTO> GetFromLinkedin(int noOfConnections, UserInfo MyUserInfo)
        {
            LinkedinData mLinkedinData = new LinkedinData();
            IEnumerable<EmailDTO> m_emaillist;
            //m_emaillist = mLinkedinData.GetEmailList(noOfConnections,MyUserInfo);
            m_emaillist = mLinkedinData.GetConnections(noOfConnections, MyUserInfo);
            foreach (EmailDTO em in m_emaillist)
            {
                IEnumerable<EmailDTO> emails = _db.Query<EmailDTO>("SELECT * FROM emaillist WHERE profileid = @profileid", new { profileid = em.profileid }).ToList();
                if (emails.Count() <= 0)
                {
                    int rowsAffected = _db.Execute(@"INSERT emaillist(firstname,lastname,email,phone,mobile,title,company,website,location,Url,photourl,profileid,lastupdate,tenantId,userId,listId) values (@firstname, @lastname, @email,@phone,@mobile,@title,@company,@website,@location,@Url,@photourl,@profileid,@lastupdate,@tenantId,@userId,@listId)",
                    new { firstname = em.firstname, lastname = em.lastname, email = em.email, phone = em.phone, mobile = em.mobile, title = em.title, company = em.company, website = em.website, location = em.location, Url = em.Url, photourl = em.photourl, profileid = em.profileid, lastupdate = DateTime.Now, tenantId = 1, userId = 1 ,listId=1});
                }
                else
                {
                    IEnumerable<EmailDTO> emailexist = _db.Query<EmailDTO>("SELECT * FROM emaillist WHERE profileid = @profileid and listId=1", new { profileid = em.profileid }).ToList();
                    if (emailexist.Count() <= 0)
                    {
                        int rowsAffected = _db.Execute(@"INSERT emaillist(firstname,lastname,email,phone,mobile,title,company,website,location,Url,photourl,profileid,lastupdate,tenantId,userId,listId) values (@firstname, @lastname, @email,@phone,@mobile,@title,@company,@website,@location,@Url,@photourl,@profileid,@lastupdate,@tenantId,@userId,@listId)",
                        new { firstname = em.firstname, lastname = em.lastname, email = em.email, phone = em.phone, mobile = em.mobile, title = em.title, company = em.company, website = em.website, location = em.location, Url = em.Url, photourl = em.photourl, profileid = em.profileid, lastupdate = DateTime.Now, tenantId = 1, userId = 1, listId = 1 });
                    }

                }

            }
            return m_emaillist;
        }

        public IEnumerable<EmailDTO> GetFromLinkedInWithEmails(int noOfConnections, UserInfo MyUserInfo)
        {
            LinkedinData mLinkedinData = new LinkedinData();
            IEnumerable<EmailDTO> m_emaillist;
            m_emaillist = mLinkedinData.GetEmailList(noOfConnections,MyUserInfo);
            foreach (EmailDTO em in m_emaillist)
            {
                IEnumerable<EmailDTO> emails = _db.Query<EmailDTO>("SELECT * FROM emaillist WHERE profileid = @profileid", new { profileid = em.profileid }).ToList();
                if (emails.Count() <= 0)
                {
                    int rowsAffected = _db.Execute(@"INSERT emaillist(firstname,lastname,email,phone,mobile,title,company,website,location,Url,photourl,profileid,lastupdate,tenantId,userId) values (@firstname, @lastname, @email,@phone,@mobile,@title,@company,@website,@location,@Url,@photourl,@profileid,@lastupdate,@tenantId,@userId)",
                    new { firstname = em.firstname, lastname = em.lastname, email = em.email, phone = em.phone, mobile = em.mobile, title = em.title, company = em.company, website = em.website, location = em.location, Url = em.Url, photourl = em.photourl, profileid = em.profileid, lastupdate = DateTime.Now, tenantId = 1, userId = 1 });
                }
                else
                {
                    IEnumerable<EmailDTO> emailexist = _db.Query<EmailDTO>("SELECT * FROM emaillist WHERE profileid = @profileid and listId=0", new { profileid = em.profileid }).ToList();
                    if (emailexist.Count() <= 0)
                    {
                        int rowsAffected = _db.Execute(@"INSERT emaillist(firstname,lastname,email,phone,mobile,title,company,website,location,Url,photourl,profileid,lastupdate,tenantId,userId,listId) values (@firstname, @lastname, @email,@phone,@mobile,@title,@company,@website,@location,@Url,@photourl,@profileid,@lastupdate,@tenantId,@userId,@listId)",
                        new { firstname = em.firstname, lastname = em.lastname, email = em.email, phone = em.phone, mobile = em.mobile, title = em.title, company = em.company, website = em.website, location = em.location, Url = em.Url, photourl = em.photourl, profileid = em.profileid, lastupdate = DateTime.Now, tenantId = 1, userId = 1, listId = 0 });
                    }

                }

            }
            return m_emaillist;
        }

        public bool SendMessage(string ProfileURL, string Message, UserInfo MyUserInfo)
        {
            EmailDTO mEmailDTO = new EmailDTO();
            LinkedinData mLinkedinData = new LinkedinData();

            return mLinkedinData.SendMessage(ProfileURL, Message, MyUserInfo);

        }

        public IEnumerable<EmailDTO> SendInvitations(string SearchStr, string ConnectionType, UserInfo MyUserInfo)
        {
            IEnumerable<EmailDTO> m_emaillist;
            LinkedinData mLinkedinData = new LinkedinData();
            m_emaillist=mLinkedinData.SendInvitations(SearchStr, ConnectionType, MyUserInfo);

            foreach (EmailDTO em in m_emaillist)
            {
                IEnumerable<EmailDTO> emails = _db.Query<EmailDTO>("SELECT * FROM emaillist WHERE profileid = @profileid and typeId=1", new { profileid = em.profileid }).ToList();
                if (emails.Count() <= 0)
                {
                    int rowsAffected = _db.Execute(@"INSERT emaillist(firstname,lastname,email,phone,mobile,title,company,website,location,Url,photourl,profileid,lastupdate,tenantId,userId,listId,typeId) values (@firstname, @lastname, @email,@phone,@mobile,@title,@company,@website,@location,@Url,@photourl,@profileid,@lastupdate,@tenantId,@userId,@listId,@typeId)",
                    new { firstname = em.firstname, lastname = em.lastname, email = em.email, phone = em.phone, mobile = em.mobile, title = em.title, company = em.company, website = em.website, location = em.location, Url = em.Url, photourl = em.photourl, profileid = em.profileid, lastupdate = DateTime.Now, tenantId = 1, userId = 1, listId = 1 ,typeId=1});
                }
                else
                {

                    int rowsAffected = _db.Execute(@"INSERT emaillist(firstname,lastname,email,phone,mobile,title,company,website,location,Url,photourl,profileid,lastupdate,tenantId,userId,listId,typeId) values (@firstname, @lastname, @email,@phone,@mobile,@title,@company,@website,@location,@Url,@photourl,@profileid,@lastupdate,@tenantId,@userId,@listId,typeId)",
                    new { firstname = em.firstname, lastname = em.lastname, email = em.email, phone = em.phone, mobile = em.mobile, title = em.title, company = em.company, website = em.website, location = em.location, Url = em.Url, photourl = em.photourl, profileid = em.profileid, lastupdate = DateTime.Now, tenantId = 1, userId = 1, listId = 1, typeId = 1 });

                    //IEnumerable<EmailDTO> emailexist = _db.Query<EmailDTO>("SELECT * FROM emaillist WHERE listId=1 and typeId=1").ToList();
                    //if (emailexist.Count() <= 0)
                    //{
                    //    int rowsAffected = _db.Execute(@"INSERT emaillist(firstname,lastname,email,phone,mobile,title,company,website,location,Url,photourl,profileid,lastupdate,tenantId,userId,listId,typeId) values (@firstname, @lastname, @email,@phone,@mobile,@title,@company,@website,@location,@Url,@photourl,@profileid,@lastupdate,@tenantId,@userId,@listId,typeId)",
                    //    new { firstname = em.firstname, lastname = em.lastname, email = em.email, phone = em.phone, mobile = em.mobile, title = em.title, company = em.company, website = em.website, location = em.location, Url = em.Url, photourl = em.photourl, profileid = em.profileid, lastupdate = DateTime.Now, tenantId = 1, userId = 1, listId = 1,typeId=1 });
                    //}

                }

            }

            return m_emaillist;
        }


        public IEnumerable<SendInvitationDTO> ClickSendInvitaion(List<String> ProfileURl, string[] connectionid, string Message, UserInfo MyUserInfo)
        {
            IEnumerable<SendInvitationDTO> m_emaillist;
            LinkedinData mLinkedinData = new LinkedinData();
            m_emaillist = mLinkedinData.ClickSendInvitaion(ProfileURl, connectionid, Message, MyUserInfo);
            foreach (SendInvitationDTO em in m_emaillist)
            {
                if (em.Message != "")
                {
                    int rowsAffected = _db.Execute(@"INSERT sentinvitations(connectionId,Message,lastUpdate,userId,isActive) values (@connectionId,@Message,@lastUpdate,@userId,@isActive)",
                        new { connectionId = em.connectionid, Message = em.Message, lastUpdate = em.lastUpdate, userId = em.userId, isActive = em.isActive });
                }
            }
            return m_emaillist;
        }

        public IEnumerable<SendInvitationDTO> FindStatusofInvitationById(List<String> ProfileURl, string[] connectionid, string Message, UserInfo MyUserInfo)
        {
            IEnumerable<SendInvitationDTO> m_emaillist;
            LinkedinData mLinkedinData = new LinkedinData();
            m_emaillist = mLinkedinData.FindStatusofInvitationById(ProfileURl, connectionid, Message, MyUserInfo);
            
            return m_emaillist;
        }
        
    }
}
