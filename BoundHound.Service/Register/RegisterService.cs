﻿using BoundHound.Business.Linkedin;
using BoundHound.Model;
using BoundHound.Service.Interfaces;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace BoundHound.Service.Register
{
    public class RegisterService : IRegisterService
    {
        public ResultReponse RegisterForm(UserDTO RegisterInfo)
        {
            try
            {
                RegisterBusiness m_RegisterBusiness = new RegisterBusiness();

                return m_RegisterBusiness.RegisterForm(RegisterInfo);

            }
            catch (Exception exp)
            {
                throw;
            }
        }

       
    }
}
