﻿using BoundHound.Model;
using System;
using System.Collections.Generic;
using System.Text;

namespace BoundHound.Service.Interfaces
{
    public interface IProfileService
    {
        Profile GetProfile();
    }
}
