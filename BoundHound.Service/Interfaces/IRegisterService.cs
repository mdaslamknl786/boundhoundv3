﻿using BoundHound.Model;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace BoundHound.Service.Interfaces
{
    public interface IRegisterService
    {
        ResultReponse RegisterForm(UserDTO RegisterInfo);
    }
}
