﻿using BoundHound.Model;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace BoundHound.Service.Interfaces
{
    public interface ILinkedinService
    {
        List<Emails> GetEmails(UserInfo MyUserInfo);
        List<Emails> GetConnections(UserInfo MyUserInfo);
        List<Emails> GetConnectionEmailByProfileId(string profileid, UserInfo MyUserInfo);
        List<Emails> GetConnectionEmailByProfileList(List<Emails> EmailsList, UserInfo MyUserInfo);
        List<Emails> GetFromLinkedin(int noOfConnections, UserInfo MyUserInfo);
        int UpdateEmail(Emails email, UserInfo MyUserInfo);
    }
    
}
