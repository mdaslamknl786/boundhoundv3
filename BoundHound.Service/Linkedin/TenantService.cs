﻿using BoundHound.Business.Linkedin;
using BoundHound.Model;
using BoundHound.Service.Interfaces;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace BoundHound.Service.Linkedin
{
    public class TenantService : ITenantService
    {
        public TenantDTO GetTenantInfo(int TenantId)
        {
            TenantDTO TenantInfo = new TenantDTO();
            TenantBusiness m_TenantBusiness = new TenantBusiness();
            TenantInfo = m_TenantBusiness.GetTenantInfo(TenantId);
            return TenantInfo;
        }
    }
}
