﻿using BoundHound.Model;
using BoundHound.Service.Interfaces;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using BoundHound.Business;
using BoundHound.Business.Linkedin;

namespace BoundHound.Service.Linkedin
{
    public class LinkedinService : ILinkedinService
    {
        public List<Emails> GetEmails(UserInfo MyUserInfo)
        {
            List<Emails> EmailsList = new List<Emails>();
            LinkedinBusiness m_LinkedinBusiness = new LinkedinBusiness();
            EmailsList = m_LinkedinBusiness.getem(MyUserInfo);
            return EmailsList;
        }

        public List<Emails> GetConnections(UserInfo MyUserInfo)
        {
            List<Emails> EmailsList = new List<Emails>();
            LinkedinBusiness m_LinkedinBusiness = new LinkedinBusiness();
            EmailsList = m_LinkedinBusiness.GetConnections(MyUserInfo);
            return EmailsList;
        }

        public List<Emails> GetConnectionEmailByProfileId(string profileid, UserInfo MyUserInfo)
        {
            List<Emails> EmailsList = new List<Emails>();
            LinkedinBusiness m_LinkedinBusiness = new LinkedinBusiness();
            EmailsList = m_LinkedinBusiness.GetConnectionEmailByProfileId(profileid,MyUserInfo);
            return EmailsList;
        }

        public List<Emails> GetConnectionEmailByProfileList(List<Emails> _EmailsList, UserInfo MyUserInfo)
        {
            List<Emails> EmailsList = new List<Emails>();
            LinkedinBusiness m_LinkedinBusiness = new LinkedinBusiness();
            EmailsList = m_LinkedinBusiness.GetConnectionEmailByProfileList(_EmailsList,MyUserInfo);
            return EmailsList;
        }

        public List<Emails> GetFromLinkedin(int noOfConnections, UserInfo MyUserInfo)
        {
            List<Emails> EmailsList = new List<Emails>();
            LinkedinBusiness m_LinkedinBusiness = new LinkedinBusiness();

            EmailsList = m_LinkedinBusiness.GetFromLinkedin(noOfConnections,MyUserInfo);

            return EmailsList;
        }

        public int UpdateEmail(Emails email, UserInfo MyUserInfo)
        {
            int returnValue = 0;
            LinkedinBusiness m_LinkedinBusiness = new LinkedinBusiness();

            returnValue = m_LinkedinBusiness.UpdateEmail(email,MyUserInfo);

            return returnValue;
        }
    }
}
