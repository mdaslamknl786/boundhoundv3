﻿using BoundHound.Model;
using BoundHound.Service.Interfaces;
using System;
using System.Collections.Generic;
using System.Text;

namespace BoundHound.Service
{
    public class ProfileService : IProfileService
    {
        public Profile GetProfile()
        {
            Profile ProfileInfo = new Profile();

            ProfileInfo.Id = 1;
            ProfileInfo.ProfileID = 1;
            ProfileInfo.FirstName = "Mohammed";
            ProfileInfo.LastName = "Aslam";
            ProfileInfo.URL = "http:\\wwww.mdaslam.com\\pic";

            return ProfileInfo;
        }
    }
}
