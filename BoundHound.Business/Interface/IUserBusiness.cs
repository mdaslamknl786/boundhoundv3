﻿using BoundHound.Model;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace BoundHound.Business.Interface
{
    public interface IUserBusiness
    {
        UserInfo Authenticate(string UserName, string Password);

        UserInfo TokenValidation(string Email, int UserId, int TenantId);
    }
}
