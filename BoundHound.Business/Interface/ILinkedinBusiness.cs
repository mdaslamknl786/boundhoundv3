﻿using BoundHound.Model;
using System;
using System.Collections.Generic;
using System.Text;

namespace BoundHound.Business.Interface
{
    public interface ILinkedinBusiness
    {
        IEnumerable<EmailDTO> GetConnections(int noOfConnections, UserInfo MyUserInfo);
        IEnumerable<EmailDTO> GetConnectionEmailByProfileId(string Id, UserInfo MyUserInfo);
        IEnumerable<EmailDTO> GetConnectionEmailByProfileList(IEnumerable<EmailDTO> EmailsList, UserInfo MyUserInfo);
        IEnumerable<EmailDTO> GetFromLinkedin(int noOfConnections, UserInfo MyUserInfo);
        IEnumerable<EmailDTO> GetFromLinkedInWithEmails(int noOfConnections, UserInfo MyUserInfo);
        bool SendMessage(string ProfileURL, string Message, UserInfo MyUserInfo);
        IEnumerable<EmailDTO> SendInvitations(string SearchStr, string ConnectionType, UserInfo MyUserInfo);

        IEnumerable<SendInvitationDTO> ClickSendInvitaion(string Id, UserInfo MyUserInfo);

        IEnumerable<SendInvitationDTO> FindStatusofInvitationById(string Id, UserInfo MyUserInfo);
    }
}
