﻿using BoundHound.Model;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace BoundHound.Business.Interface
{
    public interface IEmailListBusiness
    {
        IEnumerable<EmailDTO> GetEmailList(UserInfo MyUserInfo,int listId);
        int UpdateEmail(EmailDTO email, UserInfo MyUserInfo);
        IEnumerable<EmailDTO> GetConnectionByProfileId(string Id,UserInfo MyUserInfo);
        IEnumerable<ListDTO> GetEmailListByListId(UserInfo MyUserInfo, int listId);
        IEnumerable<EmailDTO> GetEmailInfoById(string Id);
        int UpdateConnectionEmail(int Id, string email);
        IEnumerable<EmailDTO> GetEmailprofileInfoById(string Id);
    }
}
