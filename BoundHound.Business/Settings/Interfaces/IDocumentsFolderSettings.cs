﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace BoundHound.Business.Settings.Interfaces
{
    public interface IDocumentsFolderSettings
    {
        string GetInitialDocumentsFolderPath { get; }
        string GetImportsDocumentsFolderPath { get; }
    }
}
