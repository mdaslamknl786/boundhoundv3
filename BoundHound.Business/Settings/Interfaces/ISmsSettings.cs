﻿namespace BoundHound.Business.Settings.Interfaces
{
    public interface ISmsSettings
    {
        string RetrieveSmsBaseUrl { get; }

        string RetrieveSmsAuthToken { get; }

        string RetrieveSmsSenderAddress { get; }
    }
}
