﻿namespace BoundHound.Business.Settings.Interfaces
{
    public interface IEmailSetting
    {
        string SmtpHost { get; }
        int SmtpPort { get; }
        bool SmtpRequiresSSL { get; }
        string SmtpUsername { get; }
        string SmtpPassword { get; }
        string EmailFromAddress { get; }
        string DoNotReplyFromAddress { get; }
        string NotificationEmailAddress { get; }
        string NotificationMode { get; }
        string NotificationTestAddress { get; }
        string ProductionErrorsAddress { get; }
    }
}
