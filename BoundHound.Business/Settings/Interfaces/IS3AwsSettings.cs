﻿//using Amazon;

namespace BoundHound.Business.Settings.Interfaces
{
    public interface IS3AwsSettings
    {
        string S3AWSAccessKey { get; }
        string S3AWSSecretKey { get; }
        string S3DocumentsFolderPrefix { get; }
        string S3BucketName { get; }
        //RegionEndpoint S3RegionEndpoint { get; }
    }
}
