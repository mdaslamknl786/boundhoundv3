﻿using System.Collections.Generic;

namespace BoundHound.Business.Settings.Interfaces
{
    public interface IConfigFileService
    {
        Dictionary<string, string> AppSettings { get; }
        string GetSharedItemsFolder { get; }
    }
}
