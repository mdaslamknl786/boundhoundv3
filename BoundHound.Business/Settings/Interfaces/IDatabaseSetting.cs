﻿namespace BoundHound.Business.Settings.Interfaces
{
    public interface IDatabaseSetting
    {
        short DefaultDatabaseId { get; }
    }
}
