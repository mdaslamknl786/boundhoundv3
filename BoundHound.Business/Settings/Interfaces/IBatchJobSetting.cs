﻿namespace BoundHound.Business.Settings.Interfaces
{
    public interface IBatchJobSetting
    {
        int BatchJobRunSize { get; }
        int BatchJobRunTimeInSeconds { get; }
        bool RunBatchJobsForSpecifiedDuration { get; }

        int NumberOfBatchJobsToProcessPerIteration { get; }
        bool AllowUseParallelForBatchJobs { get; }

    }
}
