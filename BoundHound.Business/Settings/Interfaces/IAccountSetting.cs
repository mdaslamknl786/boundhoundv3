﻿namespace BoundHound.Business.Settings.Interfaces
{
    public interface IAccountSetting
    {
        string ServerDomain { get; }
    }
}
