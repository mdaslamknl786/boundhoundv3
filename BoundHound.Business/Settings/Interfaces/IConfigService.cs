﻿using System.Data;
using System.Collections.Generic;


namespace BoundHound.Business.Settings.Interfaces
{
    public interface IConfigService
    {
        bool RetrieveSettingAsBool(MasterConfigSettings.SettingType settingType);
        int RetrieveSettingAsInt(MasterConfigSettings.SettingType settingType);
        string RetrieveSetting(MasterConfigSettings.SettingType settingType);
        void DeleteSetting(MasterConfigSettings.SettingType settingType, IDbConnection conn);
        void InsertSetting(MasterConfigSettings.SettingType settingType, string settingValue, IDbConnection conn);
        MasterConfigSettingInfo GetDefaultSettingValue(MasterConfigSettings.SettingType settingType);
        IDictionary<MasterConfigSettings.SettingType, string> SelectAllSettings();
    }

    public struct MasterConfigSettingInfo
    {
        public MasterConfigSettings.SettingType Type { get; set; }
        public string Description { get; set; }
        public string Value { get; set; }
        public bool IsSensitive { get; set; }
    }
}
