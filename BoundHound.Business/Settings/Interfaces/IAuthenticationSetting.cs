﻿namespace BoundHound.Business.Settings.Interfaces
{
    public interface IAuthenticationSetting
    {
        string AuthTokenEncryptionKey { get; }
        string ApiClientKey { get; }
        bool UseSSL { get; }
    }
}
