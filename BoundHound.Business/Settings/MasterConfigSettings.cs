﻿using BoundHound.Business.Settings.Interfaces;
using BoundHound.DbService.Interfaces;
using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace BoundHound.Business.Settings
{
    public class MasterConfigSettings : IConfigService, IEmailSetting, IBatchJobSetting, IDatabaseSetting, ISmsSettings, IAuthenticationSetting,
                                        IDocumentsFolderSettings, IAccountSetting, IS3AwsSettings
    {
        public enum SettingType
        {
            Unused = 0,
            SmtpHost = 100,
            SmtpPort = 101,
            SmtpRequiresSSL = 102,
            SmtpUsername = 103,
            SmtpPassword = 104,
            EmailNotificationMode = 200,
            EmailFromAddress = 201,
            DoNotReplyFromAddress = 202,
            NotificationEmailAddress = 203,
            NotificationMode = 204,
            NotificationTestAddress = 205,
            ProductionErrorsAddress = 206,
            ToAddressForTestEmails = 207,
            BatchJobRunSize = 300,
            RunBatchJobsForSpecifiedDuration = 301,
            BatchJobRunTimeInSeconds = 302,
            NumberOfBatchJobsToProcessPerIteration = 303,
            AllowUseParallelForBatchJobs = 304,
            DefaultDatabaseId = 400,
            SmsBaseUrl = 500,
            SmsAuthKey = 501,
            SmsSenderId = 502,
            InitialDocumentsFolderPath = 600,
            AuthTokenEncryptionKey = 700,
            ApiClientKey = 800,
            UseSSL = 801,
            ServerDomain = 901,
            S3AWSAccessKey = 1000,
            S3AWSSecretKey = 1001,
            S3DocumentsFolderPrefix = 1002,
            S3BucketName = 1003,
            S3RegionEndpoint = 1004
        }

        private readonly IDatabaseService m_dbService;
        private readonly IConfigFileService m_configFileService;


        public MasterConfigSettings(IDatabaseService dbService, IConfigFileService configFileService)
        {
            m_dbService = dbService;
            m_configFileService = configFileService;
        }

        //public MasterConfigSettings(IDatabaseService dbService, IConfigFileService configFileService)
        //{
        //    m_dbService = dbService;
        //    m_configFileService = configFileService;
        //}

        public bool RetrieveSettingAsBool(SettingType settingType)
        {
            var value = RetrieveSetting(settingType).Trim();
            return "true".Equals(value, StringComparison.OrdinalIgnoreCase)
                   || "yes".Equals(value, StringComparison.OrdinalIgnoreCase)
                   || "1".Equals(value, StringComparison.OrdinalIgnoreCase);
        }

        public int RetrieveSettingAsInt(SettingType settingType)
        {
            return Convert.ToInt32(RetrieveSetting(settingType));
        }

        public string RetrieveSetting(SettingType settingType)
        {
            var value = RetrieveSettingValueFromConfigFile(settingType);
            if (value != null)
            {
                return value;
            }

            var setting = RetrieveSettingValueFromDb(settingType);

            var settingValue = !string.IsNullOrWhiteSpace(setting)
                ? setting
                : GetDefaultSettingValue(settingType).Value;

            return settingValue;
        }

        public void DeleteSetting(SettingType settingType, IDbConnection conn)
        {
            //const string sql = "DELETE FROM masterconfigsettings WHERE id = ?id";
            //var values = new Dictionary<string, object>
            //{
            //    {"id", (int) settingType}
            //};

            //conn.Execute(sql, values);
        }

        public void InsertSetting(SettingType settingType, string settingValue, IDbConnection conn)
        {
            const string sql = "INSERT IGNORE INTO masterconfigsettings (id, settingValue) VALUES (?id, ?settingValue) ON DUPLICATE KEY UPDATE settingValue = ?settingValue";
            var values = new Dictionary<string, object>
            {
                {"id", (int) settingType},
                {"settingValue", settingValue}
            };

            //conn.Execute(sql, values);
        }

        public MasterConfigSettingInfo GetDefaultSettingValue(SettingType settingType)
        {
            switch (settingType)
            {
                case SettingType.SmtpHost:
                    return new MasterConfigSettingInfo
                    {
                        Type = SettingType.SmtpHost,
                        Description = "The machine running an SMTP service for sending out email.",
                        Value = "smtp.sendgrid.net",
                        IsSensitive = false
                    };
                case SettingType.SmtpPort:
                    return new MasterConfigSettingInfo
                    {
                        Type = SettingType.SmtpPort,
                        Description = "The port number where the SMTP service is running.",
                        Value = "587",
                        IsSensitive = false
                    };
                case SettingType.SmtpRequiresSSL:
                    return new MasterConfigSettingInfo
                    {
                        Type = SettingType.SmtpRequiresSSL,
                        Description = "Specifies whether SSL is used to access the specified SMTP mail server.",
                        Value = "true",
                        IsSensitive = false
                    };
                case SettingType.SmtpUsername:
                    return new MasterConfigSettingInfo
                    {
                        Type = SettingType.SmtpUsername,
                        Description = "The username used for accessing the SMTP service.",
                        Value = "patasala.in",
                        IsSensitive = false
                    };
                case SettingType.SmtpPassword:
                    return new MasterConfigSettingInfo
                    {
                        Type = SettingType.SmtpPassword,
                        Description = "The password used for accessing the SMTP service.",
                        Value = "patasala12",
                        IsSensitive = true
                    };
                case SettingType.EmailFromAddress:
                    return new MasterConfigSettingInfo
                    {
                        Type = SettingType.EmailFromAddress,
                        Description = "Specifies email from address.",
                        Value = "staging@patasala.in",
                        IsSensitive = false
                    };
                case SettingType.DoNotReplyFromAddress:
                    return new MasterConfigSettingInfo
                    {
                        Type = SettingType.DoNotReplyFromAddress,
                        Description = "Specifies email from address.",
                        Value = "donotreply@patasala.in",
                        IsSensitive = false
                    };
                case SettingType.NotificationEmailAddress:
                    return new MasterConfigSettingInfo
                    {
                        Type = SettingType.NotificationEmailAddress,
                        Description = "Specifies email notification address.",
                        Value = "admin@patasala.in",
                        IsSensitive = false
                    };
                case SettingType.ToAddressForTestEmails:
                    return new MasterConfigSettingInfo
                    {
                        Type = SettingType.ToAddressForTestEmails,
                        Description = "Email address for test emails.",
                        Value = "",
                        IsSensitive = false
                    };
                case SettingType.EmailNotificationMode:
                    return new MasterConfigSettingInfo
                    {
                        Type = SettingType.EmailNotificationMode,
                        Description = "Email notification mode",
                        Value = "",
                        IsSensitive = true
                    };
                case SettingType.NotificationTestAddress:
                    return new MasterConfigSettingInfo
                    {
                        Type = SettingType.NotificationTestAddress,
                        Description = "Notifications test email to address",
                        Value = "staging@patasala.in",
                        IsSensitive = false
                    };
                case SettingType.ProductionErrorsAddress:
                    return new MasterConfigSettingInfo
                    {
                        Type = SettingType.ProductionErrorsAddress,
                        Description = "Production error notification email address",
                        Value = "staging@patasala.in",
                        IsSensitive = false
                    };
                case SettingType.BatchJobRunSize:
                    return new MasterConfigSettingInfo
                    {
                        Type = SettingType.BatchJobRunSize,
                        Description = "Number of batch jobs execution per batch",
                        Value = "100",
                        IsSensitive = false
                    };

                case SettingType.BatchJobRunTimeInSeconds:
                    return new MasterConfigSettingInfo
                    {
                        Type = SettingType.BatchJobRunTimeInSeconds,
                        Description = "Batch jobs run time size",
                        Value = "300",
                        IsSensitive = false
                    };

                case SettingType.RunBatchJobsForSpecifiedDuration:
                    return new MasterConfigSettingInfo
                    {
                        Type = SettingType.RunBatchJobsForSpecifiedDuration,
                        Description = "Run Batch Jobs For Specified Duration",
                        Value = "false",
                        IsSensitive = false
                    };
                case SettingType.NumberOfBatchJobsToProcessPerIteration:
                    return new MasterConfigSettingInfo
                    {
                        Type = SettingType.NumberOfBatchJobsToProcessPerIteration,
                        Description = "Number Of Batch Jobs To Process Per Iteration",
                        Value = "50",
                        IsSensitive = false
                    };
                case SettingType.DefaultDatabaseId:
                    return new MasterConfigSettingInfo
                    {
                        Type = SettingType.DefaultDatabaseId,
                        Description = "Default database for school account",
                        Value = "1",
                        IsSensitive = false
                    };
                case SettingType.SmsBaseUrl:
                    return new MasterConfigSettingInfo
                    {
                        Type = SettingType.SmsBaseUrl,
                        Description = "Default url to send SMS",
                        Value = "https://control.msg91.com/api/postsms.php",
                        IsSensitive = false
                    };
                case SettingType.SmsAuthKey:
                    return new MasterConfigSettingInfo
                    {
                        Type = SettingType.SmsAuthKey,
                        Description = "Authentication Key used to send SMS",
                        Value = "180194AKC5zxEBZ59eb05dd",
                        IsSensitive = true
                    };
                case SettingType.SmsSenderId:
                    return new MasterConfigSettingInfo
                    {
                        Type = SettingType.SmsSenderId,
                        Description = "Sms sender name used to display as From address",
                        Value = "PatSal",
                        IsSensitive = false
                    };
                case SettingType.AuthTokenEncryptionKey:
                    return new MasterConfigSettingInfo
                    {
                        Type = SettingType.AuthTokenEncryptionKey,
                        Description = "Authenciation token encryption key",
                        Value = "2c485a4c-c6c3-4582-bc3d-6177286c9e61",
                        IsSensitive = true
                    };
                case SettingType.ApiClientKey:
                    return new MasterConfigSettingInfo
                    {
                        Type = SettingType.ApiClientKey,
                        Description = "Api client key",
                        Value = "86d45234-01f4-41d0-a6c8-39cfee6079df",
                        IsSensitive = true
                    };
                case SettingType.UseSSL:
                    return new MasterConfigSettingInfo
                    {
                        Type = SettingType.UseSSL,
                        Description = "Api client key",
                        Value = "false",
                        IsSensitive = true
                    };
                case SettingType.InitialDocumentsFolderPath:
                    return new MasterConfigSettingInfo
                    {
                        Type = SettingType.InitialDocumentsFolderPath,
                        Description = "Initial path for documents path",
                        Value = "c://patasala//documents",
                        IsSensitive = false
                    };
                case SettingType.ServerDomain:
                    return new MasterConfigSettingInfo
                    {
                        Type = SettingType.ServerDomain,
                        Description = "default domain for environment",
                        Value = ".patasala.co",
                        IsSensitive = false
                    };
                case SettingType.S3AWSAccessKey:
                    return new MasterConfigSettingInfo
                    {
                        Type = SettingType.S3AWSAccessKey,
                        Description = "Access key for S3 document storage",
                        Value = "AKIAJGYZMCSPRAMA3YTA",
                        IsSensitive = true
                    };
                case SettingType.S3AWSSecretKey:
                    return new MasterConfigSettingInfo
                    {
                        Type = SettingType.S3AWSSecretKey,
                        Description = "Secret key for S3 document storage",
                        Value = "l+KtbTZQBAFH8yR57/Qlv+ijICxYmiQTrJidRR1o",
                        IsSensitive = true
                    };
                case SettingType.S3DocumentsFolderPrefix:
                    return new MasterConfigSettingInfo
                    {
                        Type = SettingType.S3DocumentsFolderPrefix,
                        Description = "Default location for storing documents in the S3 bucket",
                        Value = "Documents/",
                        IsSensitive = false
                    };
                case SettingType.S3BucketName:
                    return new MasterConfigSettingInfo
                    {
                        Type = SettingType.S3BucketName,
                        Description = "Name of S3 bucket where files are stored",
                        Value = "dev-patasala",
                        IsSensitive = true
                    };
                //case SettingType.S3RegionEndpoint:
                //    return new MasterConfigSettingInfo
                //    {
                //        Type = SettingType.S3RegionEndpoint,
                //        Description = "RegionEndpoint for the S3 bucket",
                //        Value = RegionEndpoint.USEast1.SystemName,
                //        IsSensitive = false
                //    };
                default:
                    return new MasterConfigSettingInfo
                    {
                        Type = SettingType.Unused,
                        Description = string.Empty,
                        Value = string.Empty,
                        IsSensitive = false
                    };
            }
        }


        #region Helper functions

        /// <summary>
        /// Retrieve certain values from the database and never from the cache
        /// </summary>
        /// <returns></returns>
        private string RetrieveSettingValueFromDb(SettingType settingType)
        {
            //const string sql = "SELECT id, settingValue FROM masterconfigsettings WHERE id = @Id";

            //var data = m_dbService.GetMasterDbReadConnection().Query(sql, new { Id = (int)settingType }).SingleOrDefault();

            //return data == null ? GetDefaultSettingValue(settingType).Value : data.settingValue;
            return "";
        }

        private string RetrieveSettingValueFromConfigFile(SettingType settingType)
        {
            var key = settingType.ToString();

            var appSettings = m_configFileService.AppSettings;
            return appSettings.ContainsKey(key) ? appSettings[key] : null;
        }

        public IDictionary<SettingType, string> SelectAllSettings()
        {

            IDictionary<SettingType, string> settings;


            settings = new Dictionary<SettingType, string>();
            const string sql = "SELECT id, settingValue FROM masterconfigsettings";

            var data = m_dbService.ExecuteDataTable(m_dbService.GetMasterDbReadConnection(), sql, null);

            foreach (DataRow row in data.Rows)
            {
                var setting = row["settingValue"];
                if (setting != DBNull.Value)
                {
                    var id = (SettingType)Convert.ToInt32(row["id"]);
                    var settingValue = setting.ToString();
                    settings[id] = settingValue;
                }
            }

            return settings;
        }

       


        #endregion

        #region IEmailSettings

        public string SmtpHost => RetrieveSetting(SettingType.SmtpHost);

        public int SmtpPort => RetrieveSettingAsInt(SettingType.SmtpPort);

        public bool SmtpRequiresSSL => RetrieveSettingAsBool(SettingType.SmtpRequiresSSL);

        public string SmtpUsername => RetrieveSetting(SettingType.SmtpUsername);

        public string SmtpPassword => RetrieveSetting(SettingType.SmtpPassword);

        public string EmailNotificationMode => RetrieveSetting(SettingType.EmailNotificationMode);

        public string EmailFromAddress => RetrieveSetting(SettingType.EmailFromAddress);
        public string DoNotReplyFromAddress => RetrieveSetting(SettingType.DoNotReplyFromAddress);
        public string NotificationEmailAddress => RetrieveSetting(SettingType.NotificationEmailAddress);
        public string NotificationMode => RetrieveSetting(SettingType.NotificationMode);
        public string NotificationTestAddress => RetrieveSetting(SettingType.NotificationTestAddress);
        public string ProductionErrorsAddress => RetrieveSetting(SettingType.ProductionErrorsAddress);

        public string ToAddressForTestEmails => RetrieveSetting(SettingType.ToAddressForTestEmails);

        #endregion


        public int BatchJobRunSize => RetrieveSettingAsInt(SettingType.BatchJobRunSize);
        public int BatchJobRunTimeInSeconds => RetrieveSettingAsInt(SettingType.BatchJobRunTimeInSeconds);
        public bool RunBatchJobsForSpecifiedDuration => RetrieveSettingAsBool(SettingType.RunBatchJobsForSpecifiedDuration);
        public int NumberOfBatchJobsToProcessPerIteration => RetrieveSettingAsInt(SettingType.NumberOfBatchJobsToProcessPerIteration);
        public bool AllowUseParallelForBatchJobs => RetrieveSettingAsBool(SettingType.AllowUseParallelForBatchJobs);

        public short DefaultDatabaseId => (short)RetrieveSettingAsInt(SettingType.DefaultDatabaseId);

        #region ISmsSettings

        public string RetrieveSmsBaseUrl => RetrieveSetting(SettingType.SmsBaseUrl);
        public string RetrieveSmsAuthToken => RetrieveSetting(SettingType.SmsAuthKey);
        public string RetrieveSmsSenderAddress => RetrieveSetting(SettingType.SmsSenderId);

        #endregion

        #region Documents Folder settings
        public string GetInitialDocumentsFolderPath => RetrieveSetting(SettingType.InitialDocumentsFolderPath);
        public string GetImportsDocumentsFolderPath => System.IO.Path.Combine(GetInitialDocumentsFolderPath, "Import");

        #endregion

        #region AuthenticationSettings
        public string AuthTokenEncryptionKey => RetrieveSetting(SettingType.AuthTokenEncryptionKey);
        public string ApiClientKey => RetrieveSetting(SettingType.ApiClientKey);
        public bool UseSSL => RetrieveSettingAsBool(SettingType.UseSSL);

        #endregion

        #region S3AwsSettings

        public string S3AWSAccessKey => RetrieveSetting(SettingType.S3AWSAccessKey);
        public string S3AWSSecretKey => RetrieveSetting(SettingType.S3AWSSecretKey);
        public string S3DocumentsFolderPrefix => RetrieveSetting(SettingType.S3DocumentsFolderPrefix);
        public string S3BucketName => RetrieveSetting(SettingType.S3BucketName);

        //public RegionEndpoint S3RegionEndpoint => RegionEndpoint.GetBySystemName(RetrieveSetting(SettingType
        //    .S3RegionEndpoint));

        #endregion

        public string ServerDomain => RetrieveSetting(SettingType.ServerDomain);
    }
}
