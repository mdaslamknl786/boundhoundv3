﻿using BoundHound.Business.Interface;
using BoundHound.DbService.EmailList;
using BoundHound.DbService.Linkedin;
using BoundHound.Model;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace BoundHound.Business.Linkedin
{
    public class EmailListBusiness : IEmailListBusiness
    {


        public IEnumerable<EmailDTO> GetConnectionByProfileId(string Id, UserInfo MyUserInfo)
        {
            LinkedinBusiness mLinkedinBusiness = new LinkedinBusiness();
            return mLinkedinBusiness.GetConnectionEmailByProfileId(Id, MyUserInfo);
        }

        public IEnumerable<EmailDTO> GetEmailInfoById(string Id)
        {
            EmailListDbService m_EmailListDbService = new EmailListDbService();
            return m_EmailListDbService.GetEmailInfoById(Id);
        }


        public IEnumerable<EmailDTO> GetEmailList(UserInfo MyUserInfo,int listId)
        {
            EmailListDbService m_EmailListDbService = new EmailListDbService();
            return  m_EmailListDbService.GetEmailList(MyUserInfo, listId);
        }

        public IEnumerable<ListDTO> GetEmailListByListId(UserInfo MyUserInfo, int listId)
        {
            EmailListDbService m_EmailListDbService = new EmailListDbService();
            return m_EmailListDbService.GetEmailListByListId(MyUserInfo, listId);
        }

        public int UpdateConnectionEmail(int Id, string email)
        {
            int returnValue = 0;

            EmailListDbService m_EmailListDbService = new EmailListDbService();

            returnValue = m_EmailListDbService.UpdateConnectionEmail(Id,email);

            return returnValue;
        }

        public int UpdateEmail(EmailDTO email, UserInfo MyUserInfo)
        {
            int returnValue = 0;

            EmailListDbService m_EmailListDbService = new EmailListDbService();

            returnValue = m_EmailListDbService.UpdateEmail(email);

            return returnValue;
        }


        public IEnumerable<EmailDTO> GetEmailprofileInfoById(string Id)
        {
            EmailListDbService m_EmailListDbService = new EmailListDbService();
            return m_EmailListDbService.GetEmailprofileInfoById(Id);
        }
    }
}
