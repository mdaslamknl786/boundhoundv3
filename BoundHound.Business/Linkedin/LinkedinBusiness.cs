﻿using BoundHound.Business.Interface;
using BoundHound.Model;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using BoundHound.DbService.Linkedin;
namespace BoundHound.Business.Linkedin
{
    public class LinkedinBusiness : ILinkedinBusiness
    {

        
        public IEnumerable<EmailDTO> GetConnections(int noOfConnections, UserInfo MyUserInfo)
        {
            //TODO Get EmailDTO From DBService
            LinkedinDbService m_LinkedinDbService = new LinkedinDbService();
            return m_LinkedinDbService.GetConnections(noOfConnections, MyUserInfo);
        }

        public IEnumerable<EmailDTO> GetConnectionEmailByProfileId(string Id, UserInfo MyUserInfo)
        {
            //TODO Get EmailDTO From DBService
            string ProfileURL = "";

            List<EmailDTO> emailList = new List<EmailDTO>();
            EmailListBusiness memailListBusiness = new EmailListBusiness();
            IEnumerable<EmailDTO> emailinfo;
            emailinfo = memailListBusiness.GetEmailInfoById(Id);
            if(emailinfo !=null)
            {
                ProfileURL = emailinfo.FirstOrDefault().Url;
            }

            LinkedinDbService m_LinkedinDbService = new LinkedinDbService();
            return m_LinkedinDbService.GetConnectionEmailByProfileId(ProfileURL, MyUserInfo);

            //return emailinfo;
        }

        public IEnumerable<EmailDTO> GetConnectionEmailByProfileList(IEnumerable<EmailDTO> _EmailDTOList, UserInfo MyUserInfo)
        {
            //TODO Get EmailDTO From DBService
            LinkedinDbService m_LinkedinDbService = new LinkedinDbService();
            return  m_LinkedinDbService.GetConnectionEmailByProfileList(_EmailDTOList,MyUserInfo);
        }

        public IEnumerable<EmailDTO> GetFromLinkedin(int noOfConnections, UserInfo MyUserInfo)
        {
            //TODO Get EmailDTO From DBService
            LinkedinDbService m_LinkedinDbService = new LinkedinDbService();
            return m_LinkedinDbService.GetFromLinkedin(noOfConnections, MyUserInfo);
        }

        public bool SendMessage(string ProfileURL, string Message, UserInfo MyUserInfo)
        {
            //TODO Get EmailDTO From DBService
            LinkedinDbService m_LinkedinDbService = new LinkedinDbService();
            return m_LinkedinDbService.SendMessage(ProfileURL, Message, MyUserInfo);
        }

        public IEnumerable<EmailDTO> GetFromLinkedInWithEmails(int noOfConnections, UserInfo MyUserInfo)
        {
            LinkedinDbService m_LinkedinDbService = new LinkedinDbService();
            return m_LinkedinDbService.GetFromLinkedInWithEmails(noOfConnections, MyUserInfo);

        }

        public IEnumerable<EmailDTO> SendInvitations(string SearchStr, string ConnectionType, UserInfo MyUserInfo)
        {
            LinkedinDbService m_LinkedinDbService = new LinkedinDbService();
            return m_LinkedinDbService.SendInvitations(SearchStr, ConnectionType, MyUserInfo);
        }


        public IEnumerable<SendInvitationDTO> ClickSendInvitaion(string Id, UserInfo MyUserInfo)
        {
            List<string> ProfileURL = new List<string>();
            EmailListBusiness memailListBusiness = new EmailListBusiness();
            IEnumerable<EmailDTO> emailinfo;
            string[] Totalid = Id.Split(new string[] { "," }, StringSplitOptions.None);
            emailinfo = memailListBusiness.GetEmailprofileInfoById(Id);
            foreach (var emailinfo1 in emailinfo)
            {
                ProfileURL.Add(emailinfo1.Url);
            }
            string message = "Hi ";

            LinkedinDbService m_LinkedinDbService = new LinkedinDbService();
            return m_LinkedinDbService.ClickSendInvitaion(ProfileURL, Totalid, message, MyUserInfo);


        }

        public IEnumerable<SendInvitationDTO> FindStatusofInvitationById(string Id, UserInfo MyUserInfo)
        {
            List<string> ProfileURL = new List<string>();
            EmailListBusiness memailListBusiness = new EmailListBusiness();
            IEnumerable<EmailDTO> emailinfo;
            string[] Totalid = Id.Split(new string[] { "," }, StringSplitOptions.None);
            emailinfo = memailListBusiness.GetEmailprofileInfoById(Id);
            foreach (var emailinfo1 in emailinfo)
            {
                ProfileURL.Add(emailinfo1.Url);
            }
            string message = "Hi ";

            LinkedinDbService m_LinkedinDbService = new LinkedinDbService();
            return m_LinkedinDbService.FindStatusofInvitationById(ProfileURL, Totalid, message, MyUserInfo);


        }
    }
}
