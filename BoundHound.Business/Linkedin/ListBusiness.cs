﻿using BoundHound.Business.Interface;
using BoundHound.DbService.Linkedin;
using BoundHound.DbService.List;
using BoundHound.Model;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace BoundHound.Business.Linkedin
{
    public class ListBusiness : IListBusiness
    {

        public IEnumerable<ListDTO> GetList(UserInfo MyUserInfo)
        {
            ListDbService m_ListDbService = new ListDbService();
            return m_ListDbService.GetList(MyUserInfo);
        }
        public int CreateList(ListDTO listDTO, UserInfo MyUserInfo)
        {
            ListDbService m_ListDbService = new ListDbService();
            return m_ListDbService.CreateList(listDTO,MyUserInfo);
        }

        public int DeleteList(int Id, UserInfo MyUserInfo)
        {
            ListDbService m_ListDbService = new ListDbService();
            return m_ListDbService.DeleteList(Id,MyUserInfo);
        }

        public ListDTO GetListInfo(int Id, UserInfo MyUserInfo)
        {
            ListDbService m_ListDbService = new ListDbService();
            return m_ListDbService.GetListInfo(Id,MyUserInfo);
        }

        public int MoveToList(string EmailIds, int listId, UserInfo MyUserInfo)
        {
            ListDbService m_ListDbService = new ListDbService();
            return m_ListDbService.MoveToList(EmailIds,listId,MyUserInfo);
        }

        public ListDTO UpdateList(ListDTO listDTO, UserInfo MyUserInfo)
        {
            ListDbService m_ListDbService = new ListDbService();
            return m_ListDbService.UpdateList(listDTO,MyUserInfo);
        }
    }
}
