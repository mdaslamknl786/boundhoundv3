﻿using BoundHound.Business.Services.Tenant;
using BoundHound.DbService.Tenant;
using BoundHound.Model;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace BoundHound.Business.Services.Tenant
{
    public class TenantBusiness : ITenantBusiness
    {
        public TenantDTO GetTenantInfo(int TenantId)
        {
            TenantDbService m_TenantDbService = new TenantDbService();
            TenantDTO TenantInfo = new TenantDTO();
            TenantInfo = m_TenantDbService.GetTenantInfo(TenantId);

            return TenantInfo;
        }
    }
}
