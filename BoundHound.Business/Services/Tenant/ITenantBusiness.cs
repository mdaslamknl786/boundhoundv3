﻿using BoundHound.Model;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace BoundHound.Business.Services.Tenant
{
    public interface ITenantBusiness
    {
        TenantDTO GetTenantInfo(int TenantId);
    }
}
