﻿using BoundHound.Business.Services.Register;
using BoundHound.DbService.Linkedin;
using BoundHound.Model;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace BoundHound.Business.Services.Register
{
    public class RegisterBusiness : IRegisterBusiness
    {
        public ResultReponse RegisterForm(UserDTO RegisterInfo)
        {

            try
            {

                RegisterDbService m_RegisterDbService = new RegisterDbService();

                return m_RegisterDbService.RegisterForm(RegisterInfo);
            }
            catch(Exception exp)
            {
                throw exp;
            }

        }
    }
}
