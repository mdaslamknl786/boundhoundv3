﻿using BoundHound.Framework;
using BoundHound.Model;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace BoundHound.Business.Services.User
{
    public interface IUserServiceBusiness
    {
        UserDTO IsUserCredentialsValid(string userName);
        bool IsUserExist(string userName);
        bool ResetPassword(BoundHoundContext context, int id);
        bool ChangePassword(BoundHoundContext context, LoginDTO dto);
        UserDTO GetUserInfo(BoundHoundContext context, int id);
    }
}
