﻿using BoundHound.Framework;
using BoundHound.Model.UserRole;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace BoundHound.Business.Security.Permission
{
    public interface IPermissionService
    {
        bool HasPermission(BoundHoundContext context, PermissionAccessLevel permissionAccessLevel);
        void PerformPermissionCheck(BoundHoundContext context, PermissionAccessLevel permissionAccessLevel, string callingClass = null);
        IEnumerable<PermissionDTO> GetUserPermissions(BoundHoundContext context);
    }
}
