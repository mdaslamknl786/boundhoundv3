﻿using BoundHound.Enterprise.Enum.Role;
using BoundHound.Framework;
using BoundHound.Model.UserRole;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace BoundHound.Business.Security.Permission
{
    public class PermissionService : IPermissionService
    {
        //private readonly IPermissionReader m_permissionReader;
        //public PermissionService(IPermissionReader permissionReader)
        //{
        //    m_permissionReader = permissionReader;
        //}
        public PermissionService()
        {
            //m_permissionReader = permissionReader;
        }


        public bool HasPermission(BoundHoundContext context, PermissionAccessLevel permissionAccessLevel)
        {
            //var accessLevel = GetAccessLevel(context, permissionAccessLevel.Permission);
            //return accessLevel >= permissionAccessLevel.AccessLevel;
            return true;
        }

        public void PerformPermissionCheck(BoundHoundContext context, PermissionAccessLevel permissionAccessLevel,
            string callingClass = null)
        {
            if (!HasPermission(context, permissionAccessLevel))
            {
                throw new UnauthorizedAccessException();
            }
        }

        public IEnumerable<PermissionDTO> GetUserPermissions(BoundHoundContext context)
        {
            yield return new PermissionDTO(AccessLevel.AddEdit);
        }

        //private AccessLevel GetAccessLevel(BoundHoundContext context, Permission permission)
        //{
        //    var userId = context.ActingUserId;
        //    if (userId == UserConstants.BatchProcessUserId
        //         || userId == UserConstants.AnonymousUserId
        //         || userId == OtherConstants.DefaultUserId)
        //    {
        //        return AccessLevel.Delete;
        //    }

        //    var roleId = m_permissionReader.GetUserRoleId(context);
        //    if (roleId == (int)Role.DefaultRole.Administrator)
        //    {
        //        return AccessLevel.Delete;
        //    }

        //    var permissions = m_permissionReader.GetPermissions(context, roleId);
        //    var userPermission = permissions.FirstOrDefault(p => p.Permission == permission);
        //    return userPermission?.AccessLevel ?? AccessLevel.None;
        //}
    }
}
