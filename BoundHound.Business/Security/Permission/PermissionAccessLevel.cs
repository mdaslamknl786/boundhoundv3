﻿using BoundHound.DbService.Utils;
using BoundHound.Enterprise.Enum.Role;
using System;


namespace BoundHound.Business.Security.Permission
{
    public class PermissionAccessLevel : IEquatable<PermissionAccessLevel>
    {
        //public Permission Permission { get; }
        public Enterprise.Enum.Role.AccessLevel AccessLevel { get; }

        //public PermissionAccessLevel(Enum.Role.Permission permission, AccessLevel accessLevel = AccessLevel.AddEdit)
        //{
        //    Permission = permission;
        //    AccessLevel = accessLevel;
        //}

        public PermissionAccessLevel(AccessLevel accessLevel = AccessLevel.AddEdit)
        {
            //Permission = permission;
            AccessLevel = accessLevel;
        }

        public override bool Equals(object obj)
        {
            return (PermissionAccessLevel)obj != null && Equals((PermissionAccessLevel)obj);
        }

        public bool Equals(PermissionAccessLevel other)
        {
            //return Permission == other?.Permission &&
            //       AccessLevel == other.AccessLevel;

            return AccessLevel == other.AccessLevel;
        }

        public override int GetHashCode()
        {
            //return HashCode.Start
            //    .Hash(Permission)
            //    .Hash(AccessLevel);

            return HashCode.Start.Hash(AccessLevel);
        }
        public override string ToString() => $"{AccessLevel}";

        //public override string ToString() => $"{Permission}|{AccessLevel}";
    }
}
