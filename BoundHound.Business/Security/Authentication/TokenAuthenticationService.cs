﻿using BoundHound.Business.Settings.Interfaces;
using BoundHound.Model;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace BoundHound.Business.Security.Authentication
{
    public class TokenAuthenticationService : ITokenAuthenticationService
    {
        private readonly IAuthenticationSetting m_authenticationSetting;

        public string CreateJSONWebToken(UserDTO dto, int? branchId = null)
        {
            throw new NotImplementedException();
        }

        public bool IsAuthenticatedViaToken(HttpActionContext filterContext)
        {
            throw new NotImplementedException();
        }

        public bool ValidateJSONWebToken(string tokenString)
        {
            throw new NotImplementedException();
        }

        public BoundHoundDTO ValidateServerName(string tokenString)
        {
            throw new NotImplementedException();
        }
        //private readonly IUserDataReader m_userDataReader;
        //private readonly ISchoolService m_schoolService;
        //private readonly IHttpContextService m_httpContextService;



        //public TokenAuthenticationService(IAuthenticationSetting authenticationSetting, IUserDataReader userDataReader, IHttpContextService httpContextService, ISchoolService schoolService)
        //{
        //    m_authenticationSetting = authenticationSetting;
        //    m_userDataReader = userDataReader;
        //    m_httpContextService = httpContextService;
        //    m_schoolService = schoolService;
        //}

        //public bool IsAuthenticatedViaToken(HttpActionContext filterContext)
        //{
        //    if (filterContext.Request.Headers.Contains(TokenAuthentication.TokenHeaderName))
        //    {
        //        var authToken =
        //            filterContext.Request.Headers.GetValues(TokenAuthentication.TokenHeaderName).FirstOrDefault();
        //        return !string.IsNullOrEmpty(authToken) && ValidateJSONWebToken(authToken);
        //    }

        //    return false;
        //}

        //public string CreateJSONWebToken(UserDTO dto, int? branchId = null)
        //{
        //    var tokenHandler = new JWTSecurityTokenHandler();
        //    var encryptionKey = m_authenticationSetting.AuthTokenEncryptionKey;
        //    var apiClientKey = m_authenticationSetting.ApiClientKey;
        //    var symetricKey = StringUtils.GetBytes(encryptionKey);
        //    var domainName = HttpContext.Current.Request.Url.Host;
        //    var scheme = m_authenticationSetting.UseSSL;

        //    var now = DateTime.UtcNow;
        //    var tokenDescriptor = new SecurityTokenDescriptor
        //    {
        //        Subject = new ClaimsIdentity(new[]
        //        {
        //            new Claim(AuthenticatedPatasalaUser.Field.UserId.ToString(), dto.Id.ToString()),
        //            new Claim(AuthenticatedPatasalaUser.Field.UserEmail.ToString(), dto.Email),
        //            new Claim(AuthenticatedPatasalaUser.Field.UserName.ToString(), dto.UserName),
        //            new Claim(AuthenticatedPatasalaUser.Field.SchoolId.ToString(), dto.SchoolId.ToString()),
        //            new Claim(AuthenticatedPatasalaUser.Field.BranchId.ToString(), branchId == null ? Convert.ToUInt32(dto.BranchId).ToString() : branchId.ToString()),
        //            new Claim(AuthenticatedPatasalaUser.Field.Role.ToString(), dto.RoleId.ToString()),
        //            new Claim(AuthenticatedPatasalaUser.Field.PersonId.ToString(), dto.PersonId.ToString()),
        //            new Claim(AuthenticatedPatasalaUser.Field.ClientKey.ToString(), apiClientKey)
        //        }, AuthenticationTypes.Signature),
        //        TokenIssuerName = TokenAuthentication.TokenIssuer,
        //        AppliesToAddress = $@"{scheme}://{domainName}",
        //        Lifetime = new Lifetime(now, now.AddMonths(3)),
        //        SigningCredentials = new SigningCredentials(new InMemorySymmetricSecurityKey(symetricKey),
        //            "http://www.w3.org/2001/04/xmldsig-more#hmac-sha256",
        //            "http://www.w3.org/2001/04/xmlenc#sha256")
        //    };
        //    var token = tokenHandler.CreateToken(tokenDescriptor);
        //    var tokenString = tokenHandler.WriteToken(token);
        //    return tokenString;
        //}

        //public bool ValidateJSONWebToken(string tokenString)
        //{
        //    var tokenHandler = new JWTSecurityTokenHandler();
        //    var encryptionKey = m_authenticationSetting.AuthTokenEncryptionKey;
        //    var symetricKey = StringUtils.GetBytes(encryptionKey);
        //    var domainName = HttpContext.Current.Request.Url.Host;
        //    var scheme = m_authenticationSetting.UseSSL;

        //    var validationParameters = new TokenValidationParameters
        //    {
        //        AllowedAudience = $@"{scheme}://{domainName}",
        //        SigningToken = new BinarySecretSecurityToken(symetricKey),
        //        ValidIssuer = TokenAuthentication.TokenIssuer
        //    };

        //    try
        //    {
        //        var principal = tokenHandler.ValidateToken(tokenString, validationParameters);

        //        var userIdClaim = principal.Identities.First()
        //            .Claims.FirstOrDefault(c => c.Type == AuthenticatedPatasalaUser.Field.UserId.ToString());
        //        var userNameClaim = principal.Identities.First()
        //            .Claims.FirstOrDefault(c => c.Type == AuthenticatedPatasalaUser.Field.UserName.ToString());
        //        var userEmailClaim = principal.Identities.First()
        //            .Claims.FirstOrDefault(c => c.Type == AuthenticatedPatasalaUser.Field.UserEmail.ToString());
        //        var schoolIdClaim = principal.Identities.First()
        //            .Claims.FirstOrDefault(c => c.Type == AuthenticatedPatasalaUser.Field.SchoolId.ToString());
        //        var branchIdClaim = principal.Identities.First()
        //            .Claims.FirstOrDefault(c => c.Type == AuthenticatedPatasalaUser.Field.BranchId.ToString());
        //        var roleIdClaim = principal.Identities.First()
        //            .Claims.FirstOrDefault(c => c.Type == AuthenticatedPatasalaUser.Field.Role.ToString());
        //        var personIdClaim = principal.Identities.First()
        //            .Claims.FirstOrDefault(c => c.Type == AuthenticatedPatasalaUser.Field.PersonId.ToString());

        //        var userId = Convert.ToUInt32(userIdClaim.Value);
        //        var email = userEmailClaim != null ? userEmailClaim.Value : "";

        //        if (userId > 0)
        //        {
        //            try
        //            {
        //                var context = new SchoolContext(Convert.ToInt32(schoolIdClaim.Value),
        //                    Convert.ToInt32(branchIdClaim.Value), Convert.ToInt32(userId), userNameClaim.Value,
        //                    userEmailClaim.Value, Convert.ToInt32(personIdClaim.Value));

        //                var userDto = m_userDataReader.SelectUserInfo(context, Convert.ToInt32(userId));
        //                if (userDto == null)
        //                {
        //                    throw new InvalidAuthTokenException("No active user found to authenticate");
        //                }

        //                var apiClientKey = m_authenticationSetting.ApiClientKey;

        //                var claimsCollection = new List<Claim>()
        //                {
        //                    new Claim(AuthenticatedPatasalaUser.Field.UserId.ToString(), userId.ToString()),
        //                    new Claim(AuthenticatedPatasalaUser.Field.UserName.ToString(), userNameClaim.Value),
        //                    new Claim(AuthenticatedPatasalaUser.Field.UserEmail.ToString(), userEmailClaim.Value),
        //                    new Claim(AuthenticatedPatasalaUser.Field.SchoolId.ToString(), schoolIdClaim.Value),
        //                    new Claim(AuthenticatedPatasalaUser.Field.BranchId.ToString(),
        //                        branchIdClaim != null ? branchIdClaim.Value : ""),
        //                    new Claim(AuthenticatedPatasalaUser.Field.Role.ToString(), roleIdClaim.Value),
        //                    new Claim(AuthenticatedPatasalaUser.Field.PersonId.ToString(), personIdClaim.Value),
        //                    new Claim(AuthenticatedPatasalaUser.Field.ClientKey.ToString(), apiClientKey)
        //                };

        //                ClaimsIdentity claimIdentity =
        //                    new ClaimsIdentity(claimsCollection, AuthenticationTypes.Signature);
        //                principal = new ClaimsPrincipal(claimIdentity);

        //                m_httpContextService.CurrentUser = principal;
        //                return true;
        //            }
        //            catch (Exception ex)
        //            {
        //                throw new AuthenticationException("Something went wrong while validating authentication token",
        //                    ex);
        //            }
        //        }
        //        throw new AuthenticationException(
        //            $"Oops!! UserId not found in authentication token, {userId}, {email}");
        //    }
        //    catch (SignatureVerificationException ex)
        //    {
        //        throw new AuthenticationException("Invalid signature authentication token", ex);
        //    }
        //    catch (Exception ex)
        //    {
        //        throw new AuthenticationException("Invalid authentication token", ex);
        //    }
        //}

        //public SchoolDTO ValidateServerName(string tokenString)
        //{
        //    return m_schoolService.GetSchoolAccount(tokenString);
        //}
    }
}
