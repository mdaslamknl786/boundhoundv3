﻿using BoundHound.Model;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace BoundHound.Business.Security.Authentication
{
    public interface ITokenAuthenticationService
    {
        bool IsAuthenticatedViaToken(HttpActionContext filterContext);

        string CreateJSONWebToken(UserDTO dto, int? branchId = null);

        bool ValidateJSONWebToken(string tokenString);

        BoundHoundDTO ValidateServerName(string tokenString);
    }
}
