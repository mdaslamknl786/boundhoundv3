﻿using BoundHound.Business.Interface;
using BoundHound.DbService.Linkedin;
using BoundHound.DbService.List;
using BoundHound.DbService.Users;
using BoundHound.Model;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace BoundHound.Business.User
{
    public class UsersBusiness : IUserBusiness
    {
        public UserInfo Authenticate(string UserName, string Password)
        {
            UsersDbService m_UsersDbService = new UsersDbService();
            return m_UsersDbService.Authenticate(UserName, Password);
        }

        public UserInfo TokenValidation(string Email, int UserId, int TenantId)
        {
            UsersDbService m_UsersDbService = new UsersDbService();
            return m_UsersDbService.TokenValidation(Email, UserId, TenantId);
        }
    }
}
