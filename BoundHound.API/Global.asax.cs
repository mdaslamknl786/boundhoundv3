﻿using BoundHound.API.App_Start;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Http;
using System.Web.Mvc;
using System.Web.Optimization;
using System.Web.Routing;

namespace BoundHound.API
{
    public class WebApiApplication : System.Web.HttpApplication
    {
        public string Log4NetFileLocation = "";
        public string LogFullPath = "";
        protected void Application_Start()
        {
            AreaRegistration.RegisterAllAreas();

            GlobalConfiguration.Configure(WebApiConfig.Register);
            GlobalConfiguration.Configure(IOCConfig.RegisterManagers);

            FilterConfig.RegisterGlobalFilters(GlobalFilters.Filters);
            RouteConfig.RegisterRoutes(RouteTable.Routes);
            

            BundleConfig.RegisterBundles(BundleTable.Bundles);

            var log4NetPath = Server.MapPath("~/log4net.config");
            //For Log
            log4net.Config.XmlConfigurator.ConfigureAndWatch(new System.IO.FileInfo(log4NetPath));
            LogFullPath = Server.MapPath("~/logs/");
            Log4NetFileLocation = string.Format("{0}BoundHound.API-", LogFullPath);
            Helpers.ChangeFileLocation(Log4NetFileLocation);

        }

        protected void Session_Start()
        {
            LogFullPath = Server.MapPath("~/logs/");
            Log4NetFileLocation = string.Format("{0}BoundHound.API-", LogFullPath);
            Session["LogFilePath"] = Log4NetFileLocation;
        }

        protected void Application_BeginRequest(object sender, EventArgs e)
        {
            //HttpContext.Current.Response.AddHeader("Access-Control-Allow-Origin", "*");

            //if (HttpContext.Current.Request.HttpMethod == "OPTIONS")
            //{
            //    // These headers are handling the "pre-flight" OPTIONS call sent by the browser
            //    HttpContext.Current.Response.AddHeader("Access-Control-Allow-Methods", "POST, GET, PUT, DELETE, HEAD");
            //    HttpContext.Current.Response.AddHeader("Access-Control-Allow-Headers", "Content-Type, Accept, token");
            //    HttpContext.Current.Response.AddHeader("Access-Control-Max-Age", "1728000");
            //    HttpContext.Current.Response.End();
            //}

            //HttpContext.Current.Response.AddHeader("Access-Control-Expose-Headers", "Date");
        }
    }
}
