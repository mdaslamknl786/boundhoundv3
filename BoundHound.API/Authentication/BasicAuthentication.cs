﻿using BoundHound.Business.Interface;
using BoundHound.Model;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Net.Http.Headers;

using System.Security.Principal;
using System.Threading;
using System.Web;
using System.Web.Http.Controllers;
using System.Web.Http.Filters;
using System.Web.Mvc;
using System.Web.Script.Serialization;
namespace BoundHound.API.Authentication
{
    public class BasicAuthentication : AuthorizationFilterAttribute
    {
        public override void OnAuthorization(HttpActionContext actionContext)
        {
            var provider = actionContext.ControllerContext.Configuration.DependencyResolver.GetService(typeof(IUserBusiness)) as IUserBusiness;
            UserInfo MyUserInfo = null;
            string BoundHoundAuthToken = "";
            var tokenEntities = new AuthUserDTO { };
            //Checking token in header if exists will go for token validation else go for username password validation
            HttpRequestHeaders headers = actionContext.Request.Headers;
            if (headers.Contains("BoundHoundAuthToken"))
            {
                BoundHoundAuthToken = headers.GetValues("BoundHoundAuthToken").First();
                //validating token with data base
                MyUserInfo = Helpers.ValidateToken(BoundHoundAuthToken, actionContext);
                if (MyUserInfo != null)
                {
                    tokenEntities = new AuthUserDTO
                    {
                        TenatId = MyUserInfo.TenantId,
                        UserId = MyUserInfo.UserId
                    };

                    //BasicAuthenticationDTO _basicAuthIdentity = new BasicAuthenticationDTO(tokenEntities);
                    //var genericPrincipal = new GenericPrincipal(_basicAuthIdentity, null);
                    //Thread.CurrentPrincipal = genericPrincipal;
                }
            }
            else
            {
                JavaScriptSerializer js = new JavaScriptSerializer();
                var request = actionContext.Request.Content.ReadAsStringAsync().Result;
                UserInfo UsersInfo = js.Deserialize<UserInfo>(request);
                if (UsersInfo != null)
                {
                    //validating username and password with data base
                    MyUserInfo = provider.Authenticate(UsersInfo.Email, UsersInfo.Password);
                }

                if (MyUserInfo != null)
                {
                    UserDTO mUserDTO = new UserDTO();
                    mUserDTO.TenantId = MyUserInfo.TenantId;
                    mUserDTO.UserEmail = UsersInfo.Email;
                    mUserDTO.Id = MyUserInfo.UserId;
                    mUserDTO.UserName = MyUserInfo.UserName;

                    //Generating token Based on above parameters
                    string Token = Helpers.GenerateToken(mUserDTO);

                    tokenEntities = new AuthUserDTO
                    {
                        UserName = MyUserInfo.UserName,
                        UserEmail = UsersInfo.Email,
                        TenatId = MyUserInfo.TenantId,
                        UserId = MyUserInfo.UserId,
                        SubscriptionInDays = MyUserInfo.SubscriptionId,
                        access_token = Token,
                        ExpiredDate = DateTime.Now.AddHours(1).ToString(),
                        GenerateDate = DateTime.Now.ToString(),
                        Modules = ""
                    };

                    //BasicAuthenticationDTO _basicAuthIdentity = new BasicAuthenticationDTO(tokenEntities);
                    //var genericPrincipal = new GenericPrincipal(_basicAuthIdentity, null);
                    //Thread.CurrentPrincipal = genericPrincipal;

                    //_basicAuthIdentity.UserName = MyUserInfo.UserName;
                    //_basicAuthIdentity.UserEmail = UsersInfo.Email;
                    //_basicAuthIdentity.TenatId = MyUserInfo.TenantId;
                    //_basicAuthIdentity.UserId = MyUserInfo.UserId;
                    //_basicAuthIdentity.SubscriptionInDays = MyUserInfo.SubscriptionId;
                    //_basicAuthIdentity.BoundHoundAuthToken = Token;
                    //_basicAuthIdentity.ExpiredDate = DateTime.Now.AddHours(1).ToString();
                    //_basicAuthIdentity.GenerateDate = DateTime.Now.ToString();
                    //_basicAuthIdentity.Modules = "";
                }
                else
                {
                    actionContext.Response = actionContext.Request.CreateResponse(HttpStatusCode.Unauthorized, "Invalid UserName or password");
                }

            }


            BasicAuthenticationDTO _basicAuthIdentity = new BasicAuthenticationDTO(tokenEntities);
            var genericPrincipal = new GenericPrincipal(_basicAuthIdentity, null);
            Thread.CurrentPrincipal = genericPrincipal;


            //  base.OnAuthorization(actionContext);
        }

    }
}