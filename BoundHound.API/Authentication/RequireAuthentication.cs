﻿using BoundHound.Business.Security.Authentication;
using BoundHound.Framework.Services;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Http.Filters;


namespace BoundHound.API.Authentication
{
    public class RequireAuthentication : AuthorizationFilterAttribute
    {
        private Lazy<IHttpContextService> m_httpContextService;
        private Lazy<ITokenAuthenticationService> m_tokenAuthenticationService;
        public RequireAuthentication()
        {
            m_httpContextService = new Lazy<IHttpContextService>(ServiceLocator.GetInstance<IHttpContextService>);
            m_tokenAuthenticationService = new Lazy<ITokenAuthenticationService>(ServiceLocator.GetInstance<ITokenAuthenticationService>);
        }

        //public override void OnAuthorization(HttpActionContext filterContext)
        //{
        //    SchoolContext schoolContext = null;
        //    if (filterContext.Request.Headers.Contains(TokenAuthentication.TokenHeaderName) ||
        //        filterContext.Request.Headers.Contains(TokenAuthentication.TokenHeaderServerName))
        //    {
        //        schoolContext = new PatasalaCommon(m_httpContextService.Value, m_tokenAuthenticationService.Value)
        //            .GetSchoolContext();

        //        if (schoolContext == null)
        //        {
        //            filterContext.Response = new HttpResponseMessage(HttpStatusCode.Unauthorized);
        //        }
        //    }
        //    else if (!HttpContext.Current.User.Identity.IsAuthenticated)
        //    {
        //        filterContext.Response = new HttpResponseMessage(HttpStatusCode.Unauthorized);
        //    }

        //    if (HttpContext.Current.User.Identity.IsAuthenticated)
        //    {
        //        var authorizeAttribute = GetApiAuthorizeAttributes(filterContext.ActionDescriptor).FirstOrDefault();
        //        if (authorizeAttribute != null)
        //        {
        //            var hasPermission = ServiceLocator.GetInstance<IPermissionService>().HasPermission(schoolContext,
        //                new PermissionAccessLevel(authorizeAttribute.Permission, authorizeAttribute.AccessLevel));
        //            if (!hasPermission)
        //            {
        //                filterContext.Response = new HttpResponseMessage(HttpStatusCode.Forbidden);
        //            }
        //        }
        //    }
        //}

        //private IEnumerable<AuthorizeApiAttribute> GetApiAuthorizeAttributes(HttpActionDescriptor descriptor)
        //{
        //    return descriptor.GetCustomAttributes<AuthorizeApiAttribute>(true)
        //        .Concat(descriptor.ControllerDescriptor.GetCustomAttributes<AuthorizeApiAttribute>(true));
        //}
    }
}