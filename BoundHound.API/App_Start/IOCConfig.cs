﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Http;
using BoundHound.Business.Interface;
using BoundHound.Business.Linkedin;
using BoundHound.Business.User;
using SimpleInjector;
using SimpleInjector.Integration.WebApi;
namespace BoundHound.API.App_Start
{
    public static class IOCConfig
    {
        public static void RegisterManagers(HttpConfiguration config)
        {
            var container = new SimpleInjector.Container();

            //EmailList
            container.Register<IEmailListBusiness, EmailListBusiness>(Lifestyle.Transient);

            //Linkedin
            container.Register<ILinkedinBusiness, LinkedinBusiness>(Lifestyle.Transient);

            //Tenant
            container.Register<ITenantBusiness, TenantBusiness>(Lifestyle.Transient);

            //Register
            container.Register<IRegisterBusiness, RegisterBusiness>(Lifestyle.Transient);

            //List
            container.Register<IListBusiness, ListBusiness>(Lifestyle.Transient);

            //User
            container.Register<IUserBusiness, UsersBusiness>(Lifestyle.Transient);

            // User
            //container.Register<ILoginManager, LoginManager>(Lifestyle.Transient);
            //container.Register<ILoginRepository, LoginRepository>(Lifestyle.Transient);



            config.DependencyResolver = new SimpleInjectorWebApiDependencyResolver(container);
        }
    }
}