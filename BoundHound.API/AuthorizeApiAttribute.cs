﻿using BoundHound.Enterprise.Enum.Role;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace BoundHound.API
{
    public class AuthorizeApiAttribute : Attribute
    {
        public Permission Permission { get; }
        public AccessLevel AccessLevel { get; }
        public AuthorizeApiAttribute(Permission permission, AccessLevel accessLevel)
        {
            Permission = permission;
            AccessLevel = accessLevel;
        }
    }
}