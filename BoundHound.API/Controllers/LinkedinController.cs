﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Web.Http;
using BoundHound.Model;
using System.Web.Http.Cors;
using System.Configuration;
using BoundHound.API.Base;
using BoundHound.Business.Interface;

namespace BoundHound.API.Controllers
{

    public class LinkedinController : ApiControllerBase
    {
        private readonly ILinkedinBusiness m_ILinkedinBusiness;
        private readonly IEmailListBusiness m_IEmailListBusiness;
        private readonly UserInfo MyUserInfo = new UserInfo();
        //private readonly IDashBoardManager _dashboardManager;

        public LinkedinController(ILinkedinBusiness mlinkedinBusiness,IEmailListBusiness memailListBusiness)
        {
            MyUserInfo= SetUserInfo();
            m_ILinkedinBusiness = mlinkedinBusiness;
            m_IEmailListBusiness = memailListBusiness;
        }


        [Route("api/GetFromLinkedin/{noOfConnections}")]
        [HttpGet]
        public IHttpActionResult GetFromLinkedin(int noOfConnections)
        {
            try
            {
                
                IEnumerable<EmailDTO> EmailDTOList = new List<EmailDTO>();
                EmailDTOList = m_ILinkedinBusiness.GetFromLinkedin(noOfConnections, MyUserInfo);
                return Json(EmailDTOList);
            }
            catch (Exception Ex)
            {
                Helpers.ErrorLog(typeof(LinkedinController).ToString(), "GetFromLinkedin", Ex.ToString());
            }
            return null;
        }

        [Route("api/GetFromLinkedInWithEmails/{noOfConnections}")]
        [HttpGet]
        public IHttpActionResult GetFromLinkedInWithEmail(int noOfConnections)
        {
            try
            {

                IEnumerable<EmailDTO> EmailDTOList = new List<EmailDTO>();
                EmailDTOList = m_ILinkedinBusiness.GetFromLinkedInWithEmails(noOfConnections, MyUserInfo);
                return Json(EmailDTOList);
            }
            catch (Exception Ex)
            { Helpers.ErrorLog(typeof(LinkedinController).ToString(), "GetFromLinkedin", Ex.ToString()); }
            return null;
        }

        [Route("api/GetConnections/{noOfConnections}")]
        [HttpGet]
        public IHttpActionResult GetConnections(int noOfConnections)
        {
            try
            {
                IEnumerable<EmailDTO> EmailDTOList = new List<EmailDTO>();
                EmailDTOList = m_ILinkedinBusiness.GetConnections(noOfConnections,MyUserInfo);
                return Json(EmailDTOList);
            }
            catch (Exception Ex)
            { Helpers.ErrorLog(typeof(LinkedinController).ToString(), "GetConnections", Ex.ToString()); }
            return null;
        }

        [Route("api/GetConnectionEmailByProfileId/{Id}/{tenantId}/{userId}")]
        [HttpGet]
        public IHttpActionResult GetConnectionEmailByProfileId(string Id, int TenantId, int UserId)
        {
            string MyEmail = "";
            try
            {
                MyUserInfo.TenantId = TenantId;
                MyUserInfo.UserId = UserId;

                Helpers.Info(typeof(EmailController).ToString(), "GetConnectionEmailByProfileId", "Started " + DateTime.Now.ToString());
                IEnumerable<EmailDTO> EmailDTOList = null;

                EmailDTOList = m_ILinkedinBusiness.GetConnectionEmailByProfileId(Id, MyUserInfo);

                Helpers.Info(typeof(EmailController).ToString(), "GetConnectionEmailByProfileId", "Done " + DateTime.Now.ToString());

                MyEmail = EmailDTOList.FirstOrDefault().email;

                return Json(MyEmail);
            }
            catch (Exception Ex)
            { Helpers.ErrorLog(typeof(LinkedinController).ToString(), "GetConnectionEmailByProfileId", Ex.ToString()); }
            return null;
        }


        [Route("api/GetConnectionEmailByProfileList/{listId}")]
        [HttpGet]
        public IHttpActionResult GetConnectionEmailByProfileList(int listId)
        {
            try
            {
                IEnumerable<EmailDTO> EmailDTOList;
                EmailDTOList = m_IEmailListBusiness.GetEmailList(MyUserInfo, listId);
                EmailDTOList = m_ILinkedinBusiness.GetConnectionEmailByProfileList(EmailDTOList, MyUserInfo);
                return Json(EmailDTOList);
            }
            catch (Exception Ex)
            { Helpers.ErrorLog(typeof(LinkedinController).ToString(), "GetConnectionEmailByProfileList", Ex.ToString()); }
            return null;
        }


        [HttpPost, Route("api/SendMessage/{ProfileId}")]
        public IHttpActionResult SendMessage(string ProfileId)
        {
            try
            {
                IEnumerable<EmailDTO> mEmailDTO;
                string ProfileURL = "";
                bool ReturnValue=false;
                string SendMessage = "Hi [[Name]] Would like to introduct our company";

                
                mEmailDTO = m_IEmailListBusiness.GetConnectionByProfileId(ProfileId,MyUserInfo);
                if(mEmailDTO !=null)
                {
                    ProfileURL = mEmailDTO.FirstOrDefault().Url;
                    SendMessage= SendMessage.Replace("[[Name]]", mEmailDTO.FirstOrDefault().firstname + ' ' + mEmailDTO.FirstOrDefault().lastname);
                    ReturnValue = m_ILinkedinBusiness.SendMessage(ProfileURL, SendMessage, MyUserInfo);
                }
                
                return Json(ReturnValue.ToString());
            }
            catch (Exception Ex)
            { Helpers.ErrorLog(typeof(EmailController).ToString(), "UpdateEmail", Ex.ToString()); }
            return null;
        }

        [Route("api/SendInvitions/{SearchStr}/{ConnectionType}/{tenantId}/{userId}")]
        [HttpGet]
        public IHttpActionResult SendInvitions(string SearchStr, string ConnectionType, int TenantId, int UserId)
        {
            try
            {
                MyUserInfo.TenantId = TenantId;
                MyUserInfo.UserId = UserId;
                IEnumerable<EmailDTO> EmailDTOList;
                Helpers.Info(typeof(ListController).ToString(), "SendInvitions", "Started " + DateTime.Now.ToString());
                EmailDTOList = m_ILinkedinBusiness.SendInvitations(SearchStr, ConnectionType, MyUserInfo);
                Helpers.Info(typeof(ListController).ToString(), "SendInvitions", "End " + DateTime.Now.ToString());
                return Json(EmailDTOList);
            }
            catch (Exception Ex)
            { Helpers.ErrorLog(typeof(LinkedinController).ToString(), "SendInvitions", Ex.ToString()); }
            return null;
        }



        [Route("api/ClickSendInvitaion/{profileID}/{tenantId}/{userId}")]
        [HttpGet]
        public IHttpActionResult ClickSendInvitaion(string ProfileId, int TenantId, int UserId)
        {
            try
            {
                MyUserInfo.TenantId = TenantId;
                MyUserInfo.UserId = UserId;

                Helpers.Info(typeof(LinkedinController).ToString(), "ClickSendInvitaion", "Started " + DateTime.Now.ToString());
                IEnumerable<SendInvitationDTO> EmailDTOList = null;
                EmailDTOList = m_ILinkedinBusiness.ClickSendInvitaion(ProfileId, MyUserInfo);
                Helpers.Info(typeof(LinkedinController).ToString(), "ClickSendInvitaion", "Done " + DateTime.Now.ToString());
                return Json(EmailDTOList);
            }
            catch (Exception Ex)
            { Helpers.ErrorLog(typeof(LinkedinController).ToString(), "ClickSendInvitaion", Ex.ToString()); }
            return null;
        }

        [Route("api/FindStatusofInvitationById/{Id}/{tenantId}/{userId}")]
        [HttpGet]
        public IHttpActionResult FindStatusofInvitationById(string Id, int TenantId, int UserId)
        {
            ResultReponse MyResultReponse = new ResultReponse();
            try
            {
                MyUserInfo.TenantId = TenantId;
                MyUserInfo.UserId = UserId;

                Helpers.Info(typeof(LinkedinController).ToString(), "FindStatusofInvitationById", "Started " + DateTime.Now.ToString());
                IEnumerable<SendInvitationDTO> EmailDTOList = null;

                EmailDTOList = m_ILinkedinBusiness.FindStatusofInvitationById(Id, MyUserInfo);

                Helpers.Info(typeof(LinkedinController).ToString(), "FindStatusofInvitationById", "Done " + DateTime.Now.ToString());

                if (EmailDTOList.Count() > 0)
                {
                    MyResultReponse.Status = "PENDING";
                    MyResultReponse.StatusCode = "PENDING";
                    MyResultReponse.Message = "Status Pending";
                }
                //else
                //{
                //    MyResultReponse.Status = "NOTFOUND";
                //    MyResultReponse.StatusCode = "NOTFOUND";
                //    MyResultReponse.Message = "Email Not Found";
                //    MyResultReponse.Data = MyEmail;
                //}


                return Json(MyResultReponse);
            }
            catch (Exception Ex)
            {
                Helpers.ErrorLog(typeof(LinkedinController).ToString(), "FindStatusofInvitationById", Ex.ToString());
                MyResultReponse.Status = "ERROR";
                MyResultReponse.StatusCode = "NOTFOUND";
                MyResultReponse.Message = "Status Not Found";
            }
            return null;
        }
    }
}
