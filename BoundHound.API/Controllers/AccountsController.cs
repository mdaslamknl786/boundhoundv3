﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Web.Http;
using BoundHound.Model;
using System.Web.Http.Cors;
using System.Configuration;
using BoundHound.API.Base;
using BoundHound.Business.Interface;
using BoundHound.API.Authentication;
using System.Threading;

namespace BoundHound.API.Controllers
{
    public class AccountsController : ApiControllerBase
    {
        private readonly ILinkedinBusiness m_ILinkedinBusiness;
        private readonly IEmailListBusiness m_IEmailListBusiness;
        private readonly IUserBusiness m_IUserBusiness;
        private readonly UserInfo MyUserInfo = new UserInfo();

        public AccountsController(IUserBusiness muserBusiness)
        {
            MyUserInfo = SetUserInfo();
            m_IUserBusiness = muserBusiness;
        }

        [BasicAuthentication]
        [HttpPost, Route("api/authenticate")]
        public IHttpActionResult Authenticate()
        {

            try
            {
                BasicAuthenticationDTO _basicAuthIdentity = Thread.CurrentPrincipal.Identity as BasicAuthenticationDTO;
                //  return Json(tokenEntities);
                return Content(HttpStatusCode.OK, _basicAuthIdentity._AuthUserDTO);
            }
            catch (Exception Ex)
            { Helpers.ErrorLog(typeof(EmailController).ToString(), "Authenticate", Ex.ToString()); }
            return null;
        }






        //[HttpPost, Route("api/authenticate")]
        //public IHttpActionResult Authenticate(UserInfo userInfo)
        //{
        //    UserInfo MyUserInfo = null;
        //    try
        //    {

        //        MyUserInfo = m_IUserBusiness.Authenticate(userInfo.Email, userInfo.Password);
        //        //if (userInfo != null)
        //        //{
        //        //    ProfileURL = mEmailDTO.FirstOrDefault().Url;
        //        //    ReturnValue = m_ILinkedinBusiness.SendMessage(ProfileURL, SendMessage, MyUserInfo);
        //        //}

        //        return Json(userInfo);
        //    }
        //    catch (Exception Ex)
        //    { Helpers.ErrorLog(typeof(EmailController).ToString(), "UpdateEmail", Ex.ToString()); }
        //    return null;
        //}




    }
}
