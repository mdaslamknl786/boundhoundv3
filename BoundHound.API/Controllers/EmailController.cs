﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Web.Http;
using BoundHound.Model;
using System.Web.Http.Cors;
using System.Configuration;
using BoundHound.API.Base;
using BoundHound.Business.Interface;

namespace BoundHound.API.Controllers
{
    public class EmailController : ApiControllerBase
    {
        private readonly IEmailListBusiness m_IEmailListBusiness;
        private readonly ILinkedinBusiness m_ILinkedinBusiness;
        private readonly UserInfo MyUserInfo = new UserInfo();

        public EmailController(IEmailListBusiness memailListBusiness,ILinkedinBusiness mlinkedinBusiness)
        {
            MyUserInfo = SetUserInfo();
            m_IEmailListBusiness = memailListBusiness;
            m_ILinkedinBusiness = mlinkedinBusiness;
        }

        // GET Linkedin/GetEmail
        [Route("api/getemaillist/{tenantId}/{userId}/{listId}")]
        [HttpGet]
        public IHttpActionResult GetEmailList(int TenantId,int UserId, int listId)
        {
            try
            {
                MyUserInfo.TenantId = TenantId;
                MyUserInfo.UserId = UserId;

                Helpers.Info(typeof(EmailController).ToString(), "GetEmails", "Started " + DateTime.Now.ToString());
                IEnumerable<EmailDTO> EmailsList = null;
                EmailsList = m_IEmailListBusiness.GetEmailList(MyUserInfo,listId);
                Helpers.Info(typeof(EmailController).ToString(), "GetEmails", "Done " + DateTime.Now.ToString());

                return Json(EmailsList);
            }
            catch (Exception Ex)
            { Helpers.ErrorLog(typeof(EmailController).ToString(), "GetEmails", Ex.ToString()); }
            return null;
        }



        // GET Linkedin/GetEmail
        [Route("api/getemailListbylistId/{tenantId}/{userId}/{listId}")]
        [HttpGet]
        public IHttpActionResult GetEmailListByListId(int TenantId, int UserId,int listId)
        {
            try
            {
                MyUserInfo.TenantId = TenantId;
                MyUserInfo.UserId = UserId;

                Helpers.Info(typeof(EmailController).ToString(), "GetEmails", "Started " + DateTime.Now.ToString());
                IEnumerable<ListDTO> EmailsList = null;
                EmailsList = m_IEmailListBusiness.GetEmailListByListId(MyUserInfo,listId);
                Helpers.Info(typeof(EmailController).ToString(), "GetEmails", "Done " + DateTime.Now.ToString());

                return Json(EmailsList);
            }
            catch (Exception Ex)
            { Helpers.ErrorLog(typeof(EmailController).ToString(), "GetEmails", Ex.ToString()); }
            return null;
        }


        [Route("api/findemailbyid/{Id}/{tenantId}/{userId}")]
        [HttpGet]
        public IHttpActionResult FindEmailById(string Id,int TenantId, int UserId)
        {
            string MyEmail = "";
            ResultReponse MyResultReponse = new ResultReponse();
            try
            {
                MyUserInfo.TenantId = TenantId;
                MyUserInfo.UserId = UserId;

                Helpers.Info(typeof(EmailController).ToString(), "FindEmailById", "Started " + DateTime.Now.ToString());
                IEnumerable<EmailDTO> EmailDTOList = null;

                EmailDTOList =m_ILinkedinBusiness.GetConnectionEmailByProfileId(Id, MyUserInfo);

                Helpers.Info(typeof(EmailController).ToString(), "FindEmailById", "Done " + DateTime.Now.ToString());

                if (EmailDTOList.Count() > 0)
                { 

                    MyEmail = EmailDTOList.FirstOrDefault().email;
                    MyResultReponse.Status = "SUCCESS";
                    MyResultReponse.StatusCode = "SUCCESS";
                    MyResultReponse.Message = "Email Found";
                    MyResultReponse.Data = MyEmail;

                    foreach (EmailDTO email in EmailDTOList)
                    {
                        int returnValue = 0;
                        email.emaillistId = Convert.ToInt32(Id);
                        returnValue = m_IEmailListBusiness.UpdateConnectionEmail(email.emaillistId, MyEmail);
                    }

                }
                else
                {
                    MyResultReponse.Status = "NOTFOUND";
                    MyResultReponse.StatusCode = "NOTFOUND";
                    MyResultReponse.Message = "Email Not Found";
                    MyResultReponse.Data = MyEmail;
                }


                return Json(MyResultReponse);
            }
            catch (Exception Ex)
            {
                Helpers.ErrorLog(typeof(EmailController).ToString(), "FindEmailById", Ex.ToString());
                MyResultReponse.Status = "ERROR";
                MyResultReponse.StatusCode = "NOTFOUND";
                MyResultReponse.Message = "Email Not Found";
            }
            return null;
        }



        [HttpPost, Route("api/UpdateEmail")]
        public IHttpActionResult UpdateEmail(EmailDTO email)
        {
            try
            {
                int returnValue = 0;
                returnValue = m_IEmailListBusiness.UpdateEmail(email, MyUserInfo);
                return Json(returnValue);
            }
            catch (Exception Ex)
            { Helpers.ErrorLog(typeof(EmailController).ToString(), "UpdateEmail", Ex.ToString()); }
            return null;
        }
    }
}
