﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Web.Http;
using BoundHound.Model;
using System.Web.Http.Cors;
using System.Configuration;
using BoundHound.API.Base;
using BoundHound.Business.Interface;

namespace BoundHound.API.Controllers
{
    public class RegisterController : ApiControllerBase
    {
        private readonly IRegisterBusiness m_IRegisterBusiness;
        private readonly UserInfo MyUserInfo = new UserInfo();
        //private readonly IDashBoardManager _dashboardManager;

        public RegisterController(IRegisterBusiness mregisterBusiness)
        {
            MyUserInfo=SetUserInfo();
            m_IRegisterBusiness = mregisterBusiness;
        }
        [HttpPost, Route("api/RegisterForm")]
        public IHttpActionResult RegisterForm(RegisterForm registerForm)
        {
            ResultReponse MyResultResponse = new ResultReponse();
            try
            {
                UserDTO MyInfo = new UserDTO();

                Helpers.Info(typeof(LinkedinController).ToString(), "RegisterForm : " + registerForm.Email, "Started " + DateTime.Now.ToString());
               
                Helpers.Info(typeof(LinkedinController).ToString(), "RegisterForm : " +registerForm.Email, "Done " + DateTime.Now.ToString());

                //Set UserDTO
                MyInfo.Id = 0;
                MyInfo.UserName = registerForm.UserName;
                MyInfo.UserEmail = registerForm.Email;
                MyInfo.UserPassword= registerForm.Password;
                MyInfo.TenantId = registerForm.TenantID;
                MyInfo.IsFirstTime = registerForm.IsFirstTime;

                MyResultResponse = m_IRegisterBusiness.RegisterForm(MyInfo);
                return Json(MyResultResponse);
            }
            catch (Exception Ex)
            { Helpers.ErrorLog(typeof(LinkedinController).ToString(), "RegisterForm", Ex.ToString()); }
            return null;
        }

    }
}
