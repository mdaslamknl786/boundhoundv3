﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Web.Http;
using BoundHound.Model;
using System.Web.Http.Cors;
using System.Configuration;
using BoundHound.API.Base;
using BoundHound.Business.Interface;

namespace BoundHound.API.Controllers
{
    public class TenantController : ApiControllerBase
    {
        private readonly ITenantBusiness m_ITenantBusiness;
        private readonly UserInfo MyUserInfo = new UserInfo();
        public TenantController(ITenantBusiness mtenantBusiness)
        {
            MyUserInfo = SetUserInfo();
            m_ITenantBusiness = mtenantBusiness;
        }


        // GET Linkedin/GetEmail
        [Route("api/GetTenantInfo/{TenantId}")]
        [HttpGet]
        public IHttpActionResult GetTenantInfo(int TenantId)
        {
            try
            {
                Helpers.Info(typeof(LinkedinController).ToString(), "GetTenantInfo", "Started " + DateTime.Now.ToString());
                TenantDTO TenantInfo = new TenantDTO();
                TenantInfo = m_ITenantBusiness.GetTenantInfo(TenantId);
                Helpers.Info(typeof(LinkedinController).ToString(), "GetTenantInfo", "Done " + DateTime.Now.ToString());

                return Json(TenantInfo);
            }
            catch (Exception Ex)
            { Helpers.ErrorLog(typeof(LinkedinController).ToString(), "GetTenantInfo", Ex.ToString()); }
            return null;
        }
    }
}
