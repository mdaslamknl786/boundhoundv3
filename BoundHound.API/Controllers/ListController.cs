﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Web.Http;
using BoundHound.Model;
using System.Web.Http.Cors;
using System.Configuration;
using BoundHound.API.Base;
using BoundHound.Business.Interface;

namespace BoundHound.API.Controllers
{
    public class ListController : ApiControllerBase
    {
        private readonly IListBusiness m_IListBusiness;
        private readonly UserInfo MyUserInfo = new UserInfo();

        public ListController(IListBusiness mlistBusiness)
        {
            MyUserInfo = SetUserInfo();
            m_IListBusiness = mlistBusiness;
        }

        // GET Linkedin/GetEmail
        [Route("api/list/getlist/{tenantId}/{userId}")]
        [HttpGet]
        public IHttpActionResult GetList(int TenantId, int UserId)
        {
            try
            {
                MyUserInfo.TenantId = TenantId;
                MyUserInfo.UserId = UserId;

                Helpers.Info(typeof(ListController).ToString(), "GetList", "Started " + DateTime.Now.ToString());
                IEnumerable<ListDTO> EmailsList = null;
                EmailsList = m_IListBusiness.GetList(MyUserInfo);
                Helpers.Info(typeof(ListController).ToString(), "GetList", "Done " + DateTime.Now.ToString());

                return Json(EmailsList);
            }
            catch (Exception Ex)
            { Helpers.ErrorLog(typeof(ListController).ToString(), "GetEmails", Ex.ToString()); }
            return null;
        }


        [Route("api/list/GetListInfo/{Id}/{tenantId}/{userId}")]
        [HttpGet]
        public IHttpActionResult GetListInfo(int Id, int TenantId, int UserId)
        {
            try
            {
                MyUserInfo.TenantId = TenantId;
                MyUserInfo.UserId = UserId;

                Helpers.Info(typeof(ListController).ToString(), "GetListInfo", "Started " + DateTime.Now.ToString());
                ListDTO listDTOInfo = null;
                listDTOInfo = m_IListBusiness.GetListInfo(Id,MyUserInfo);
                Helpers.Info(typeof(ListController).ToString(), "GetListInfo", "Done " + DateTime.Now.ToString());

                return Json(listDTOInfo);
            }
            catch (Exception Ex)
            {
                Helpers.ErrorLog(typeof(ListController).ToString(), "GetListInfo", Ex.ToString());
            }
            return null;
        }


        [HttpPost, Route("api/list/createlist/{tenantId}/{userId}")]
        public IHttpActionResult CreateList(ListDTO listDTO, int TenantId, int UserId)
        {
            ResultReponse MyResultReponse = new ResultReponse();
            try
            {
                MyUserInfo.TenantId = TenantId;
                MyUserInfo.UserId = UserId;

                int returnValue = 0;
                Helpers.Info(typeof(ListController).ToString(), "CreateList", "Started " + DateTime.Now.ToString());
                returnValue = m_IListBusiness.CreateList(listDTO, MyUserInfo);
                Helpers.Info(typeof(ListController).ToString(), "CreateList", "Done " + DateTime.Now.ToString());
                if (returnValue > 0)
                {
                    MyResultReponse.Status = "SUCCESS";
                    MyResultReponse.StatusCode = "SUCCESS";
                    MyResultReponse.Message = "List Created Successfully ";
                    MyResultReponse.Data = returnValue.ToString();
                }
                else
                {
                    MyResultReponse.Status = "ERROR";
                    MyResultReponse.StatusCode = "ERROR";
                    MyResultReponse.Message = "Error Occured while inserting contact administrator";
                    MyResultReponse.Data = "0";
                }
                return Json(MyResultReponse);
            }
            catch (Exception Ex)
            {
                Helpers.ErrorLog(typeof(EmailController).ToString(), "CreateList", Ex.ToString());
                MyResultReponse.Status = "ERROR";
                MyResultReponse.StatusCode = "ERROR";
                MyResultReponse.Message = "Error Occured while inserting contact administrator";
                MyResultReponse.Data = "0";
            }
            return Json(MyResultReponse);
        }


        [HttpPost, Route("api/list/UpdateList/{tenantId}/{userId}")]
        public IHttpActionResult UpdateList(ListDTO listDTO, int TenantId, int UserId)
        {
            try
            {
                MyUserInfo.TenantId = TenantId;
                MyUserInfo.UserId = UserId;
                
                ListDTO ReturnlistDTO = null;
                Helpers.Info(typeof(ListController).ToString(), "UpdateList", "Started " + DateTime.Now.ToString());
                ReturnlistDTO = m_IListBusiness.UpdateList(listDTO, MyUserInfo);
                Helpers.Info(typeof(ListController).ToString(), "UpdateList", "Done " + DateTime.Now.ToString());
                return Json(ReturnlistDTO);
            }
            catch (Exception Ex)
            { Helpers.ErrorLog(typeof(EmailController).ToString(), "UpdateList", Ex.ToString()); }
            return null;
        }

        [HttpPost, Route("api/list/DeleteList/{Id}/{tenantId}/{userId}")]
        public IHttpActionResult DeleteList(int Id, int TenantId, int UserId)
        {
            try
            {
                MyUserInfo.TenantId = TenantId;
                MyUserInfo.UserId = UserId;
                int returnValue = 0;
                Helpers.Info(typeof(ListController).ToString(), "DeleteList", "Started " + DateTime.Now.ToString());
                returnValue = m_IListBusiness.DeleteList(Id, MyUserInfo);
                Helpers.Info(typeof(ListController).ToString(), "DeleteList", "Done " + DateTime.Now.ToString());
                return Json(returnValue);
            }
            catch (Exception Ex)
            { Helpers.ErrorLog(typeof(EmailController).ToString(), "DeleteList", Ex.ToString()); }
            return null;
        }

        [HttpGet, Route("api/list/MoveToList/{EmailIds}/{listId}/{tenantId}/{userId}")]
        public IHttpActionResult MoveToList(string EmailIds,int listId, int TenantId, int UserId)
        {
            try
            {
                MyUserInfo.TenantId = TenantId;
                MyUserInfo.UserId = UserId;

                int returnValue = 0;
                Helpers.Info(typeof(ListController).ToString(), "MoveToList", "Started " + DateTime.Now.ToString());
                returnValue = m_IListBusiness.MoveToList(EmailIds,listId, MyUserInfo);
                Helpers.Info(typeof(ListController).ToString(), "MoveToList", "Done " + DateTime.Now.ToString());
                return Json(returnValue);
            }
            catch (Exception Ex)
            { Helpers.ErrorLog(typeof(EmailController).ToString(), "MoveToList", Ex.ToString()); }
            return null;
        }





    }
}
