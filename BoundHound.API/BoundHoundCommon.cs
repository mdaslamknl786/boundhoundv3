﻿using BoundHound.Business.Security.Authentication;
using BoundHound.Framework;
using BoundHound.Framework.Security;
using BoundHound.Framework.Services;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace BoundHound.API
{
    public class BoundHoundCommon
    {
        private readonly IHttpContextService m_httpContextService;
        private readonly ITokenAuthenticationService m_tokenAuthenticationService;

        public BoundHoundCommon(IHttpContextService httpContextService, ITokenAuthenticationService tokenAuthenticationService)
        {
            m_httpContextService = httpContextService;
            m_tokenAuthenticationService = tokenAuthenticationService;
        }

        public BoundHoundContext GetBoundHoundContext()
        {
            var authHeaderValues = HttpContext.Current.Request.Headers.GetValues(TokenAuthentication.TokenHeaderName);

            if (authHeaderValues != null)
            {
                if (!m_httpContextService.IsAuthenticated || !m_httpContextService.CurrentUserClaims.Any())
                {
                    var authToken = authHeaderValues.FirstOrDefault();
                    if (!string.IsNullOrEmpty(authToken))
                    {
                        m_tokenAuthenticationService.ValidateJSONWebToken(authToken);
                    }
                }
            }
            var authenticatedBoundHoundUser = new AuthenticatedBoundHoundUser(m_httpContextService);

            if (m_httpContextService.IsAuthenticated)
            {
                return new BoundHoundContext(authenticatedBoundHoundUser.TenantId(), authenticatedBoundHoundUser.UserId(), 
                    authenticatedBoundHoundUser.UserName(), authenticatedBoundHoundUser.UserEmail());

            }

            authHeaderValues = HttpContext.Current.Request.Headers.GetValues(TokenAuthentication.TokenHeaderServerName);

            if (authHeaderValues != null)
            {
                if (!m_httpContextService.IsAuthenticated || !m_httpContextService.CurrentUserClaims.Any())
                {
                    var authToken = authHeaderValues.FirstOrDefault();
                    if (!string.IsNullOrEmpty(authToken))
                    {
                        var dto = m_tokenAuthenticationService.ValidateServerName(authToken);

                        if (dto != null)
                        {
                            //return new BoundHoundContext(dto.Id, OtherConstants.DefaultUserId,
                            //    UserConstants.AnonymousUserId, OtherConstants.PatasalaAnonymousUserName,
                            //    string.Empty, OtherConstants.DefaultUserId);

                            return new BoundHoundContext(dto.Id, UserConstants.AnonymousUserId, OtherConstants.BoundHoundAnonymousUserName, string.Empty);
                        }
                        return null;
                    }
                }
            }
            return null;
        }

        public BoundHoundContext GetAnonymousSchoolContext()
        {
            return new BoundHoundContext(1, 1, UserConstants.BoundhoundAnonymousUserName,"");
        }
    }
}